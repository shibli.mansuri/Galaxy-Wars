﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MortarBehavior : MonoBehaviour {

	Transform playerTransform;
	public PlayerMovement playerMovementClass;

	CameraMovement cameraClass;
	[SerializeField]
	GameObject crossHairInner;
	[SerializeField]
	GameObject crossHairMiddle;
	[SerializeField]
	GameObject crossHairOuter;

	public Transform rocket;
	SpriteRenderer bulletOneColor;
	SpriteRenderer bulletTwoColor;
	SpriteRenderer bulletThreeColor;

	CircleCollider2D mortarCollider;
	public GameObject explosionParticle;
	public AudioClip explosiveSound;


	public float yoffset ;

	bool waitForCouroutine;

	Vector3 mortarSizeScaleUp;
	Vector3 mortarSizeScaleDown;


	public float  revolutionsOuter;
	public float revolutionsMiddle;
	public float revolutionsInner;

	public float fadeDuration=1f;
	public float scaleDuration=1f;

	public float beatInterval = 0.5f;
	public float scaleUpInterval = 0.3f;
	float moveSpeed=0.5f;

	Quaternion startingRotation;

	float countDownTillExplosion = 0;
	float scaleSize=0.03f;
	float rocketSpeed=0.2f;
	int count;

	Sequence ChangeColorOrangeSequence;
	Sequence ChangeColorRedSequence;
	int rotationAngle=180;

	void Start () 
	{
		InitializeComponents ();
		StartCoroutine (SearchPlayerAndRotate());
	}

	public void InitializeComponents()
	{
		playerTransform = GameObject.FindWithTag(DataConstants.STR_PLAYER_TAG).transform;
		cameraClass = Camera.main.GetComponent<CameraMovement>();

		mortarSizeScaleUp = this.transform.localScale;
		mortarSizeScaleUp.x = this.transform.localScale.x+scaleSize;
		mortarSizeScaleUp.y = this.transform.localScale.y+scaleSize;
		mortarSizeScaleDown = this.transform.localScale;
		bulletOneColor = crossHairInner.GetComponent<SpriteRenderer> ();
		bulletTwoColor = crossHairMiddle.GetComponent<SpriteRenderer> ();
		bulletThreeColor = crossHairOuter.GetComponent<SpriteRenderer> ();

		mortarCollider = GetComponent<CircleCollider2D> ();
		mortarCollider.enabled = false;
		waitForCouroutine = true;

		startingRotation = new Quaternion (0, 0, 0, 0);
		count = 0;
		rocket.gameObject.SetActive (false);
		playerTransform.position = playerMovementClass.desiredPos;
	}

	IEnumerator SearchPlayerAndRotate()
	{
		StartCoroutine( Wait (3.0f));
		//seeking
		while (waitForCouroutine) 
		{
			crossHairInner.transform.RotateAround(crossHairInner.transform.position,Vector3.forward,rotationAngle*revolutionsInner*Time.deltaTime);
			crossHairMiddle.transform.RotateAround(crossHairMiddle.transform.position,Vector3.forward,rotationAngle*revolutionsMiddle*Time.deltaTime);
			crossHairOuter.transform.RotateAround(crossHairOuter.transform.position,Vector3.forward,rotationAngle*revolutionsOuter*Time.deltaTime);

			transform.DOMove (playerTransform.position, moveSpeed);
			yield return null;
		}


		crossHairInner.transform.rotation=startingRotation;
		crossHairMiddle.transform.rotation=startingRotation;
		crossHairOuter.transform.rotation=startingRotation;
		this.transform.parent = cameraClass.transform;
		waitForCouroutine = true;
		SetExplodingPoint();
			
	}
	IEnumerator Wait(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		waitForCouroutine = false;
	}
	void SetExplodingPoint()
	{
		Color orange=new Color(210f/255f,118f/255f,0f);
		ChangeColorOrangeSequence=DOTween.Sequence();
		ChangeColorOrangeSequence.Append (bulletOneColor.DOColor (orange, 1f))
			.Join (bulletTwoColor.DOColor (orange, 1f))
			.Join (bulletThreeColor.DOColor (orange, 1f));

		ScaleUp (scaleUpInterval + countDownTillExplosion);
	}

	void ScaleUp(float scaleUpInterval)
	{
		this.transform.DOScale (mortarSizeScaleUp, scaleUpInterval);

		StartCoroutine( ScaleDown (scaleUpInterval));

	}
	IEnumerator ScaleDown(float scaleDownInterval)
	{
		this.transform.DOScale (mortarSizeScaleDown, scaleUpInterval);

		countDownTillExplosion += 0.2f;
		count++;

		StartCoroutine (Wait (beatInterval));
		while (waitForCouroutine) 
		{
			yield return null;
		}
		waitForCouroutine = true;

		if (count < 3) {

			ScaleUp (scaleUpInterval-countDownTillExplosion);
		} 
		else 
		{
			RocketLaunch ();	
		}

	}

	void RocketLaunch()
	{
		rocket.gameObject.SetActive(true);
		rocket.DOMove (Helper.GetPosRelativeCamera (0.5f, 1f), rocketSpeed).From ().OnComplete (FadeAndScaleUp).SetEase(Ease.InCubic);

		ChangeColorRedSequence = DOTween.Sequence ();
		ChangeColorRedSequence.Append (bulletOneColor.DOColor (Color.red, 0.2f))
			.Join (bulletTwoColor.DOColor (Color.red, 0.2f))
			.Join (bulletThreeColor.DOColor (Color.red, 0.2f))
			.Join (bulletTwoColor.DOColor (Color.red, 0.2f));
		Blast ();
	}
	void FadeAndScaleUp()
	{
		Sequence FadeAndScaleUp = DOTween.Sequence ();
		FadeAndScaleUp
			.Append (bulletOneColor.DOFade (Color.red.a - 2f, fadeDuration))
			.Join (bulletTwoColor.DOFade (Color.red.a - 2f, fadeDuration))
			.Join (bulletThreeColor.DOFade (Color.red.a - 2f, fadeDuration))
			.Join (crossHairInner.transform.DOScale (mortarSizeScaleUp, scaleDuration))
			.Join (crossHairMiddle.transform.DOScale (mortarSizeScaleUp, scaleDuration))
			.Join (crossHairOuter.transform.DOScale (mortarSizeScaleUp, scaleDuration));
	}
	public void Blast()
	{
		mortarCollider.enabled = true;
		if(explosionParticle)
		{
			GameObject explosion;
			explosion = Instantiate (explosionParticle, transform.position,Quaternion.identity,gameObject.transform) as GameObject;
			explosion.SetActive (true);
			bulletOneColor.enabled = false;
			bulletTwoColor.enabled = false;
			bulletThreeColor.enabled = false;
			Destroy (explosion, 2);
		}
		if (explosiveSound) {
			AudioManager.instance.PlaySingle (explosiveSound, 0.75f);
		}
		Destroy (gameObject,1.5f);
	}
}