﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class GameAnalyticsManager : MonoBehaviour
{


	public static GameAnalyticsManager  instance;



	void Awake()
	{
		if (instance) {
			Destroy (this.gameObject);

		}
		else 
		{
			instance = new GameAnalyticsManager ();

		}

	}

	public   void OnGameOver( int levelNo,int waveNo=3)
	{
		
		Analytics.CustomEvent ("GameOverData", new Dictionary<string, object> {
			{ "LevelNo",levelNo },
			{ "DeathWave", waveNo }
		});
	}
	public   void OnLevelComplete(int levelNo,float timeTaken, int noOfAttempts,float playerHealth)
	{
		Analytics.CustomEvent("Level "+levelNo+" Data", new Dictionary<string, object>
			{
				{ "TimeTakenToComplete", timeTaken},
				{ "NoOfAttempts",noOfAttempts},
				{ "RemaingHealth",playerHealth}
			});
		

	}

	public   void OnApplicationClose(int currentLevel)
	{
		
		Analytics.CustomEvent("ApplicationClose", new Dictionary<string, object>
			{
				{ "LevelNo", currentLevel}
			});

	}


	void OnApplicationQuit()
	{
		OnApplicationClose (GameStatistics.instance.levelButton);
	}



}