﻿using UnityEngine;

public class SplineWalker : MonoBehaviour {
    //public UbhLinearShot shot;
    private BezierSpline spline;
    [SerializeField]
    private float duration;

	public bool lookForward;

	public SplineWalkerMode mode;

	private float progress;
	private bool goingForward = true;

   
    /// <summary>
    /// ABC Edit: counts the number of loops completed in ping pong or loop, used for events based on looping
    /// </summary>
    private int loops;

    private void Update () {
		if (goingForward) {
			progress += Time.deltaTime / duration;
			if (progress > 1f) {
				if (mode == SplineWalkerMode.Once) {
					progress = 1f;
				}
				else if (mode == SplineWalkerMode.Loop) {
					progress -= 1f;
                    loops++;// looping from start again , increment loops
				}
				else {
					progress = 2f - progress;
					goingForward = false;
				}
			}
		}
		else {
			progress -= Time.deltaTime / duration;
			if (progress < 0f) {
				progress = -progress;
				goingForward = true;
                loops++;//looping in ping pong, increment loops
			}
		}

		Vector3 position = spline.GetPoint(progress);
		transform.localPosition = position;
        
		if (lookForward) {
            //ABCEDIT start
            //transform.LookAt(position + spline.GetDirection(progress));
            transform.up =  - spline.GetDirection(progress);
            //shot._Angle = transform.eulerAngles.z ;
            ///ABCEDIT end
		}
	}

    /// <summary>
    /// ABC EDIT: spline can only be accessed (set) through this method . This resets the spline walker
    /// .Is used at this moment for boss state changes 
    /// </summary>
    /// <param name="_spline"></param>
    public void SetPath(BezierSpline _spline)
    {
        progress = 0;
        loops = 0;
        spline = _spline;
    }

    /// <summary>
    /// ABC EDIT: Used to know when boss has reached end of their entrance path
    /// </summary>
    /// <returns></returns>
    public bool IsMovementComplete
    {
        get
        {
            return (mode == SplineWalkerMode.Once && progress == 1);
        }
        
    }

    /// <summary>
    /// ABC Edit
    /// </summary>
    /// <returns></returns>
    public int Loops()
    {
        return loops;
    }

    public float Duration()
    {
        return duration;
    }

    public void SetDuration(float _duration)
    {
        this.duration = _duration;
    }

    
}