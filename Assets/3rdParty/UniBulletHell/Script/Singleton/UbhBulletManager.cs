﻿using System.Collections.Generic;

/// <summary>
/// Manager of UbhBullet
/// </summary>
public class UbhBulletManager : UbhSingletonMonoBehavior<UbhBulletManager>
{
    private List<UbhBullet> _BulletList;

    private void Update()
    {
        if (_BulletList == null)
        {
            return;
        }

        // update bullet move
        for (int i = 0; i < _BulletList.Count; i++)
        {
            if (_BulletList[i] == null || _BulletList[i].enabled == false)
            {
                continue;
            }
            _BulletList[i].UpdateMove();
        }
    }

    /// <summary>
    /// Add bullet
    /// </summary>
    public void AddBullet(UbhBullet bullet)
    {
        if (_BulletList == null)
        {
            _BulletList = new List<UbhBullet>(4096);
        }

        for (int i = 0; i < _BulletList.Count; i++)
        {
            if (_BulletList[i] == null)
            {
                _BulletList[i] = bullet;
            }
        }

        _BulletList.Add(bullet);
    }
}
