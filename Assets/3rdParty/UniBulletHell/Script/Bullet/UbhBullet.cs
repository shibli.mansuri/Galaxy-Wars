﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Ubh bullet.
/// </summary>
public class UbhBullet : UbhMonoBehaviour
{
    private float speed;
    private float angle;
    private float accelSpeed;
    private float accelTurn;
    private bool homing;
    private Transform homingTarget;
    private float homingAngleSpeed;
    private bool wave;
    private float waveSpeed;
    private float waveRangeSize;
    private bool pauseAndResume;
    private float pauseTime;
    private float resumeTime;
    private UbhUtil.AXIS axisMove;

    private float selfFrameCnt;
    private float selfTimeCount;

    private bool addedManageList;

    
	void Start()
	{

		print ("bullet instantiating");
	}

	public bool shooting
    {
        get;
        private set;
    }

    private void OnDisable()
    {
        transform.ResetPosition();
        transform.ResetRotation();
        shooting = false;
    }

    /// <summary>
    /// Bullet Shot
    /// </summary>
    public void Shot(float speed, float angle, float accelSpeed, float accelTurn,
                      bool homing, Transform homingTarget, float homingAngleSpeed,
                      bool wave, float waveSpeed, float waveRangeSize,
                      bool pauseAndResume, float pauseTime, float resumeTime, UbhUtil.AXIS axisMove)
    {
        if (addedManageList == false)
        {
            // Add to bullet manager
            addedManageList = true;
            UbhBulletManager.Instance.AddBullet(this);
        }

        if (shooting)
        {
            return;
        }
        shooting = true;

        this.speed = speed;
        this.angle = angle;
        this.accelSpeed = accelSpeed;
        this.accelTurn = accelTurn;
        this.homing = homing;
        this.homingTarget = homingTarget;
        this.homingAngleSpeed = homingAngleSpeed;
        this.wave = wave;
        this.waveSpeed = waveSpeed;
        this.waveRangeSize = waveRangeSize;
        this.pauseAndResume = pauseAndResume;
        this.pauseTime = pauseTime;
        this.resumeTime = resumeTime;
        this.axisMove = axisMove;

        if (axisMove == UbhUtil.AXIS.X_AND_Z)
        {
            // X and Z axis
            transform.SetEulerAnglesY(-angle);
        }
        else
        {
            // X and Y axis
            transform.SetEulerAnglesZ(angle);
        }

        selfFrameCnt = 0f;
        selfTimeCount = 0f;
    }

    /// <summary>
    /// Update Move
    /// </summary>
    public void UpdateMove()
    {


        if (shooting == false)
        {
            return;
        }

        selfTimeCount += UbhTimer.Instance.DeltaTime;

        // pause and resume.
        if (pauseAndResume && pauseTime >= 0f && resumeTime > pauseTime)
        {
            if (pauseTime <= selfTimeCount && selfTimeCount < resumeTime)
            {
                return;
            }
        }

        if (homing)
        {
            // homing target.
            if (homingTarget != null && 0f < homingAngleSpeed)
            {
                float rotAngle = UbhUtil.GetAngleFromTwoPosition(transform, homingTarget, axisMove);
                float myAngle = 0f;
                if (axisMove == UbhUtil.AXIS.X_AND_Z)
                {
                    // X and Z axis
                    myAngle = -transform.eulerAngles.y;
                }
                else
                {
                    // X and Y axis
                    myAngle = transform.eulerAngles.z;
                }

                float toAngle = Mathf.MoveTowardsAngle(myAngle, rotAngle, UbhTimer.Instance.DeltaTime * homingAngleSpeed);

                if (axisMove == UbhUtil.AXIS.X_AND_Z)
                {
                    // X and Z axis
                    transform.SetEulerAnglesY(-toAngle);
                }
                else
                {
                    // X and Y axis
                    transform.SetEulerAnglesZ(toAngle);
                }
            }

        }
        else if (wave)
        {
            // acceleration turning.
            angle += (accelTurn * UbhTimer.Instance.DeltaTime);
            // wave.
            if (0f < waveSpeed && 0f < waveRangeSize)
            {
                float waveAngle = angle + (waveRangeSize / 2f * Mathf.Sin(selfFrameCnt * waveSpeed / 100f));
                if (axisMove == UbhUtil.AXIS.X_AND_Z)
                {
                    // X and Z axis
                    transform.SetEulerAnglesY(-waveAngle);
                }
                else
                {
                    // X and Y axis
                    transform.SetEulerAnglesZ(waveAngle);
                }
            }
            selfFrameCnt++;

        }
        else
        {
            // acceleration turning.
            float addAngle = accelTurn * UbhTimer.Instance.DeltaTime;
            if (axisMove == UbhUtil.AXIS.X_AND_Z)
            {
                // X and Z axis
                transform.AddEulerAnglesY(-addAngle);
            }
            else
            {
                // X and Y axis
                transform.AddEulerAnglesZ(addAngle);
            }
        }

        // acceleration speed.
        speed += (accelSpeed * UbhTimer.Instance.DeltaTime);

        // move.
        if (axisMove == UbhUtil.AXIS.X_AND_Z)
        {
            // X and Z axis
            transform.position += transform.forward.normalized * speed * UbhTimer.Instance.DeltaTime;
        }
        else
        {
            // X and Y axis
            transform.position += transform.up.normalized * speed * UbhTimer.Instance.DeltaTime;
        }
    }
}