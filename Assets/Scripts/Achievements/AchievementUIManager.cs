﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class AchievementUIManager : MonoBehaviour {

    public AchievementView _view;
    private AchievementController _controller;

    void Start()
    {
        Initialize();
        PopulateAchievements();
    }

    private void Initialize()
    {
        //assign controller
        _controller = new AchievementController();
    }

    private void PopulateAchievements()
    {
        // create panel for all achievements present in the achievements controller
        for (int i = 0; i < _controller.Achievements.Count; i++)
        {
            int index = i;
            _view.CreatePanel(_controller.Achievements[index], index);
        }
    }

    public void AchievementClaimed(int index)
    {
        _controller.ClaimRewards(index);
        RepopulateAchievements();
    }

    public void RepopulateAchievements()
    {
        _view.RemoveAllAchievements();
        PopulateAchievements();
    }
}