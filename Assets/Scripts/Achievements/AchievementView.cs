﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementView : MonoBehaviour {

    public AchievementUIManager _manager;
    public GameObject achievementCellPrefab;
    public List<GameObject> achievementPanels;
    private List<AchievementCellUI> achievementCellsUI;
    [SerializeField]
    private Transform content;

    private void Awake()
    {
        Initialize(); 
    }

    public void Initialize()
    {
        achievementCellsUI = new List<AchievementCellUI>();
        
        // initialize list holding all chest panels
        achievementPanels = new List<GameObject>();

        achievementCellPrefab = Resources.Load(DataConstants.ACHIEVEMENTS_CELL_UI_PATH) as GameObject;

    }

    public void CreatePanel(AchievementModel achievement, int index)
    {
        GameObject panel = Instantiate(achievementCellPrefab, content);
        AchievementCellUI panelInfo = panel.GetComponent<AchievementCellUI>();

        int levelToClaim = achievement.GetMinimumUnclaimedLevel();
        int quantity = GameStatistics.instance.GetStat(achievement.Stat);
        
        //set cell information
        panelInfo.Name.text = achievement.Name;
        panelInfo.LevelInfo.text = "Level " + (achievement.GetMinimumUnclaimedLevel()+1) + "/" + (achievement.MaxLevel+1);
        panelInfo.RewardList.text = achievement.PayOut[achievement.GetMinimumUnclaimedLevel()].ToString();
        panelInfo.Slider.minValue = achievement.Limits[levelToClaim].x;
        panelInfo.Slider.maxValue = achievement.Limits[levelToClaim].y;

        if (panelInfo.Slider.minValue == panelInfo.Slider.maxValue)
            panelInfo.Slider.minValue--;

        panelInfo.Slider.value = quantity;

        panel.name = "Achievement Cell#" + index;
        achievementPanels.Add(panel);
        achievementCellsUI.Add(panelInfo);

        if (quantity >= achievement.Limits[levelToClaim].y)
        {
            DelegateClaimFunctionality(index);
        }

    }

    private void DelegateClaimFunctionality(int index)
    {
        Button button = achievementCellsUI[index].Button;
        button.interactable = true;
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(delegate {
            _manager.AchievementClaimed(index);
            // ShowClaimPanel(index);
        });
    }

   /* private void ShowClaimPanel(int index)
    {
        AchievementCellUI achievementInfo = achievementCellsUI[index];
        GameObject claimPanel = Instantiate(_claimPanelPrefab, content.parent.parent);
        ClaimPanelUI claimPanelInfo = claimPanel.GetComponent<ClaimPanelUI>();
        Button button = claimPanelInfo.Button;

        claimPanelInfo.Heading.text = achievementInfo.Name.text;
        claimPanelInfo.RewardList.text = achievementInfo.RewardList.text;

        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(delegate{
            Destroy(claimPanel);
            _manager.AchievementClaimed(index);
           
        });
    }*/

    public void RemoveAllAchievements()
    {
        for (int i = 0; i < achievementPanels.Count; i++)
        {
            Destroy(achievementPanels[i]);
            Destroy(achievementCellsUI[i]);
        }

        achievementPanels = new List<GameObject>();
        achievementCellsUI = new List<AchievementCellUI>();
    }
}
