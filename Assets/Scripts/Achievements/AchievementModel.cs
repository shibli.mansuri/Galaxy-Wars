﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AchievementModel  {
	public string _name;
    public int _maxLevel;
    public int _currentLevel;      // _currentLevel is a derived attribute
    public List <RewardItems> _payOut;
    public List <Vector2> _limits;
    public GameStatistics.Stat _stat;

	//constructor
	public AchievementModel(string name, int maxLevel, List<RewardItems> payout, List<Vector2> limits, GameStatistics.Stat stat)
	{
		_name = name;
		_maxLevel = maxLevel;
		_payOut = payout;
		_limits = limits;
		_stat = stat;
        _currentLevel = CurrentLevel;
	}

    public AchievementModel(AchievementModelToJSON jsonModel)
    {
        _name = jsonModel._name;
        _maxLevel = jsonModel._maxLevel;
        _currentLevel = jsonModel._currentLevel;
        _payOut = jsonModel._payOut;
        _limits = jsonModel._limits;
        _stat = (GameStatistics.Stat)jsonModel._stat;
    }

    public int GetMinimumUnclaimedLevel()
    {
        int i = 0;
        while(this._payOut[i].isEmpty()!=false && i<=_currentLevel)
        {
            i++;
        }
        return i;
    }

	public bool hasReachedMaxLevel()
	{
		return _currentLevel >= _maxLevel;	
	}

    public string GetAsString()
    {
        return JsonUtility.ToJson(new AchievementModelToJSON(this));
    }

	public string Name {
		get {
			return _name;
		}
		set {
			_name = value;
		}
	}

	public int MaxLevel {
		get {
			return _maxLevel;
		}
		set {
			_maxLevel = value;
		}
	}

	public int CurrentLevel {
		get {
			if (!hasReachedMaxLevel ()) {

				for (int i = 0; i <= _maxLevel; i++) {
					if (GameStatistics.instance.GetStat (_stat) >= _limits [i].x && GameStatistics.instance.GetStat (_stat) <= _limits [i].y) {
						 _currentLevel = i ;
					}
				}
			} else
				 _currentLevel = _maxLevel;

			return _currentLevel;
			
		}
		set {
			_currentLevel = value;
		}
	}

	public List<RewardItems> PayOut {
		get {
			return _payOut;
		}
		set {
			_payOut = value;
		}
	}

	public List<Vector2> Limits {
		get {
			return _limits;
		}
		set {
			_limits = value;
		}
	}

	public GameStatistics.Stat Stat {
		get {
			return _stat;
		}
		set {
			_stat = value;
		}
	}
}
[Serializable]
public struct AchievementModelToJSON
{
   public string _name;
   public int _maxLevel;
   public int _currentLevel;  
   public List<RewardItems> _payOut;
   public List<Vector2> _limits;
   public int _stat;

    public AchievementModelToJSON(AchievementModel model)
    {
        _name = model.Name;
        _maxLevel = model.MaxLevel;
        _currentLevel = model.CurrentLevel;
        _payOut = model.PayOut;
        _limits = model.Limits;
        _stat = (int)model.Stat;
    }
}
