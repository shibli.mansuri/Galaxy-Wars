﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementCellUI : MonoBehaviour {

    public Text _name;
    public Text _rewardList;
    public Text _levelInfo;
    public Slider _slider;
    public Button _button;


    public Text Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public Text RewardList
    {
        get
        {
            return _rewardList;
        }

        set
        {
            _rewardList = value;
        }
    }

    public Text LevelInfo
    {
        get
        {
            return _levelInfo;
        }

        set
        {
            _levelInfo = value;
        }
    }

    public Slider Slider
    {
        get
        {
            return _slider;
        }

        set
        {
            _slider = value;
        }
    }

    public Button Button
    {
        get
        {
            return _button;
        }

        set
        {
            _button = value;
        }
    }
}
