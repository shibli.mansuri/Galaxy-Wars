﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementController  {

	private List<AchievementModel> achievements;

    public AchievementController()
    {
        achievements = new List<AchievementModel>();
     
        if (PlayerPrefsManager.GetAchievements() == "{}")
        {
            GameObject loader = Resources.Load(DataConstants.ACHIEVEMENTS_LOADER_PATH) as GameObject;
            achievements = loader.GetComponent<AchievementLoader>().achievementList;
            SaveAchievements();
        }
        else
            LoadAchievements();

     }

    public void AddAchievement(AchievementModel achievement)
    {
        achievements.Add(achievement);
    }

    public void LoadAchievements()
    {
        achievements = new List<AchievementModel>();
        string jsonString = PlayerPrefsManager.GetAchievements();
        if (jsonString != "{}" && jsonString != "")
        {
            char[] separator = { '*' };
            string[] achievementStrings = jsonString.Split(separator);
            for (int i = 0; i < achievementStrings.Length; i++)
            {
                AchievementModelToJSON jsonModel = JsonUtility.FromJson<AchievementModelToJSON>(achievementStrings[i]);
                achievements.Add(new AchievementModel(jsonModel));
            }
        }
    }

    public void SaveAchievements()
    {
        string jsonString = "";
        for (int i = 0; i < achievements.Count; i++)
        {
            jsonString += achievements[i].GetAsString();
            if (i != achievements.Count - 1)
                jsonString += '*';
        }
        PlayerPrefsManager.SetAchievements(jsonString);
    }

    public void ClaimRewards(int index)
    {
        //get the specific achievement
        AchievementModel model = achievements[index];

        //get rewards that need to be cashed
        RewardItems rewards = model.PayOut[model.GetMinimumUnclaimedLevel()];

        //add rewards to player inventory
        PlayerInventory.instance.AddReward(rewards);

        // empty rewards
        model.PayOut[model.GetMinimumUnclaimedLevel()] = new RewardItems();

        CheckAchievementStatus(index);

        SaveAchievements();
        LoadAchievements();
    }

    public void CheckAchievementStatus(int index)
    {
        //if all rewards are collected of the achievement
        if (achievements[index].CurrentLevel == achievements[index].CurrentLevel && achievements[index].PayOut[achievements[index].MaxLevel].isEmpty())
        {
            achievements.RemoveAt(index);
        }
    }

    public List<AchievementModel> Achievements
    {
        get
        {
            return achievements;
        }

        set
        {
            achievements = value;
        }
    }
}
