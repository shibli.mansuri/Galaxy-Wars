﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameStatistics : MonoBehaviour {

	public enum Stat
    {
        TotalCoins,
        TotalODFrags,
        TotalPowerGems,
        EnemyKills,
		EnemySpawns,
        LevelsPlayed,
        BossesDefeated
    }

	public int noOfAttempts=1;
	public int currentWaveSpawned = 0;

	public static GameStatistics instance;
	private int[] statistics;

    public int levelButton;

	void Awake(){

		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else if (instance != this)
			Destroy (gameObject);

	}

	void OnEnable()
	{
		Initialize ();
        //PlayerPrefsManager.DeleteAll();
        //statistics[(int)Stat.TotalCoins] = 0;
        //statistics[(int)Stat.TotalODFrags] = 0;
        //statistics[(int)Stat.TotalPowerGems] = 0;
        //statistics[(int)Stat.TurretEnemyKills] = 0;
        //statistics[(int)Stat.BossesDefeated] = 0;
        //SaveStatistics();
        LoadFromPlayerPrefs();

	}

	void Initialize()
	{
		statistics = new int[(Enum.GetValues(typeof(Stat)).Length)];
	}

	public void AddToStat(GameStatistics.Stat stat, int value)
	{
		statistics [(int)stat] += value;
        SaveStatistics();

    }

    public void SetStat(GameStatistics.Stat stat, int value)
    {
        statistics[(int)stat] = value;
        SaveStatistics();
    }

	public int GetStat(GameStatistics.Stat stat)
    {
		return statistics [(int)stat];
	}

	public string ConvertToString()
	{
		string resultString = "";
		for (int i = 0; i < statistics.Length; i++) {
			resultString += statistics [i].ToString ();
			if(i != statistics.Length-1)
				resultString+="*";
		}

		return resultString;
	}

    private void SaveStatistics()
    {
        PlayerPrefsManager.SetStatistics(ConvertToString());
    }

    private void LoadFromPlayerPrefs()
    {
        string statsString = PlayerPrefsManager.GetStatistics();
        if(statsString != "")
        {
            string[] numbers = statsString.Split('*');
            for (int i = 0; i < statistics.Length; i++)
            {
                statistics[i] = int.Parse(numbers[i]);
            }
        }
    }
}