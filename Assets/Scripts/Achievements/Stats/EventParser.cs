﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventParser : MonoBehaviour {

    public static EventParser instance;
    AchievementController _controller;
    public GamePlayUIView _view;

    void Awake()
    {
        if (instance == null)
            instance = GameObject.FindObjectOfType<EventParser>();
        else if (instance != null)
            Destroy(this.gameObject);

    }

    void OnLevelWasLoaded()
    {
        _view = GameObject.FindObjectOfType<GamePlayUIView>().GetComponent<GamePlayUIView>();
    }

    void Start()
    {
        _controller = new AchievementController();
    }

    public void AddStat(GameStatistics.Stat stat, int value)
    {
        if (_controller.Achievements.Find(model => model.Stat == stat)!= null)
        {
            int levelBefore = _controller.Achievements.Find(model => model.Stat == stat).CurrentLevel;
            //Debug.Log("Level Before: " + levelBefore + " // Coins: " + GameStatistics.instance.GetStat(stat));

            GameStatistics.instance.AddToStat(stat, value);

            int levelAfter = _controller.Achievements.Find(model => model.Stat == stat).CurrentLevel;
            //Debug.Log("Level After: " + levelAfter + " // Coins: " + GameStatistics.instance.GetStat(stat));

            if (levelBefore != levelAfter)
            {
                _view.ShowAchievementAlert();
                //Debug.Log("Hey! You just unlocked an achievement");
            }
        }
    }

    // set absoulute value of a stat
    public void SetStat(GameStatistics.Stat stat, int value)
    {
        if (_controller.Achievements.Find(model => model.Stat == stat) != null)
        {
            int levelBefore = _controller.Achievements.Find(model => model.Stat == stat).CurrentLevel;
            GameStatistics.instance.SetStat(stat, value);
            int levelAfter = _controller.Achievements.Find(model => model.Stat == stat).CurrentLevel;

            if (levelBefore != levelAfter)
            {
                _view.ShowAchievementAlert();
                _controller.SaveAchievements();
                _controller.LoadAchievements();
            }
        }
    }


}
