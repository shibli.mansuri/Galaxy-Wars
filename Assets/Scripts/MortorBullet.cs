﻿using UnityEngine;
public class MortorBullet : MonoBehaviour
{

	public float speed;
	public float behaviourSwitchDuration;
	bool upwardMovement;
	public float lifeTime;
	public GameObject explosionParticle;
	public AudioClip explosiveSound;
	private CircleCollider2D collider;
	public int  damage=5;

	void Start()
	{
		InvokeRepeating ("GoReverseFarward", behaviourSwitchDuration, behaviourSwitchDuration);
		InvokeRepeating ("GoLeftRight", 3, 3);
		Invoke ("DoExplosions", lifeTime);
		collider = GetComponent<CircleCollider2D> ();
		collider.enabled = false;
	}

	void FixedUpdate () 
	{ 
		var pos = transform.position;
		if (upwardMovement) {
			pos.y += speed * Time.deltaTime;
			transform.position = pos; 
		} else 
		{
			pos.x += speed * Time.deltaTime;
			transform.position = pos; 
		}
	} 
	void GoReverseFarward()
	{
		speed = speed*(-1);
	}

	void GoLeftRight()
	{
		upwardMovement = !upwardMovement;
	}

	public void DoExplosions()
	{
		//CancelInvoke ("DoLeftRight");
		//CancelInvoke ("DoReverse");
		collider.enabled = true;
		if(explosionParticle)
		{
			GameObject explosion;
			explosion = Instantiate (explosionParticle, transform.position,Quaternion.identity,gameObject.transform) as GameObject;
			explosion.SetActive (true);
			GetComponent<SpriteRenderer> ().enabled = false;
			Destroy (explosion, 2);
		}
		if (explosiveSound) 
		{
			AudioManager.instance.PlaySingle (explosiveSound, 0.75f);
			print ("Played the sound");

		}

		Destroy (gameObject,1.5f);
	}
}
