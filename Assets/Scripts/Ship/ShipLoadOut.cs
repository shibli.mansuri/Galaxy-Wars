﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]//so we can convert this to JSON and save it
public class ShipLoadOut
{

    List<GunSocket> _gunSlots; //0 primary, 1 secondary, rest are assists
    int _maxGuns = 8;
    GunType _highestGunUnlocked = GunType.Laser;

    public ShipLoadOut()
    {
        // cannot initialize here as this class is serialized and 
        //initialization function calls getstring on playerprefs 
        //which throws an error during serialization
    }

    public void Initialize()
    {
        //iniTialize the load out here
        //if loadout doesnt exist in playerprefs then initialize new one
        if (!TryLoadGunConfig())
        {
            //make the loadout for shipEvoLevel1
            _gunSlots = new List<GunSocket>(_maxGuns);

            _gunSlots.AddRange(new GunSocket[]{
                                                new GunSocket(SocketState.Unlocked),
                                                new GunSocket(SocketState.Locked),
                                                new GunSocket(SocketState.Locked),
                                                new GunSocket(SocketState.Locked),
                                                new GunSocket(),
                                                new GunSocket(),
                                                new GunSocket(),
                                                new GunSocket()
                                            }
            );

            //Debug.Log("Created new ship loadout");
            SaveGunConfig();
        }
    }

    public void SaveGunConfig()//writetoPlayerprefs
    {
        LoadOutSerializer toSave = new LoadOutSerializer(_gunSlots,_highestGunUnlocked);
        string jsonData = JsonUtility.ToJson(toSave);
        PlayerPrefsManager.SetShipLoadOut(jsonData);
        PlayerPrefsManager.Save();
    }

    public bool TryLoadGunConfig()// read from PlayerPrefs
    {
        string jsonData = PlayerPrefsManager.GetShipLoadOut();
        if (string.IsNullOrEmpty(jsonData) || jsonData == "{}")
            return false;
        else
        {
            LoadOutSerializer loadOut = JsonUtility.FromJson<LoadOutSerializer>(jsonData);
            _gunSlots = new List<GunSocket>(loadOut._gunSockets);
            _highestGunUnlocked = loadOut._highestGunUnlocked;

            //TODO: remove the lines below, they are for testin
            //Debug.Log("Deleting Ship Loadout from playerprefs!!!");
            //PlayerPrefsManager.SetShipLoadOut("");
            //PlayerPrefsManager.Save();
            /////
            return true;
        }

    }

    public SocketState GetSocketState(int index)
    {
        CheckIndex(index);
        return _gunSlots[index].GetState();
    }

    public void UnlockSocket(int index)
    {
        CheckIndex(index);
        _gunSlots[index].UnlockSocket();
    }

    public GunType GetGunTypeAt(int index)
    {
        CheckIndex(index);
        return _gunSlots[index].GetGunType();
    }

    public void SetGunTypeAt(int index, GunType newType)
    {
        _gunSlots[index].SetGunType(newType);
    }

    void CheckIndex(int index)
    {
         if (index >= _gunSlots.Count)
            Debug.LogError("Index out of range. Guns = " + _gunSlots.Count + " | index = " + index);
    }

    public int GunCount
    {
        get
        {
            return _gunSlots.Count;
        }
    }

    public GunType HighestGunUnlocked
    {
        get
        {
            return _highestGunUnlocked;
        }
    }

    public bool IsGunUnlocked(GunType gun) {
        return (int)gun <= (int)_highestGunUnlocked;
    }

	public List<GunSocket> GetGunSockets()
	{
		return _gunSlots;
	}
}

[Serializable]
public class LoadOutSerializer
{
    public List<GunSocket> _gunSockets;
    public GunType _highestGunUnlocked;

    public LoadOutSerializer(List<GunSocket> collection, GunType gunsUnlocked)
    {
        _gunSockets = new List<GunSocket>(collection);
        _highestGunUnlocked = gunsUnlocked;
    }
}
