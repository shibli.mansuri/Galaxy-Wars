﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class Waves : MonoBehaviour
{
    public bool debug_SpawnNext = false;
    [SerializeField]
    WaveParameters[] _waveParams;
    [SerializeField]
    int _currentWaveIndex;
    IEnumerator _spawnWaveCoroutine;//just holds the last spawning coroutine called, 
                                    //there might be more than one routine running at the same time 
                                    //so don't expect to stop all routines by stopping this one
                                    //Convert this to a list of IEnumerators for that functionality
    int _enemiesToMonitor;//will hold the total amount of enemies we spawned and will be used
    //if the next wave has to spawn after all the enemies are dead


    private void Awake()
    {
        _currentWaveIndex = -1;//will be incremented before it is used each time, hence will start from 0
    }

    private void Update()
    {
        if (debug_SpawnNext)
        {
            SpawnNextWave();
            debug_SpawnNext = false;
        }
    }

    public void SpawnNextWave()
    {
        _currentWaveIndex++;// wave index starts from -1 so this is ok
        if (_currentWaveIndex < WaveParams.Length)
        {
            _spawnWaveCoroutine = SpawnWave(WaveParams[_currentWaveIndex]);
			GameStatistics.instance.currentWaveSpawned = _currentWaveIndex;
            StartCoroutine(_spawnWaveCoroutine);
        }
        else
        {
            EventParser.instance.SetStat(GameStatistics.Stat.LevelsPlayed, LevelManager.instance.levelNumber);
            Debug.Log("No waves left");
            //send a signal that the waves have finished
        }
    }

    public IEnumerator SpawnWave(WaveParameters waveParams)
    {
        // wait before starting if user has set startwait time
        if (waveParams.startWait > 0)
            yield return new WaitForSeconds(waveParams.startWait);

        //before spawning check how the next wave has to be spawned
        bool spawnNextAtEnd = false;
        switch (waveParams.spawnNextWhen)
        {
            case SpawnNextWhen.AfterTime:
                //wait for nextAfterTime and then spawn the next wave
                StartCoroutine(SpawnNextWaveAfter(waveParams.nextAfterTime));
                break;
            case SpawnNextWhen.AllDestroyed:
                if(_enemiesToMonitor==0)//if we're not already monitoring enemies attach a listener to the enemy death event
                    EventHub.AttachListener(DataConstants.STR_EVENT_ENEMY_DESTROYED, OnEnemyDeath);
                _enemiesToMonitor += waveParams.count;
                break;
            case SpawnNextWhen.AllSpawned:
                spawnNextAtEnd = true;//wait till all the units have spawned then spawn the next wave
                break;
            case SpawnNextWhen.Started:
                SpawnNextWave();//spawn the next wave now
                break;
        }

		int spawnedEnemyCount = 0;
		int enemyToActivatPowerUp=UnityEngine.Random.Range(0,waveParams.count-1);
		GameObject lastSpawnedEnemy=null;
        while (spawnedEnemyCount != waveParams.count)
        {
            if(waveParams.movementMode== MovementMode.NoMovement)//dont need to recheck movemement mode for each spawn, unoptimized but easy to read
            {
                if (spawnedEnemyCount<waveParams.positions.Length)
					lastSpawnedEnemy =	CreateEnemyAtPosition(waveParams.prefab, waveParams.positions[spawnedEnemyCount]);
                else
                    Debug.LogError("WaveSpawner: Positions for new spawns don't exist");
            }else if(waveParams.movementMode == MovementMode.BezierPath)
            {
                if (spawnedEnemyCount == 0)//this is the first spawn we should set the bezier paths position
                    InitializeBezierPathPosition(waveParams);
                if(waveParams.bezierPath!=null)
					lastSpawnedEnemy = CreateEnemyFollowingPath(waveParams.prefab, waveParams.bezierPath.GetComponent<BezierSpline>(),waveParams.duration);
                else
                    Debug.LogError("WaveSpawner: Bezier Path for movement doesn't exist");
            }
			if (waveParams.hasPowerUpReward&&spawnedEnemyCount==enemyToActivatPowerUp) 
			{
				if(!lastSpawnedEnemy.Equals(null))
				lastSpawnedEnemy.GetComponent<BaseEnemy> ().ActivatePowerUp ();
			}
			// to spawn health collectible on thelast enemy in wave
			if (spawnedEnemyCount == waveParams.count-1) 
			{
				
				if (waveParams.hasHealthCollectibleReward){
					lastSpawnedEnemy.GetComponent<BaseEnemy> ().ActivateHealthCollectible ();
				}
			}
            spawnedEnemyCount++;
			EventHub.TriggerEvent("EnemySpawned");
			yield return new WaitForSeconds(waveParams.inbetweenWait);
        }
        if (spawnNextAtEnd)
        {
            SpawnNextWave();
        }
    }

    IEnumerator SpawnNextWaveAfter(float delay)
    {
        yield return new WaitForSeconds(delay);
        SpawnNextWave();
    }

	GameObject CreateEnemyAtPosition(GameObject prefab, Vector2 pos)
    {
        GameObject enemy = Instantiate(prefab);
        enemy.transform.position = GetPositionFromScale(pos);
		return enemy;
    }

    Vector3 GetPositionFromScale(Vector2 pos)
    {
        Vector3 newPos = Helper.GetPosRelativeCamera(pos.x, pos.y);
        newPos.z = 0;
        return newPos;
    }

	GameObject CreateEnemyFollowingPath(GameObject prefab, BezierSpline path,float duration)
    {
        GameObject enemy = Instantiate(prefab);
        SplineWalkerEnemy splineWalker = enemy.GetComponent<SplineWalkerEnemy>();
        splineWalker.InitSplineWalker(duration, path);
		return enemy;
    }

    private void InitializeBezierPathPosition(WaveParameters waveParams)
    {
        if (waveParams.positions == null)
            Debug.LogError("WaveSpawner: Position for Bezier Path doesnt exist");
        else
        {
            GameObject bezierPathGO = Instantiate(waveParams.bezierPath);
            waveParams.bezierPath = bezierPathGO;
            waveParams.bezierPath.transform.position = GetPositionFromScale(waveParams.positions[0]);
        }
        waveParams.bezierPath.transform.parent = Camera.main.transform;
    }

    void OnEnemyDeath()
    {
        _enemiesToMonitor--;
		if (_enemiesToMonitor == 0)//spawn the next wave
        {
            EventHub.DetachListener(DataConstants.STR_EVENT_ENEMY_DESTROYED, OnEnemyDeath);
            SpawnNextWave();
        }else if (_enemiesToMonitor < 0){//should not end up here, either the monitoring didn't stop when the enemies were 0 or the function was registered as a listener twice

            Debug.LogError("Enemies to monitor is less than 0");
        }
    }

    public WaveParameters[] WaveParams
    {
        get
        {
            return _waveParams;
        }

        set
        {
            _waveParams = value;
        }
    }
}

[Serializable]
public class WaveParameters {
    [AssetList(Tags = "Enemy", Path = "/Prefabs", AutoPopulate = true)]
    [InlineEditor(InlineEditorModes.SmallPreview)]
    public GameObject prefab;
    [LabelText("Quantity"), OnValueChanged("SetPositionsArraySizeForImmovable")]
    public int count;
    [LabelText("Start Delay")]
    public float startWait = 0;
    [LabelText("Interval")]
    public float inbetweenWait = 0.2f;
	[EnumToggleButtons]
    public SpawnNextWhen spawnNextWhen = SpawnNextWhen.AllSpawned;
    [ShowIf("isSpawnNextWhenAfterTime"), Indent]
    public float nextAfterTime = 0;
    [EnumToggleButtons, OnValueChanged("SetPositionsArraySizeForMovable")]
    public MovementMode movementMode = MovementMode.BezierPath;
    [AssetList(Tags = "BezierPath", Path = "/Prefabs/Waves/Bezier Path",  AutoPopulate =true), ShowIf("isMovementModeBezeierPath"), Indent]
    public GameObject bezierPath;
	[LabelText("Duration"),ShowIf("isMovementModeBezeierPath"),Indent]
	public float duration=5f;//-1 means don't override the default in the SplineWalkerEnemy class

    public Vector2[] positions;
    public RewardDrop rewards;
	public  bool hasPowerUpReward;
	public  bool hasHealthCollectibleReward;
	public WaveParameters(WaveParameters waveParams)
    {
       

        prefab = waveParams.prefab;
        count = waveParams.count;
        startWait = waveParams.startWait;
        inbetweenWait = waveParams.inbetweenWait;
        spawnNextWhen = waveParams.spawnNextWhen;
        nextAfterTime = waveParams.nextAfterTime;
        movementMode = waveParams.movementMode;
        bezierPath = waveParams.bezierPath;
        positions = new Vector2[waveParams.positions.Length];
		hasPowerUpReward = waveParams.hasPowerUpReward;
		hasHealthCollectibleReward=waveParams.hasHealthCollectibleReward;
		duration = waveParams.duration;
        
        for (int i=0; i<waveParams.positions.Length; i++)
        {
            positions[i] = new Vector2(waveParams.positions[i].x, waveParams.positions[i].y);
        }
    }

    public WaveParameters()
    {
        prefab = null;
        count = 0;
        startWait = 0;
        inbetweenWait = 0;
        spawnNextWhen = SpawnNextWhen.Started;
        nextAfterTime = 0;
        movementMode = MovementMode.BezierPath;
        bezierPath = null;
        positions = new Vector2[0];
		hasPowerUpReward = false;
		hasHealthCollectibleReward=false;
    }

    private bool isMovementModeBezeierPath()
    {
        return true ? movementMode == MovementMode.BezierPath : movementMode != MovementMode.BezierPath;
    }

    private bool isSpawnNextWhenAfterTime()
    {
        return true ? spawnNextWhen == SpawnNextWhen.AfterTime : spawnNextWhen!=SpawnNextWhen.AfterTime; 
    }

    private void SetPositionsArraySizeForImmovable()
    {
        Vector2[] oldPositions = positions;
        if(movementMode == MovementMode.NoMovement)
            positions = new Vector2[count];
        else
            positions = new Vector2[1];
        AddOldPositions(oldPositions);
    }

    private void SetPositionsArraySizeForMovable()
    {
        Vector2[] oldPositions = positions;
        if (movementMode == MovementMode.BezierPath)
            positions = new Vector2[1];
        else
            positions = new Vector2[count];
        AddOldPositions(oldPositions);
    }

    private void AddOldPositions(Vector2[] oldPositions)
    {
        int limit = oldPositions.Length < positions.Length ? oldPositions.Length : positions.Length;
        for (int i = 0; i < limit; i++)
            positions[i] = oldPositions[i];
    }
}

public enum MovementMode
{
    NoMovement,
    BezierPath,
}

public enum SpawnNextWhen 
{
    AllSpawned,
    Started,
    AllDestroyed,
    AfterTime,
}

public struct RewardDrop
{
    int coins;
}
