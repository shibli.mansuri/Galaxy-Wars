﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//[CustomPropertyDrawer(typeof(WaveParameters))]
public class WaveParamPropertyDrawer : PropertyDrawer {

	SerializedProperty startWait, spawnNextWhen, nextAfterTime, 
        inbetweenWait, prefab, movementMode, bezierPath, 
        positions, count, duration;


	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label){

        
		startWait = property.FindPropertyRelative ("startWait");
		spawnNextWhen = property.FindPropertyRelative ("spawnNextWhen");
		nextAfterTime = property.FindPropertyRelative ("nextAfterTime");
		inbetweenWait = property.FindPropertyRelative ("inbetweenWait");
		prefab = property.FindPropertyRelative ("prefab");
		movementMode = property.FindPropertyRelative ("movementMode");
		positions = property.FindPropertyRelative ("positions");
		count = property.FindPropertyRelative ("count");
		bezierPath = property.FindPropertyRelative ("bezierPath");
        duration = property.FindPropertyRelative("duration");

		position.height = EditorGUIUtility.singleLineHeight;
		EditorGUI.indentLevel-=1;
		EditorGUI.PropertyField (new Rect (position.x, position.y, position.width, position.height ), startWait, new GUIContent("Start Delay"));
		position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

		EditorGUI.PropertyField (new Rect (position.x, position.y , position.width, position.height ), spawnNextWhen, new GUIContent("Spawn Next"));
	
		position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

		if (spawnNextWhen.enumValueIndex == 3) { // Checks if spawnNextWhen == SpawnNextWhen.AfterTime
			EditorGUI.PropertyField (new Rect (position.x, position.y , position.width, position.height ), nextAfterTime, new GUIContent ("Next After Time"));
			position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
		}
		EditorGUI.PropertyField (new Rect (position.x, position.y , position.width, position.height), inbetweenWait, new GUIContent("In Between Wait"));
		position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

		EditorGUI.ObjectField (new Rect (position.x, position.y, position.width, position.height ), prefab, new GUIContent("Prefab"));
		position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

		EditorGUI.PropertyField (new Rect (position.x, position.y , position.width, position.height), movementMode, new GUIContent("Movement Mode"));
		position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

		if (movementMode.enumValueIndex == 1) {// checks if movementMode == MovementMode.BezeirPath
			EditorGUI.PropertyField (new Rect (position.x, position.y, position.width, position.height ), bezierPath, new GUIContent ("Bezier Path"));
			position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, position.height), duration, new GUIContent("Duration"));
            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
        }

		EditorGUI.PropertyField (new Rect (position.x, position.y , position.width, position.height ), count, new GUIContent("Count"));
		position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

		if (count.intValue > 0) {

			EditorGUI.PropertyField (new Rect (position.x, position.y, position.width, position.height ), positions, new GUIContent ("Positions"));
			position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

			for (int i = 0; i < positions.arraySize; i++) {
				EditorGUI.PropertyField (new Rect (position.x, position.y, position.width, position.height), positions.GetArrayElementAtIndex (i));
				position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
			}
			
			positions.isExpanded = true;

			if (movementMode.enumValueIndex == 0)
				positions.arraySize = count.intValue;
			if (movementMode.enumValueIndex == 1)
				positions.arraySize = 1;
			
			/*for(int i = 0; i<positions.arraySize+2; i++)
				position.y += position.height + EditorGUIUtility.standardVerticalSpacing;*/
		}

		EditorGUI.DrawRect(new Rect(position.x, position.y, position.width, position.height/8f), Color.black);
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label){

		int rows = 6;
		spawnNextWhen = property.FindPropertyRelative ("spawnNextWhen");
		movementMode = property.FindPropertyRelative ("movementMode");
		positions = property.FindPropertyRelative ("positions");

		if (spawnNextWhen.enumValueIndex == 3)
			rows++;
		if (movementMode.enumValueIndex == 1)
			rows++;
		if (positions.arraySize > 0)
			rows += (positions.arraySize+3);

		return base.GetPropertyHeight (property, label) + (rows*16) ;

	}

}