﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesTester : MonoBehaviour {

    public WaveParameters param;
    public WaveParameters[] paramArray;
    public Waves waveClass;
    private void Start()
    {
        waveClass.debug_SpawnNext = true;
    }
}
