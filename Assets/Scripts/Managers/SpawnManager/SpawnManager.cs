﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour
{
    ///Handles spawning waves for a level
    Waves _waves;
    bool _monitorEnemiesAlive = false;
    private void Awake()
    {
        _waves = GetComponent<Waves>();
        if (_waves == null)
            Debug.LogError("Spawnmanager requires a waves component. No Waves component Found!");

    }

    private void OnEnable()
    {
        
    }
    private void Start()
    {
        SpawnNextWave();
    }

    void SpawnNextWave()
    {
        _waves.SpawnNextWave();
    }

    

}
