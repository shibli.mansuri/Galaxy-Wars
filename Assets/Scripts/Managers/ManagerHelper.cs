﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerHelper : MonoBehaviour {

	public static ManagerHelper instance;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);
	}
}
