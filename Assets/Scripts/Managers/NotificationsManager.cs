﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Assets.SimpleAndroidNotifications;

public class NotificationsManager : MonoBehaviour {

	public static int ScheduleNotificationForChest(ChestModel chest)
	{
		#if UNITY_IPHONE
			SchedulueNotificatonForChestOniOS(chest);
			return -1;
		#elif UNITY_ANDROID
			return SchedulueNotificatonForChestOnAndroid(chest);
		#endif
		return 0;
	}

	public static void ClearScheduledNotificationForChest(ChestModel chest)
	{
		#if UNITY_IPHONE
		ClearScheduledNotificationForChestOniOS(chest);
		#elif UNITY_ANDROID
		ClearScheduledNotificationForChestOnAndroid(chest.GetNotificationID());
		#endif
	}

	#if UNITY_IPHONE
	private static void SchedulueNotificatonForChestOniOS(ChestModel chest)
	{
		
		UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification ();
		notification.fireDate = DateTime.Parse (chest.GetUnlockTime());
		notification.alertBody = chest.GetChestName () + " is unlocked! Claim now!";
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (notification);
	}


	private static void ClearScheduledNotificationForChestOniOS(ChestModel chest)
	{
		foreach (UnityEngine.iOS.LocalNotification notification in UnityEngine.iOS.NotificationServices.scheduledLocalNotifications) {
			if (notification.alertBody.Contains (chest.GetChestName ())) {
				UnityEngine.iOS.NotificationServices.CancelLocalNotification (notification);
				break;
			}
		}
	}
	#endif

	private static int SchedulueNotificatonForChestOnAndroid(ChestModel chest)
	{
		TimeSpan delay = DateTime.Parse (chest.GetUnlockTime ()) - DateTime.UtcNow;
		int notificationID = NotificationManager.SendWithAppIcon (delay, "Chest Unlocked!", chest.GetChestName () + " is unlocked! Claim now!", Color.red, NotificationIcon.Coin, false, "LoadChestScene");
		return notificationID;
	}

	private static void ClearScheduledNotificationForChestOnAndroid(int id)
	{
		NotificationManager.Cancel (id);
	}
}
