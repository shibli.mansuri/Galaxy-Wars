﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour {
	
	const string SHIELD_LEVEL_KEY = "shield_level";
	const string SHIELD_QUANTITY_KEY = "shield_quantity";
	const string MEGABOMB_LEVEL_KEY = "megabomb_level";
	const string MEGABOMB_QUANTITY_KEY = "megabomb_quantity";
	const string MAGNET_LEVEL_KEY = "magnet_level";
	const string COINS_KEY = "coins";
    const string ODFRAG_QUANTITY_KEY = "od_quant";
    const string POWERGEMS_QUANTITY_KEY = "powergems_quant";
    const string EVO_STONE_QUANTITY_KEY = "evostone_quant";
    const string SHIP_EVO_LEVEL_KEY = "ship_evo_level";
    const string SHIP_LOADOUT_STATE_KEY = "ship_loadout_settings";
	const string CHEST_LIST_KEY = "chest_list";
    const string ACHIEVEMENTS_KEY = "achievements";
    const string GAME_STATISTICS_KEY = "game_statistics";
    const string FIRST_RUN_KEY = "first_run";

    public static void SetFirstRun(int i = 1)
    {
        PlayerPrefs.SetInt(FIRST_RUN_KEY, i);
    }

    public static int GetFirstRun()
    {
        return PlayerPrefs.GetInt(FIRST_RUN_KEY, 0);
    }

	public static void SetCoins()
	{
		PlayerPrefs.SetInt(COINS_KEY, PlayerInventory.instance.GetCoins());
	}

	public static int GetCoins()
	{
		return PlayerPrefs.GetInt (COINS_KEY);
	}

	public static void SetMegabombLevel()
	{
		PlayerPrefs.SetInt (MEGABOMB_LEVEL_KEY, PlayerInventory.instance.GetMegabomb().GetLevel());
	}

	public static int GetMegabombLevel()
	{
		return PlayerPrefs.GetInt (MEGABOMB_LEVEL_KEY);
	}

	public static void SetMegabombQuantity()
	{
		PlayerPrefs.SetInt (MEGABOMB_QUANTITY_KEY, PlayerInventory.instance.GetMegabomb().GetQuantity());
	}

	public static int GetMegabombQuantity()
	{
		return PlayerPrefs.GetInt (MEGABOMB_QUANTITY_KEY, 0);
	}

	public static void SetShieldLevel()
	{
		PlayerPrefs.SetInt (SHIELD_LEVEL_KEY, PlayerInventory.instance.GetShield().GetLevel());
	}

	public static int GetShieldLevel()
	{
		return PlayerPrefs.GetInt (SHIELD_LEVEL_KEY,0);
	}

	public static void SetShieldQuantity()
	{
		PlayerPrefs.SetInt (SHIELD_QUANTITY_KEY, PlayerInventory.instance.GetShield().GetQuantity());
	}

	public static int GetShieldQuantity()
	{
		return PlayerPrefs.GetInt (SHIELD_QUANTITY_KEY,0);
	}

	public static void SetMagnetLevel(int level)
	{
		PlayerPrefs.SetInt (MAGNET_LEVEL_KEY, level);
	}

	public static int GetMagnetLevel()
	{
		return PlayerPrefs.GetInt (MAGNET_LEVEL_KEY);
	}

    public static void Save()
    {
        PlayerPrefs.Save();
    }

    public static int GetODFrag()
    {
        return PlayerPrefs.GetInt(ODFRAG_QUANTITY_KEY,0);
    }

    public static void SetODFRAG(int quantity)
    {
        PlayerPrefs.SetInt(ODFRAG_QUANTITY_KEY,quantity);
    }

    public static int GetPowerGems()
    {
        return PlayerPrefs.GetInt(POWERGEMS_QUANTITY_KEY,0);
    }

    public static void SetPowerGems(int quantity)
    {
        PlayerPrefs.SetInt(POWERGEMS_QUANTITY_KEY, quantity);
    }

    public static int GetEvolutionStone()
    {
        return PlayerPrefs.GetInt(EVO_STONE_QUANTITY_KEY,0);
    }

    public static void SetEvoStone(int quantity)
    {
        PlayerPrefs.SetInt(EVO_STONE_QUANTITY_KEY, quantity);
    }

    public static void SetShipEvoLevel(int level)
    {
        PlayerPrefs.SetInt(SHIP_EVO_LEVEL_KEY, level);
    }

    public static int GetShipEvoLevel()
    {
        return PlayerPrefs.GetInt(SHIP_EVO_LEVEL_KEY, 1);
    }

    public static void SetShipLoadOut(string JSONstring)
    {
        PlayerPrefs.SetString(SHIP_LOADOUT_STATE_KEY, JSONstring);
    }

    public static string GetShipLoadOut()
    {
        return PlayerPrefs.GetString(SHIP_LOADOUT_STATE_KEY, null);
    }

	public static void SetChestList(string jsonString)
	{
		PlayerPrefs.SetString (CHEST_LIST_KEY, jsonString);
	}

	public static string GetChestList()
	{
		return PlayerPrefs.GetString (CHEST_LIST_KEY,"{}");
	}

    public static void SetAchievements(string jsonString)
    {
        PlayerPrefs.SetString(ACHIEVEMENTS_KEY, jsonString);
    }

    public static string GetAchievements()
    {
        return PlayerPrefs.GetString(ACHIEVEMENTS_KEY, "{}");
    }

    public static void SetStatistics(string statsString)
    {
        PlayerPrefs.SetString(GAME_STATISTICS_KEY, statsString);
    }

    public static string GetStatistics()
    {
        return PlayerPrefs.GetString(GAME_STATISTICS_KEY);
    }

	public static void DeleteAll()
	{
		PlayerPrefs.DeleteAll ();
	}
}
