﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GunManager : MonoBehaviour {
	
	private List<GunSocket> _guns;
	public List<GameObject> _gunGameObjects;
	bool hasUpgradeGunstemporary=false;
	/// initialize in inspector (part of prefab)
	//public List<GameObject> _bullets;
	public GunDefinition _gunDefinitionScriptableObject;

	void OnEnable()
	{
		EventHub.AttachListener (DataConstants.CHANGE_GUNS_EVENT_STR, ChangeGuns);
	}

	void OnDisable()
	{
		EventHub.DetachListener (DataConstants.CHANGE_GUNS_EVENT_STR, ChangeGuns);
	}

	void Start()
	{
		Initialize (); 
		LoadGuns ();	// instantiate guns with their definitions
	}

	private void Initialize()
	{
		_gunGameObjects = new List<GameObject> ();
		_guns = new List<GunSocket> ();

	}

	private void LoadGuns()
	{
		//get the stored guns
		_guns = PlayerInventory.instance.ShipLoadout.GetGunSockets ();	

		//iterate through all guns found in Player Inventory
		for (int i = 0; i < _guns.Count; i++) {

			//if gun socket is unlocked
			if (_guns [i].GetState () == SocketState.Unlocked) { 
				//identify gun name
				string slotName = IdentifyGunName (i);
				GameObject go = new GameObject (slotName);
				//make child of gun manager
				go.transform.SetParent (gameObject.transform);
				//set gun position to player pivot
				go.transform.localPosition = Vector3.zero;
				//Attach gun trigger script to gameobject
                //get index of gun type from its enum declaration
                //int enumIndexOfGunType = (int) _guns[i].GetGunType(); 
                GunType slotGunType = _guns[i].GetGunType();
				if (hasUpgradeGunstemporary) 
				{
					UpateGun(i, _guns[i]);
				}
                int slotLevel = _guns[i].GetLevel();
                GunTrigger gunTrigger = go.AddComponent<GunTrigger>();
                gunTrigger.Initialize(_gunDefinitionScriptableObject.GetMetaDataForGunType(slotGunType).gunParams[slotLevel]);
                //gunTrigger.GunDefinition = _gunDefinitionScriptableObject.GetMetaDataForGunType(slotGunType).gunParams[slotLevel];
				_gunGameObjects.Add (go);

			}
		}
	}

	void UpateGun(int index, GunSocket gun)
	{
		if (index == 0 && gun.GetLevel () < 3) 
		{
			gun.SetLevel (gun.GetLevel () + 1);
		}
	}
	public void DiscardCurrentGuns()
	{
		foreach (GameObject gunGameObject in _gunGameObjects) {
			Destroy (gunGameObject);
		}
		_gunGameObjects = new List<GameObject> ();
	}

	public void LevelupGuns()
	{
		hasUpgradeGunstemporary = true;
		ChangeGuns ();

	}
	private void ChangeGuns()
	{
		
		//delete current guns
		DiscardCurrentGuns();

		//configure the new guns
		LoadGuns();
	}

	private string IdentifyGunName(int index)
	{
		string gunName="";

		if (index == 0)
			gunName = "Primary Gun";
		else if (index == 1)
			gunName = "Secondary Gun";
		else if (index > 1)
			gunName = "Assist Gun#" + (index - 1);

		return gunName;
	}
		
}
