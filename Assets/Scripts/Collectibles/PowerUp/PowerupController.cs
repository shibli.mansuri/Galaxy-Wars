﻿using UnityEngine;
using System.Collections;
public class PowerupController
{
	PowerupModel powerupData;
	GunManager gunManager;

	public void OnPowerupCollection()
	{
		powerupData.AddPowerup ();
		if (powerupData.CheckLevelup ()) 
		{
			powerupData.DiscardPowerUps ();
			if(gunManager==null)
			gunManager = GameObject.FindObjectOfType<GunManager> ();
			gunManager.LevelupGuns ();
		}
	}
	public  PowerupController()
	{
		powerupData = new PowerupModel();
	}

}

