﻿using UnityEngine;
using System.Collections;

public class PowerupModel
{
	private int powerupCount;
	private int levelupCount=2;

	public int DesiredPowerupCount {
		get {
			return levelupCount;
		}
		set {
			levelupCount = value;
		}
	}

	public int PowerupCount {
		get {
			return powerupCount;
		}
		set {
			powerupCount = value;
		}
	}

	public void DiscardPowerUps()
	{

		PowerupCount = 0;
	}

	public void AddPowerup()
	{

		PowerupCount++;
	}

	public bool CheckLevelup()
	{
		if (powerupCount == levelupCount)
			return true; 
		else
			return false;
	}

	public PowerupModel()
	{


	}
}

