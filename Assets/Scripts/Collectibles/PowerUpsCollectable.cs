﻿using UnityEngine;
using System.Collections;

public class PowerUpsCollectable : BaseCollectible
{



	public override void OnCollidedWithPlayer(GameObject player)
	{
		DestroySelf ();
		if (player.GetComponent<PlayerController> ()) 
		{
			player.GetComponent<PlayerController> ().powerUpController.OnPowerupCollection ();

		}
	}

}

