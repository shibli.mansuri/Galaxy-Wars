﻿using UnityEngine;
using System.Collections;

public class HealthCollectible : BaseCollectible
{
	public int healthRewardAmount=1;
	PlayerHealth playerHealth;


	public virtual  void OnCollidedWithPlayer(GameObject player)
	{
		playerHealth = GameObject.FindObjectOfType<PlayerHealth> ();
		playerHealth.UpgradeHealth (healthRewardAmount);
		DestroySelf();


	}

}

