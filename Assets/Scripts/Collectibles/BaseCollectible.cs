﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCollectible : UbhMonoBehaviour {


    public void MoveAndStop()
    {
        Vector2 randomForce = Random.insideUnitCircle * 300f;
        randomForce.y *= randomForce.y > 0 ? -1:1;//change force to downwards direction

        rigidbody2D.AddForce(randomForce, ForceMode2D.Force);
        rigidbody2D.AddTorque(randomForce.SqrMagnitude());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(DataConstants.STR_PLAYER_TAG))
        {
			EventHub.TriggerEvent ("NumberOfCollectibles");
            OnCollidedWithPlayer(collision.gameObject);
        }
    }

	public virtual  void OnCollidedWithPlayer(GameObject player)
    {
		DestroySelf();
		LevelManager.instance.OnCoinCollection();
    }
		
    protected void DestroySelf()
    {
        UbhObjectPool.Instance.ReleaseGameObject(gameObject);
    }

    public void OnInvisible()//called externally from inspector by onbecameInvisibleHandler
    {
        DestroySelf();
    }
}
