﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper {

    public static Vector3 GetPosRelativeCamera(float x, float y,bool zIsZero = true)
    {
        Vector3 toReturn = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * x, Screen.height * y, 0f));
        if (zIsZero)
            toReturn.z = 0;
        return toReturn;
    }

    public static Vector3 GetPosRelativeCamera(Vector2 pos)
    {
        return GetPosRelativeCamera(pos.x, pos.y);
    }

    public static void DrawString(string text, Vector3 worldPos, Color? colour = null)
    {
       /* UnityEditor.Handles.BeginGUI();
        if (colour.HasValue) GUI.color = colour.Value;
        var view = UnityEditor.SceneView.currentDrawingSceneView;
        Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
        Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
        GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
        UnityEditor.Handles.EndGUI();*/
    }

    public static float RoundTo(float value, int decimalPlaces)
    {
        int divisor = decimalPlaces > 0 ? (int)Mathf.Pow(10, decimalPlaces) : 1;
        return Mathf.Round(value*divisor)/divisor;
    }
}
