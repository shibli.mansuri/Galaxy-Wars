﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataConstants {

    public static string level1 = "level1";
    public static string testLevel = "Sample-Gameplay-Updated";

    public static string STR_PLAYER_BULLET_TAG = "PlayerFire";
    public static string STR_ENEMY_BULLET_TAG = "EnemyFire";
    public static string STR_PLAYER_TAG = "Player";
    public static string ENEMY_TAG = "Enemy";
    public static string STR_COLLECTABLE_POWER_UP_TAG = "PowerUpObject";

    public static string STR_EVENT_ENEMY_DESTROYED = "enemyDestroyed";
    public static string STR_EVENT_GAME_PAUSED = "gamePaused";
    public static string STR_EVENT_GAME_UNPAUSED = "gameUnpaused";

    public static string LEVEL_NAME_MAIN_MENU_STR = "MainMenu";
	public static string CHANGE_GUNS_EVENT_STR = "ChangeGuns";

	public static string RESOURCES_POWERUP_COLLECTIBLE_PATH = "Prefabs/Collectibles/PowerUpCollectible";
	public static string RESOURCES_HEALTH_COLLECTIBLE_PATH = "Prefabs/Collectibles/HealthCollectible";
    public static string RESOURCES_HEALTHBAR_PATH = "Prefabs/HealthBar";
    public static string GUNSLOT_WORKSHOPUI_PATH = "Prefabs/GunSlot";
    public static string GUNTYPE_WORKSHOPUI_PATH = "Prefabs/GunType_WorkshopUI";
	public static string GUNLEVEL_WORKSHOPUI_PATH = "Prefabs/Gunlevel-View";
	public static string LOADING_UI_PATH = "Prefabs/LoadingPannel";
	public static string CHEST_CELL_UI_PATH = "Chest";
	public static string CLAIM_PANEL_UI_PATH = "ClaimPanel";
    public static string ACHIEVEMENTS_CELL_UI_PATH = "AchievementCell";
	public static string SHOP_ITEM_CELL_UI_PATH = "ShopItemCell";


    public static string ACHIEVEMENTS_LOADER_PATH = "AchievementsLoader";

    public static string LEVEL_DESIGN_ASSET_PATH = "LevelDesign";

	public static string HUD_PATH = "Canvas/HUD";


}
