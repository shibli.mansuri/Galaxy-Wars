﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SizePreviewController : MonoBehaviour {

    public List<GameObject> enemies;
    public int currentEnemyIndex;
    public float scale;
    GameObject currentEnemy;
    public Text scaleFactor;

    private void Awake()
    {
        scale = 1f;
        currentEnemyIndex = 0;
        currentEnemy =  Instantiate(enemies[currentEnemyIndex]);
        currentEnemy.SetActive(true);

        scaleFactor.text = scale.ToString();
    }

    public void NextEnemy()
    {
        Destroy(currentEnemy.gameObject);
        if (currentEnemyIndex >= enemies.Count-1)
            currentEnemyIndex = 0;
        else
            currentEnemyIndex++;
        currentEnemy = Instantiate(enemies[currentEnemyIndex]);
        currentEnemy.SetActive(true);
    }

    public void PreviousEnemy()
    {
        Destroy(currentEnemy.gameObject);
        currentEnemy.SetActive(false);
        if (currentEnemyIndex <= 0)
            currentEnemyIndex = enemies.Count-1;
        else
            currentEnemyIndex--;
        currentEnemy = Instantiate(enemies[currentEnemyIndex]);
        currentEnemy.SetActive(true);
    }

    public void ScaleUp()
    {
        if (scale >= 1f)
            scale = 0.4f;
        else
            scale += 0.1f;

       currentEnemy.transform.localScale = new Vector3(scale, scale, scale);
       scaleFactor.text = scale.ToString();
    }

    public void ScaleDown()
    {
        if (scale <= 0.4)
            scale = 1f;
        else
            scale -= 0.1f;

        currentEnemy.transform.localScale = new Vector3(scale, scale, scale);

        scaleFactor.text = scale.ToString();
    }
}
