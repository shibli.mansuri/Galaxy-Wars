﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathAutoPosition : MonoBehaviour {

    public bool _ShowDebug = false;
    public Vector2 _Position;

    private void Awake()
    {
        transform.position = Helper.GetPosRelativeCamera(_Position);
        transform.parent = Camera.main.transform;
    }

    private void OnDrawGizmos()
    {
        if (_ShowDebug)
        {
            Vector3 currentPos = Camera.main.WorldToScreenPoint(transform.position);
            float x = Mathf.Round(currentPos.x / Camera.main.pixelWidth*100)/100f;// Screen.width;
            float y = Mathf.Round(currentPos.y/ Camera.main.pixelHeight*100)/100f;// / Screen.height;
           // Gizmos.DrawIcon(transform.position, (x + "," + y),true);
            //Debug.Log(x + "," + y+" | " +Camera.main.pixelWidth+ "," + Camera.main.pixelHeight);
            Helper.DrawString((x + "," + y), transform.position,Color.blue);
        }
    }


}
