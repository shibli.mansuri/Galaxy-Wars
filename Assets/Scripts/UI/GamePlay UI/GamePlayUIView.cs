﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayUIView : MonoBehaviour {

    public GameObject workshopView;
    public GameObject achievementAlert;
    public GameObject achievementView;
    WorkshopUIManager workshopManager;

    public void Initialize()
    {
        workshopManager = workshopView.GetComponent<WorkshopUIManager>();
        HideWorkShop();
    }

    public void ShowAchievementView()
    {
        UIManager.instance.Pause(true);
        achievementView.SetActive(true);
        AchievementUIManager achievementUIManager = achievementView.GetComponent<AchievementUIManager>();
        achievementUIManager.RepopulateAchievements();
    }

    public void HideAchievementView()
    {
        achievementView.SetActive(false);
    }

    public void ShowAchievementAlert()
    {
        achievementAlert.SetActive(true);
    }

    public void HideAchievementAlert()
    {
        achievementAlert.SetActive(false);
    }

    public void ShowWorkShop()
    {
        
        workshopView.SetActive(true);
    }

    public void HideWorkShop()
    {
        workshopView.SetActive(false);
    }

    public WorkshopUIManager WorkshopManager
    {
        get
        {
            if (workshopManager == null)
                workshopManager = workshopView.GetComponent<WorkshopUIManager>();
            return workshopManager;
        }
    }
}
