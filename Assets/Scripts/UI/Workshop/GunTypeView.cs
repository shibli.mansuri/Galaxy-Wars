﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GunTypeView : MonoBehaviour {

    public Text title;
    public Image BG;
    public Text status;
    public UnityAction onTapped;

    public void SetTitle(string text)
    {
        title.text = text;
    }

    public void SetBGColor(Color color)
    {
        BG.color = color;
    }

    public void SetStatus(string text)
    {
        status.text = text;
    }

    public void OnButtonPressed()
    {
        if (onTapped != null)
        {
            onTapped();
        }
    }


}
