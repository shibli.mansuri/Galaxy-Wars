﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SlotView : MonoBehaviour {

    public Image BG;
    public Text label;
    int priceToUnlock;
    UnityAction OnPressed;

    public void ButtonPressed()
    {
        if (OnPressed != null)
            OnPressed();
    }

    public void SetImageColor(Color color)
    {
        BG.color = color;
    }

    public void SetLabelText(string text)
    {
        label.text = text;
    }

    public void ReplaceOnPressedFunction(UnityAction toCall)
    {
        OnPressed = toCall;
    }
}
