﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WorkshopUIManager : MonoBehaviour {

    UnityAction _closeMenuFunction;
    WorkshopUIView _view;
    
    ShipLoadOut _shipLoadOut;
    int _currentSelectedSlot=-1;

    private void Awake()
    { 
        if(null ==_closeMenuFunction)//if the function hasn't already been configured
            _closeMenuFunction = ()=> { UIManager.instance.LoadLevel(DataConstants.LEVEL_NAME_MAIN_MENU_STR); };
        _view = GetComponent<WorkshopUIView>();
        _shipLoadOut = PlayerInventory.instance.ShipLoadout;
    }

    private void Start()
    {
        PopulateView();
    }

    public void BackButtonPressed()
    {
        _closeMenuFunction();
    }

    public void SetCloseFunction(UnityAction toCall)
    {
        _closeMenuFunction = toCall;
    }

    void PopulateView()
    {
        PopulateCurrencies();
        _view.TryEnablingEvolution();
        InitializeGunSlots();
    }

    void PopulateCurrencies()
    {
        _view.SetCoinsCount(PlayerInventory.instance.GetCoins());
        _view.SetODFragCount(PlayerInventory.instance.ODFrags);
        _view.SetPowerGemsCount(PlayerInventory.instance.PowerGems);
    }

    void InitializeGunSlots()
    {
       
        for (int i =0; i < _shipLoadOut.GunCount; i++)
        {
            SocketState socketState = _shipLoadOut.GetSocketState(i);
            if (socketState == SocketState.Disabled)
            {
                continue;
            }
            else
            {
                _view.CreateSocket(socketState, i);
            }
        }
    }

    public void OnGunSlotPressed(int index)
    {
        //reset the view
        _view.DeSelectAllGunSlots();
        _view.DeselectCurrentGunType();

        if(_shipLoadOut.GetSocketState(index)== SocketState.Locked)//either unlock this slot
        {
            _view.HideGunTypes();//if some slot was alraedy selected and we were showing its gun types hide them
            Debug.Log("Unlock GunSlot " + index);
            if (Shopkeeper.CanBuy(Shopkeeper.ItemType.GunSlot))
            {
                Shopkeeper.BuyItem(Shopkeeper.ItemType.GunSlot);
                _shipLoadOut.UnlockSocket(index);
                PopulateCurrencies();
                Debug.Log("Switch to GunSlot " + index + " from " + _currentSelectedSlot);
                _currentSelectedSlot = index;
                _view.ShowSlotSelectedAt(index);//show that this gunslot is pressed 
                _view.ShowGunTypeScrollView(_shipLoadOut.HighestGunUnlocked);//show guntypes if hidden
                _view.ShowTypeIsSelected(_shipLoadOut.GetGunTypeAt(index));//change selection to this slot's gun type
            }
        }
        else//or display the gun types available to this slot
        {
            Debug.Log("Switch to GunSlot " + index + " from " + _currentSelectedSlot);
            _currentSelectedSlot = index;
            _view.ShowSlotSelectedAt(index);//show that this gunslot is pressed 
            _view.ShowGunTypeScrollView(_shipLoadOut.HighestGunUnlocked);//show guntypes if hidden
            _view.ShowTypeIsSelected(_shipLoadOut.GetGunTypeAt(index));//change selection to this slot's gun type
        }
    }

    public void OnGunTypePressed(GunType newType)
    {
        GunType currentType = _shipLoadOut.GetGunTypeAt(_currentSelectedSlot);
        if (newType != currentType && _shipLoadOut.IsGunUnlocked(newType))
        {
            Debug.Log("Set current gun slot : " + _currentSelectedSlot + " to gun type " + newType.ToString());
            _shipLoadOut.SetGunTypeAt(_currentSelectedSlot, newType);
			_shipLoadOut.SaveGunConfig ();
            _view.SwitchTypeFromTo(currentType, newType);
			EventHub.TriggerEvent (DataConstants.CHANGE_GUNS_EVENT_STR);
		
        }    
    }

    
    
    

    
}
