﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkshopUIView : MonoBehaviour {
    public Text numPowerGemsLbl;
    public Text numCoinsLbl;
    public Text damageLbl;
    public Text numODFragLbl;
    public Button Evolve;

    public GameObject gunSlotsScrollViewContent;
    public GameObject gunTypesScrollViewContent;

    Dictionary<int, SlotView> _gunSlotViewsDict;//stores all  the gunslot's view class referenced by the index of the slot
    WorkshopUIManager _controller;
    Dictionary<GunType, GunTypeView> _gunTypeViewsDict;
	Dictionary<int, SlotView> _gunLevelViewsDict;

    private int _currentSelectedSlot = -1;
    private GunType _currentSelectedType;

    private void Awake()
    {
        _gunSlotViewsDict = new Dictionary<int, SlotView>();
        _controller = GetComponent<WorkshopUIManager>();
    }

    public void SetODFragCount(int num)
    {
        numODFragLbl.text = "ODFrags: "+num.ToString();
    }

    public void SetCoinsCount(int num)
    {
        numCoinsLbl.text = "Coins: "+num.ToString();
    }

    public void SetDamageLbl(int num)
    {
        damageLbl.text = "Damage: "+num.ToString();
    }

    public void SetPowerGemsCount(int count)
    {
        numPowerGemsLbl.text = "PowerGems: "+count.ToString();
    }

    public void CreateSocket(SocketState socketState, int index)
    {
        GameObject gunslotObject = Resources.Load(DataConstants.GUNSLOT_WORKSHOPUI_PATH) as GameObject;
        GameObject obj = Instantiate(gunslotObject,gunSlotsScrollViewContent.transform) as GameObject;
        SlotView slotView = obj.GetComponent<SlotView>();
        
        _gunSlotViewsDict.Add(index, slotView);
        if (socketState == SocketState.Locked)
        {
            slotView.SetImageColor(Color.red);
            slotView.SetLabelText(index + " LOCKED 500 g");
            slotView.ReplaceOnPressedFunction(() => { GunSlotPressed(index); });
        }
        else
        {
            slotView.SetImageColor(Color.green);
            slotView.SetLabelText(index + " Unlocked");
            slotView.ReplaceOnPressedFunction(() => { GunSlotPressed(index); });
        }
    }

    private void GunSlotPressed(int index)
    {
        _controller.OnGunSlotPressed(index);
    }

    public void ShowSlotSelectedAt(int selectionIndex)//called from controller
    {
        ChangeGunSlotColor(selectionIndex, Color.yellow);
        _currentSelectedSlot = selectionIndex;
    }

    public void DeSelectAllGunSlots()
    {
        if (_currentSelectedSlot > -1)
            ChangeGunSlotColor(_currentSelectedSlot, Color.green);
    }

    void ChangeGunSlotColor(int index, Color newColor)
    {
        SlotView slotView;
        _gunSlotViewsDict.TryGetValue(index, out slotView);
        if (slotView == null)
        {
            Debug.LogError("WORKSHOP UI: No slotview for index : " + index);
        }
        else
        {
            slotView.SetImageColor(newColor);
        }
    }

    public void ShowGunTypeScrollView(GunType highestUnlocked)
    {
        if (_gunTypeViewsDict!=null)//if the gun objects have been populated just enable disable them   
        {
            ShowGunTypes();
        }
        else// initialize the gun objects
        {
            PopulateGunTypeScrollView(highestUnlocked);
        }
    }

    void PopulateGunTypeScrollView(GunType highestUnlocked)
    {
        GameObject obj = Resources.Load(DataConstants.GUNTYPE_WORKSHOPUI_PATH)as GameObject;
        _gunTypeViewsDict = new Dictionary<GunType, GunTypeView>();
        var gunNames = Enum.GetNames(typeof(GunType));

        for (int i = 0; i < gunNames.Length; i++)
        {
            GunTypeView view = Instantiate(obj, gunTypesScrollViewContent.transform).GetComponent<GunTypeView>();
            int index = i;
            _gunTypeViewsDict.Add((GunType)index,view);
            view.SetTitle(gunNames[index]);
            view.onTapped = () => { OnGunTypePressed((GunType)index); };
        }
        LockGunTypes(highestUnlocked);
    }

    public void OnGunTypePressed(GunType type)
    {
        _controller.OnGunTypePressed(type);
    }

    public void SwitchTypeFromTo(GunType from,GunType to)
    {
        DeselectGunType(from);
        ShowTypeIsSelected(to);
    }

    public void ShowTypeIsSelected(GunType selection)
    {
        GunTypeView view;
        _gunTypeViewsDict.TryGetValue(selection, out view);

        if (view != null) { 
            view.SetBGColor(Color.green);
            _currentSelectedType = selection;
        }
        else
        {
            Debug.LogError("View not found in type dictionary " + selection.ToString());
        }
    }

    public void DeselectCurrentGunType()
    {
        if (_gunTypeViewsDict != null)
        {
            DeselectGunType(_currentSelectedType);
        }
        
    }

    void DeselectGunType(GunType selection)
    {
        //Deselects current selection
        GunTypeView view;
        _gunTypeViewsDict.TryGetValue(selection, out view);
        if (view != null)
        {
            view.SetBGColor(Color.white);
        }
        else
        {
            Debug.LogError("View not found in Type dictionary" + selection.ToString());
        }
    }

    void LockGunTypes(GunType highestUnlockedType)
    {
        foreach(KeyValuePair<GunType,GunTypeView> item in _gunTypeViewsDict)
        {
            if ((int)item.Key > (int)highestUnlockedType)//this is unlocked
                item.Value.SetStatus("Locked Rs. 299");
            else
                item.Value.SetStatus("Choose me");
        }
    }

    public void HideGunTypes()
    {
        if (_gunTypeViewsDict != null)
        {
            foreach (KeyValuePair<GunType, GunTypeView> item in _gunTypeViewsDict)
            {
                item.Value.gameObject.SetActive(false);
            }
        }
        
    }

    public void ShowGunTypes()
    {
        if(_gunTypeViewsDict != null)
        {
            foreach (KeyValuePair<GunType, GunTypeView> item in _gunTypeViewsDict)
            {
                item.Value.gameObject.SetActive(true);
            }
        }
    }

    bool CanEvolve()
    {
        int numFrags = PlayerInventory.instance.ODFrags;
        int numOfEvoStones = PlayerInventory.instance.EvoStone;
        int coins = PlayerInventory.instance.GetCoins();
        bool hasEnoughFrags = numFrags == 4;
        bool hasEnoughEvoStones = numOfEvoStones > 0;
        bool canPay = coins > 1000;
        return (hasEnoughFrags && hasEnoughEvoStones && canPay);
    }

    public void TryEnablingEvolution()
    {
        if (CanEvolve())
        {
            Evolve.interactable = true;
        }
        else
        {
            Evolve.interactable = false;
        }
    }
		
}
