﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class ModalPanel : MonoBehaviour {

	public Text message;
	public Image iconImage;
	public Button yesButton;
	public Button noButton;
	public GameObject modalPanelObject;

	public static ModalPanel instance;

	void Awake()
	{
		if (ModalPanel.instance == null)
			ModalPanel.instance = this;
		else if (ModalPanel.instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad (gameObject);
	}

    public void Choice (string message, UnityAction yesEvent, UnityAction noEvent) {
		modalPanelObject.SetActive (true);
		modalPanelObject.transform.SetAsLastSibling ();

		yesButton.onClick.RemoveAllListeners();
		yesButton.onClick.AddListener (yesEvent);
		yesButton.onClick.AddListener (ClosePanel);

		noButton.onClick.RemoveAllListeners();
		noButton.onClick.AddListener (noEvent);
		noButton.onClick.AddListener (ClosePanel);

		this.message.text = message;

		this.iconImage.gameObject.SetActive (false);
		yesButton.gameObject.SetActive (true);
		noButton.gameObject.SetActive (true);
	}

    public void Choice(string message, UnityAction yesEvent)
    {
        modalPanelObject.SetActive(true);
        modalPanelObject.transform.SetAsLastSibling();

        yesButton.onClick.RemoveAllListeners();
        yesButton.onClick.AddListener(yesEvent);
        yesButton.onClick.AddListener(ClosePanel);

        noButton.onClick.RemoveAllListeners();
        noButton.onClick.AddListener(ClosePanel);

        this.message.text = message;

        this.iconImage.gameObject.SetActive(false);
        yesButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);
    }

    public void ClosePanel () {
		modalPanelObject.SetActive (false);
	}
}