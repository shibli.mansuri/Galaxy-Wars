﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopUIManager : MonoBehaviour {

	public Text coinsText;

	public Text megabombDescription;
	public Button megabombButton;

	public Text shieldDescription;
	public Button shieldButton;

	public GameObject _productCell;

	void Start()
	{
		Initialize ();
	}
		
	private void Initialize()
	{
		UpdateCoinsText ();
		UpdateMegabombCell ();
		UpdateShieldCell ();
		_productCell = Resources.Load (DataConstants.SHOP_ITEM_CELL_UI_PATH) as GameObject;
	}

	public void CreateProductCell(ABCProductDefinition product)
	{
		GameObject productCell = Instantiate (_productCell, GameObject.Find ("Content").transform);
		ShopCellUI productCellInfo = productCell.GetComponent<ShopCellUI> ();
		productCell.SetActive (true);
		productCell.name = product.Name;
		productCellInfo.ItemName.text = product.Name; 
		productCellInfo.PayoutText.text = product.PayOut.ToString ();
		productCellInfo.ButtonText.text = "$" + product.Price;

		productCellInfo.Button.onClick.RemoveAllListeners ();
		productCellInfo.Button.onClick.AddListener (delegate {

			//Button OnClick code goes here
			Debug.Log("Hi, My price is: " + product.Price);	
		});
	}

	public void UpdateCoinsText()
	{
		coinsText.text = "Coins: " + PlayerInventory.instance.GetCoins ();
	}
		
	public void UpdateMegabombCell()
	{
		megabombDescription.text = "Damage all enemies together! (" + PlayerInventory.instance.GetMegabomb().GetQuantity() + "/3)";  
		if (PlayerInventory.instance.GetMegabomb().GetQuantity() == 3 || PlayerInventory.instance.GetCoins () < 100 || PlayerInventory.instance.GetMegabomb().GetLevel() == 0)
			megabombButton.interactable = false;
		else
			megabombButton.interactable = true;
	}

	public void UpdateShieldCell()
	{
		shieldDescription.text = "Protect yourself from the enemy! (" + PlayerInventory.instance.GetShield().GetQuantity()+"/3)";
		if (PlayerInventory.instance.GetShield().GetQuantity() == 3 || PlayerInventory.instance.GetCoins () < 100 || PlayerInventory.instance.GetShield().GetLevel() == 0)
			shieldButton.interactable = false;
		else
			shieldButton.interactable = true;
	}
}
