﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopCellUI : MonoBehaviour {

	public Text _itemName;
	public Text _payoutText;
	public Button _button;
	public Text _buttonText;


	public Text ItemName {
		get {
			return _itemName;
		}
		set {
			_itemName = value;
		}
	}

	public Text PayoutText {
		get {
			return _payoutText;
		}
		set {
			_payoutText = value;
		}
	}

	public Button Button {
		get {
			return _button;
		}
		set {
			_button = value;
		}
	}

	public Text ButtonText {
		get {
			return _buttonText;
		}
		set {
			_buttonText = value;
		}
	}
}
