﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shop : MonoBehaviour {
    ABCProductDefinition[] products;
	public ShopUIManager _view;

    private void Start()
    {
        products = IAPManager.GetProductDefinitions();
        PopulateScrollView();

    }

    void PopulateScrollView()
    {
        //populate scrollview here with the product definitions
        for(int i = 0; i < products.Length; i++)
        {
			ABCProductDefinition currProduct = products [i];
			_view.CreateProductCell (currProduct);
			//Debug.Log(string.Format("Product : {0} for ${1} gives {2}", currProduct.Name, currProduct.Price, currProduct.PayOut));
        }
    }

    public void BuyMegabomb()
	{
		if (PlayerInventory.instance.GetCoins () >= 100) 
		{
			PlayerInventory.instance.SubtractCoins (100);
			PlayerInventory.instance.AddMegabomb ();
			_view.UpdateCoinsText ();
			_view.UpdateMegabombCell ();
		}
	}

	public void BuyShield()
	{
		if (PlayerInventory.instance.GetCoins () >= 100) 
		{
			PlayerInventory.instance.SubtractCoins (100);
			PlayerInventory.instance.AddShield ();
			_view.UpdateCoinsText ();
			_view.UpdateShieldCell ();
		}
	}
}
