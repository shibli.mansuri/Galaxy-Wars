﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

public class EventHub : MonoBehaviour
{
    private static EventHub instance;

    private Dictionary<string, UnityEvent> eventDictionary;
    private Dictionary<string, IntEvent> intEventDictionary;

    void Init()
    {
        if (eventDictionary == null)
        {
            DontDestroyOnLoad(this);
            eventDictionary = new Dictionary<string, UnityEvent>();
        }
        else
        {
            Destroy(this);//this is probably a duplicate of this singleton
        }
    }

    public static void AttachListener(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void DetachListener(string eventName, UnityAction listener)
    {
        if (instance == null) return;
        UnityEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }

    public static EventHub Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType(typeof(EventHub)) as EventHub;

                if (!instance)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    instance.Init();
                }
            }

            return instance;
        }
    }
}

[Serializable]
public class IntEvent : UnityEvent<int> { }
