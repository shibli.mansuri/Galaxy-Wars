using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance = null;
	private GameplayUIManager gamePlayUIManager;
	public PlayerHealth playerHealth;
	bool isGamePaused = false;
	public int coinsCollected = 0;
	public EventParser eventParser;
	public int levelNumber;
	int EnemiesKilled;
	int EnemiesSpawned;
	float PlayerDamaged;
	string enemyPercentage;
	string playerDamagePercentage;
	string coins;

	void Awake(){
		if (instance == null)
		{
			instance = GameObject.FindObjectOfType<LevelManager>();
			if (instance == null)
				Debug.Log("No instance of Level Manager found in scene");
		}
		else if (instance != null)
			Destroy(this.gameObject);
		playerHealth = GameObject.FindObjectOfType<PlayerHealth> ();
	}


	void Start () {

		EnemiesKilled = 0;
		EnemiesSpawned = 0;

		SetLevelPlayedStatistic();

		if (GameObject.Find ("Canvas"))
			gamePlayUIManager = FindObjectOfType<GameplayUIManager> ();
	}

	public void EnemySpawned()
	{
		EnemiesSpawned++;
		eventParser.AddStat (GameStatistics.Stat.EnemySpawns, 1);
	}
	public void EnemyKilled()
	{
		EnemiesKilled++;
		eventParser.AddStat (GameStatistics.Stat.EnemyKills, 1);
	}

	public void ShowEnemyStats()
	{
		enemyPercentage="Enemies killed: "+((EnemiesKilled*100)/EnemiesSpawned)+"%";
		PlayerDamaged = (((playerHealth._MaxHealth - playerHealth._CurrentHealth) * 100) / playerHealth._MaxHealth);
		playerDamagePercentage="Damage:"+PlayerDamaged+" %";
		gamePlayUIManager.SetPercentageStats(enemyPercentage,playerDamagePercentage);
		EventHub.DetachListener ("ShowEnemyStats", ShowEnemyStats);
	}

	private void OnEnable()
	{
		EventHub.AttachListener(DataConstants.STR_EVENT_GAME_PAUSED, SetGamePaused);
		EventHub.AttachListener(DataConstants.STR_EVENT_GAME_UNPAUSED, SetGameUnpaused);
		EventHub.AttachListener ("EnemySpawned", EnemySpawned);
		EventHub.AttachListener ("EnemyKilled", EnemyKilled);
		EventHub.AttachListener ("ShowEnemyStats", ShowEnemyStats);
	}

	private void SetGameUnpaused()
	{
		isGamePaused = false;
	}

	private void SetGamePaused()
	{
		isGamePaused = true;
	}

	private void OnDisable()
	{
		EventHub.DetachListener(DataConstants.STR_EVENT_GAME_PAUSED, SetGamePaused);
		EventHub.DetachListener(DataConstants.STR_EVENT_GAME_UNPAUSED, SetGameUnpaused);
		EventHub.DetachListener ("EnemySpawned", EnemySpawned);
		EventHub.DetachListener ("EnemyKilled", EnemyKilled);
	}

	public void OnCoinCollection(){
		coinsCollected++;
		coins = "Coins: "+coinsCollected;
		gamePlayUIManager.SetCoinsStats (coins);

		eventParser.AddStat(GameStatistics.Stat.TotalCoins, 1);
		AudioManager.instance.PlayCoinCollectibleSound ();

	}

	public bool IsGamePaused { get { return isGamePaused; } }

	private void SetLevelPlayedStatistic()
	{
		levelNumber = GameStatistics.instance.levelButton;

		if(GameStatistics.instance.GetStat(GameStatistics.Stat.LevelsPlayed) < levelNumber)
		{
			EventParser.instance.SetStat(GameStatistics.Stat.LevelsPlayed, levelNumber);
		}

	}

}