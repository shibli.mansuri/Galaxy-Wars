using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager instance = null;
	public enum GameplayState { None, Playing, Paused, SemiPaused };
	public GameplayState gameplayState; // Keeps track of the current gameplay state
	private GameObject semiPauseMenuUI;
	private GameObject pauseMenuUI;
	//loading section
	public GameObject loadingUI; 
	AsyncOperation sceneAO;
	public Slider loadingProgbar;
	public GameObject ExitPanel;
	public Transform Canvas;
	public GameObject ExitPanelCopy;
	bool doesExitPanelExist;
	void Awake(){
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (this.gameObject);

		// these if structures are for testing purposes. The same if structures are present in OnLevelWasLoaded() which are to be used in production
		if (GameObject.Find ("Canvas/SemiPauseMenu")) {		
			instance.semiPauseMenuUI = GameObject.Find ("Canvas/SemiPauseMenu");
			instance.semiPauseMenuUI.SetActive (false);
		}

		if (GameObject.Find ("Canvas/PauseMenu")) {	
			instance.pauseMenuUI = GameObject.Find ("Canvas/PauseMenu");
			instance.pauseMenuUI.SetActive (false);
		}


		//GetLoadingPannel ();
		gameplayState = GameplayState.None;
		DontDestroyOnLoad (gameObject);	
		doesExitPanelExist = false;


	}

    public void LoadLevelOne()
    {
        SceneManager.LoadScene("ModelLevel");
    }
	
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			ActivateExitPanel ();
		}
	}

	void ActivateExitPanel()
	{
		if (ExitPanelCopy == null) {
			ExitPanelCopy = Instantiate (ExitPanel, Canvas);
		}
		else if (ExitPanelCopy.activeSelf==false) 
		{
			//ExitPanelCopy = Instantiate (ExitPanel, Canvas);
			ExitPanelCopy.SetActive(true);
		}
	}

	public void ExitGame()
	{
		Application.Quit (); 
	}

		
	public void ChangeExitPanelState()
	{
		doesExitPanelExist = false;
	}

	void OnLevelWasLoaded(){

		if (GameObject.Find ("Canvas/SemiPauseMenu")) {	 	// Gets reference of semi pause menu and deactivates it.
			instance.semiPauseMenuUI = GameObject.Find ("Canvas/SemiPauseMenu");
			instance.semiPauseMenuUI.SetActive (false);
		}
		if (GameObject.Find ("Canvas/PauseMenu")) {	 	// Gets reference of pause menu and deactivates it.
			instance.pauseMenuUI = GameObject.Find ("Canvas/PauseMenu");
			instance.pauseMenuUI.SetActive (false);
		}
		gameplayState = GameplayState.None;

		if (Time.timeScale != 1f)
			Time.timeScale = 1f;
        Canvas = GameObject.FindGameObjectWithTag("canvas").GetComponent<Transform>();
    }


	public void GetLoadingPannel()
	{
		if (!GameObject.Find ("Canvas/LoadingPannel(Clone)")) 
		{	 

			instance.loadingUI = Resources.Load (DataConstants.LOADING_UI_PATH) as GameObject;
			instance.loadingUI = Instantiate (instance.loadingUI , GameObject.Find ("Canvas").transform) as GameObject;
			instance.loadingProgbar = instance.loadingUI.transform.GetComponentInChildren<Slider> ();

		}
		instance.loadingUI.GetComponent<CanvasGroup> ().alpha = 0;
		instance.loadingUI.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		instance.loadingUI.GetComponent<CanvasGroup> ().interactable= false;
	}


	public void LoadLevel(string SceneName)
	{
		gameplayState = GameplayState.None;
		GetLoadingPannel ();
		instance.loadingUI.GetComponent<CanvasGroup> ().alpha = 1;
		instance.loadingUI.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		instance.loadingUI.GetComponent<CanvasGroup> ().interactable= true;
		instance.StartCoroutine(LoadingSceneProgress(SceneName));
	}

	IEnumerator LoadingSceneProgress(string sceneName)
	{
		print ("scene "+sceneName +" being loaded");
		SceneManager.LoadScene (sceneName);
		if(sceneName.Equals("ModelLevel"))
		for (int i = 0; i < 10; i++) 
		{
			instance.loadingProgbar.value += .3f;
			yield return new WaitForSeconds (.2f);

		}
		
		GetLoadingPannel ();

	}
	public void LoadGameLevelNo(int levelNumber)
	{
		if (levelNumber == GameStatistics.instance.levelButton) 
		{
			GameStatistics.instance.noOfAttempts++;

		}
		GameStatistics.instance.levelButton = levelNumber;
		if (Time.timeScale != 1f)
			Time.timeScale = 1f;
		LoadLevel("ModelLevel");
	}

	public void LoadGameLevel()
	{
		if (Time.timeScale != 1f)
			Time.timeScale = 1f;
		LoadLevel("ModelLevel");


	}

	public void SemiPause(bool status){ // TBD whether gameplayState value to be passed as argument or hardcoded 
		if (status == true) {
			AudioManager.instance.PlaySemiPauseBGSound ();
			Time.timeScale = 0.25f;
			gameplayState = GameplayState.SemiPaused;
		} 
		else if (status == false) {
			Time.timeScale = 1f;
			gameplayState = GameplayState.Playing;
			AudioManager.instance.PlayGamePlayBGSound();
		}

		//Time.fixedDeltaTime = 0.2f * Time.timeScale;
		instance.semiPauseMenuUI.SetActive (status);
	}

	public void Pause(bool status)
	{

		if (status == true) {
			instance.SemiPause (!status);
			Time.timeScale = 0f;
			gameplayState = GameplayState.Paused;
			AudioManager.instance.PlayUIBGSound();
		} 
		else if (status == false) {
			instance.SemiPause (!status);
		}

		instance.pauseMenuUI.SetActive (status);
	}

	public bool isGamePaused(){
		return gameplayState == GameplayState.Paused ? true : false;
	}

	public bool isGameSemiPaused(){
		return gameplayState == GameplayState.SemiPaused ? true : false;
	}


}
