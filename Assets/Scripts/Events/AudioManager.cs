﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {


	public AudioSource sfxSource;
	public AudioSource musicSource;
	//public AudioSource userInterfaceSource;
	public static AudioManager instance = null;
	public float highPitchRange = 0.95f;
	public float lowPitchRange = 1.05f;
	public bool sfxStatus;
	public bool musicStatus;
	public AudioClip uIMusic;
	public AudioClip gamePlayMusic;
	public AudioClip semiPauseMusic;
	public AudioClip gameOverMusic;
	public AudioClip levelCompleteMusic;
	public AudioClip gameCompleteMusic;
	public AudioClip coinCollectibleSound;
	public AudioClip playerHitSound;
	public AudioClip btnClick;

	void Awake(){
		
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad (gameObject);
		PlayUIBGSound ();
	}
		
	void Update () {

		if (musicStatus == false && musicSource.isPlaying) {
			instance.musicSource.Stop ();
		}

		if (musicStatus == true && !musicSource.isPlaying) {
			instance.musicSource.Play ();
		}
		
	}

	public void PlaySingle(AudioClip clip, float volumeScale=.5f){
		if (sfxStatus == true) {
			//instance.sfxSource.clip = clip;
			instance.sfxSource.PlayOneShot (clip, volumeScale);

			//print ("playing random FX");
		}
		
	}

	public void PlayUISound(AudioClip clip)
	{
		
		//instance.musicSource.clip =clip;
		if (clip) 
		{
			instance.musicSource.PlayOneShot (clip);
			PlayGamePlayBGSound ();
		}
	}
		
	public void PlayGamePlayBGSound()
	{
		
		if(!gamePlayMusic.Equals(null))
		musicSource.clip = gamePlayMusic;
	}

	public void PlaySemiPauseBGSound()
	{
		if(!semiPauseMusic.Equals(null))
			musicSource.clip = semiPauseMusic;
	}

	public void PlayGameOverMusic()
	{
		if(!gameOverMusic.Equals(null))
			musicSource.clip = gameOverMusic;
	}

	public void PlayLevelCompleteMusic()
	{
		if(!levelCompleteMusic.Equals(null))
			musicSource.clip = levelCompleteMusic;
	}

	public void PlayGameCompleteMusic()
	{
		if(!gameCompleteMusic.Equals(null))
			musicSource.clip = gameCompleteMusic;
	}

	public void PlayUIBGSound()
	{

		if(!uIMusic.Equals(null))
		musicSource.clip = uIMusic;
	}

	public void PlayPlayerHitSound()
	{
		
		if (!playerHitSound.Equals (null)) {

			musicSource.PlayOneShot (playerHitSound);
		
		}

	}
	public void PlayCoinCollectibleSound()
	{
		if (!coinCollectibleSound.Equals (null))
			musicSource.PlayOneShot (coinCollectibleSound);
	}

	public void PlayBtnClickSound()
	{
		if (btnClick==null) 
		{
			//musicSource.clip = btnClick;
			instance.musicSource.PlayOneShot (btnClick);

		}

	}
	public void PlayRandomSfx(params AudioClip[] clips)
	{
		if(sfxStatus == true && clips!=null && clips.Length>0){
			int randomIndex = Random.Range (0, clips.Length);
			float randomPitch = Random.Range (lowPitchRange, highPitchRange);
			instance.sfxSource.pitch = randomPitch;
			//instance.sfxSource.clip = clips [randomIndex];
			instance.sfxSource.PlayOneShot (clips[randomIndex], 0.5f);
		}
	}
}
