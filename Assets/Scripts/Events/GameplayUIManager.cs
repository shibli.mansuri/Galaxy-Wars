﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameplayUIManager : MonoBehaviour {
	[SerializeField]
	private Text coinsText;
	[SerializeField]
	private Text TotalCoins;
	[SerializeField]
	private Text EnemyPercentage;
	[SerializeField]
	private Text PlayerDamage;
	[SerializeField]
	private Text gameOverText;


	public GameObject gameOverUI;
	public Button activateMegabombButton;
	public Button activateShieldButton;

	GamePlayUIView view;

	public bool IsGameOver; 
	public bool CollectiblesCollected=false;
	static public bool EnableSemiPause=true;
	public int noOfCollectiblesSpawned;


	private void Awake()
	{
		view = GetComponent<GamePlayUIView>();
		IsGameOver = false;
		CollectiblesCollected = false;
		EnableSemiPause=true;
	}

	private void Start()
	{
		view.Initialize();
		view.WorkshopManager.SetCloseFunction(() => { view.HideWorkShop(); });
		AudioManager.instance.PlayGamePlayBGSound ();

	}

	void Update()
	{
		if (PlayerInventory.instance.GetMegabomb().GetQuantity() < 1 || PlayerInventory.instance.GetMegabomb().GetLevel() == 0) 
		{
			activateMegabombButton.GetComponent<EventTrigger>().enabled = false;
			activateMegabombButton.interactable = false;
		}

		if (PlayerInventory.instance.GetShield ().isActive () == true || PlayerInventory.instance.GetShield ().GetQuantity () < 1 || PlayerInventory.instance.GetShield ().GetLevel () == 0) {

			activateShieldButton.GetComponent<EventTrigger> ().enabled = false;
			activateShieldButton.interactable = false;

		}  else {
			activateShieldButton.GetComponent<EventTrigger> ().enabled = true;
			activateShieldButton.interactable = true;
		}
	}

	public void SemiPause(bool status){
		UIManager.instance.SemiPause (status);
	}

	public void Pause(bool status){
		if (status == true)
		{//pause the game
			EventHub.TriggerEvent(DataConstants.STR_EVENT_GAME_PAUSED);
		}
		else
		{//unpause the game
			EventHub.TriggerEvent(DataConstants.STR_EVENT_GAME_UNPAUSED);
		}
		UIManager.instance.Pause(status);
	}

	public void LoadLevel(string name)
	{
		UIManager.instance.LoadLevel (name);
	}
	public void DecreaseNumberOfCollectibles()//called when a player collects a collectible
	{
		noOfCollectiblesSpawned--;
	}

	public void BossDeathGameover(){
		EnableSemiPause = false;
		gameOverText.text="Congratulations";
		noOfCollectiblesSpawned = GameObject.FindGameObjectsWithTag ("Collectible").Length;
		EventHub.AttachListener ("NumberOfCollectibles", DecreaseNumberOfCollectibles);
		StartCoroutine(WaitUntilAllRewardsCollected());
		AddGameCompleteAnalyticsData ();
	}

	public void AddGameCompleteAnalyticsData()
	{
		
		GameAnalyticsManager.instance.OnLevelComplete (GameStatistics.instance.levelButton, Time.time, GameStatistics.instance.noOfAttempts,LevelManager.instance.playerHealth._CurrentHealth);
		//reset attempts
		GameStatistics.instance.noOfAttempts = 1;
	}

	public void PlayerDeathGameOver()
	{
		EnableSemiPause = false;
		gameOverText.text="GameOver";
		ShowGameOverScreen ();
	}

	IEnumerator WaitUntilAllRewardsCollected()
	{
		while (!CollectiblesCollected) {
			if (noOfCollectiblesSpawned == 0) 
			{
				CollectiblesCollected = true;
				StartCoroutine(ShowBossDefeatedAfterDelay());
			}
			yield return null;
		}
		EventHub.DetachListener ("NumberOfCollectibles", DecreaseNumberOfCollectibles);

	}
	IEnumerator ShowBossDefeatedAfterDelay()
	{
		yield return new WaitForSeconds(2.0f);

		Time.timeScale = 0;

		if (GameObject.Find (DataConstants.HUD_PATH))
			GameObject.Find (DataConstants.HUD_PATH).SetActive (false);
		EventHub.TriggerEvent ("ShowEnemyStats");

		gameOverUI.SetActive (true);
		EnableSemiPause = true;

	}

	void AddGameOverAnalyticsData()
	{

		GameAnalyticsManager.instance.OnGameOver (GameStatistics.instance.levelButton,GameStatistics.instance.currentWaveSpawned);

	}



	void ShowGameOverScreen()
	{

		AddGameOverAnalyticsData ();
		Time.timeScale = 0;

		if (GameObject.Find (DataConstants.HUD_PATH))
			GameObject.Find (DataConstants.HUD_PATH).SetActive (false);
		EventHub.TriggerEvent ("ShowEnemyStats");
		gameOverUI.SetActive (true);
		EnableSemiPause = true;
	}

	public void SetPercentageStats(string enemyPercentage,string playerDamagePercentage)
	{
		EnemyPercentage.text = enemyPercentage;
		PlayerDamage.text = playerDamagePercentage;
	}

	public void SetCoinsStats(string totalCoins)
	{
		coinsText.text=totalCoins;
		TotalCoins.text=totalCoins;
	}

	public void ActivateShield(){
		PlayerInventory.instance.GetShield ().Activate ();
	}

	public void ActivateMegabomb(){
		EventHub.TriggerEvent ("MegabombActivationFromGUI");
	}

	void OnEnable(){
		EventHub.AttachListener ("BossDeathGameover", BossDeathGameover);
		EventHub.AttachListener ("PlayerDeathGameOver", PlayerDeathGameOver);


	}

	void OnDisable(){
		EventHub.DetachListener ("BossDeathGameover", BossDeathGameover);
		EventHub.DetachListener ("PlayerDeathGameOver", PlayerDeathGameOver);

	}
}
