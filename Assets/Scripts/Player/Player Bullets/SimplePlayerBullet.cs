﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlayerBullet : BasePlayerBullet {

	protected override void OnInitialize(){}
	protected override void OnUpdate(){}
	protected override void OnLateUpdate(){}
	protected override void OnDestroySelf(){}
	protected override void OnPreShoot(){}
	protected override void OnPostShoot(){}

}
