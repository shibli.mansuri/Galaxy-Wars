﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBullet : BasePlayerBullet {

	private BoxCollider2D collider;
	private SpriteRenderer renderer;
	private Transform player;

	private float yScale;

	protected override void OnInitialize()
	{
		Range = Range * 3f;
		ShootingEnabled = false;
		GetReferences ();
	}

	protected override void OnUpdate()
	{
        if (player != null)
        {
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
            transform.localScale += new Vector3(0f, 2f, 0f);
            yScale += 2f;
            collider.size = renderer.sprite.bounds.size;
            collider.offset = new Vector2(collider.offset.x, collider.size.y / 2);
        }
		
	}

	private void GetReferences()
	{
		//get reference to player
        //TODO: Get instance from PlayerController
		player = GameObject.FindGameObjectWithTag (DataConstants.STR_PLAYER_TAG).transform;

		//get reference to collider
		collider = GetComponent<BoxCollider2D> ();
	
		//get reference to sprite renderer
		renderer = GetComponent<SpriteRenderer>();

		yScale = transform.localScale.y;
	}

	protected override void OnLateUpdate(){}
	protected override void OnDestroySelf(){}
	protected override void OnPreShoot(){}
	protected override void OnPostShoot(){}
		
}
