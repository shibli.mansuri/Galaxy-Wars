﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CanonBullet : BasePlayerBullet {

	private CircleCollider2D miniBombCollider;
	public GameObject explodeVFX;

    protected override void OnInitialize()
    {
        miniBombCollider = GetComponent<CircleCollider2D>();
        miniBombCollider.enabled = false;
    }

    protected override void OnUpdate()
    {
        // throw new NotImplementedException();
    }

    protected override void OnLateUpdate()
    {
        // throw new NotImplementedException();
    }

    protected override void OnDestroySelf()
    {
        PlayFX();
        ActivateBomb();
        Invoke("DeactivateBomb", 0.5f);
    }
    //override protected void DestroySelf()
    //{


    //	GetReleasedToPool ();
    //}

    private void PlayFX()
	{
		Debug.Log ("I am a canon baby!");
		if(explodeVFX!=null)
			Instantiate(explodeVFX, transform.position, Quaternion.identity, transform.parent);
	}

	private void ActivateBomb()
	{
		
		miniBombCollider.enabled = true;
		while(miniBombCollider.radius <= 0.25f)
			miniBombCollider.radius += 0.5f * Time.deltaTime;

	}

	private void DeactivateBomb()
	{
		miniBombCollider.radius = 0.02f;
		miniBombCollider.enabled = false;
	}

    protected override void OnPreShoot()
    {
		print ("On Post Shoot");
    }

    protected override void OnPostShoot()
    {
       
    }
}
