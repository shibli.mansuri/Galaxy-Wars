﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class BasePlayerBullet : UbhMonoBehaviour {

	private float _damage; 
	private float _maximumHealth;
    private float _currentHealth;
	private float _range;
    private float _speed;
    private float _shotDelay;
	private Vector3 _startPosition;
	private bool _IsAutoShootingEnabled = true;
	public AudioClip bulletSound;

    protected abstract void OnInitialize();
    protected abstract void OnUpdate();
    protected abstract void OnLateUpdate();
    protected abstract void OnDestroySelf();
    protected abstract void OnPreShoot();
    protected abstract void OnPostShoot();

    private void Awake()
    {
    //    Debug.Log("Bullet is awake " + gameObject.name);
    }

    public void Initialize(GunDefinitionParams gunDef)
    {
        Damage = gunDef.damagePerBullet;
        MaximumHealth = gunDef.maxBulletHealth;
        Range = gunDef.range;
        Speed = gunDef.speed;
        ShotDelay = gunDef.delay;
        _currentHealth = _maximumHealth;
        _startPosition = transform.position;
		OnInitialize();
    }

    public void Shoot()
    {
		
        if (_IsAutoShootingEnabled)
        {
            OnPreShoot();
            rigidbody2D.velocity = transform.up.normalized * _speed;
			//if (bulletSound != null)
			   // AudioManager.instance.PlaySingle (bulletSound,0.050f);
            OnPostShoot();

            if (Speed == 0)
            {
                Debug.LogWarning("This bullet will not move because the speed is 0. Set the speed in the scriptable object.");
                DestroySelf();
            }
        }
        else
        {
            Debug.Log("AutoShooting Disabled");
        }
    }

    private void Update()
    {
        OnUpdate();   
    }

    void LateUpdate()
	{
        OnLateUpdate();
		if (ExceededRange()) {
			DestroySelf ();
		}
	}

	public void GetDamaged(float damageAmount)
	{
		_currentHealth -= damageAmount;
		if (_currentHealth <= 0) {
			DestroySelf ();
		}
	}

	protected virtual void DestroySelf()
	{
		GetReleasedToPool ();
        OnDestroySelf();
	}

	public virtual void GetReleasedToPool() //OnVisibleHandler will call this
	{
		UbhObjectPool.Instance.ReleaseGameObject (this.gameObject);
	}

    bool ExceededRange()
    {
        return Vector3.SqrMagnitude(transform.position -  _startPosition) >= (_range*_range);
    }

	public float Damage {
		get {
			return _damage;
		}
		set {
			_damage = value;
		}
	}

	public float MaximumHealth {
		get {
			return _maximumHealth;
		}
		set {
			_maximumHealth = value;
		}
	}

	public float CurrentHealth {
		get {
			return _currentHealth;
		}
		set {
			_currentHealth = value;
		}
	}

	public float Range {
		get {
			return _range;
		}
		set {
			_range = value;
		}
	}

	public float Speed {
		get {
			return _speed;
		}
		set {
			_speed = value;
		}
	}

	public float ShotDelay {
		get {
			return _shotDelay;
		}
		set {
			_shotDelay = value;
		}
	}

    public Vector3 StartPosition
    {
        get
        {
            return _startPosition;
        }

    }

	public bool ShootingEnabled {
		get {
			return _IsAutoShootingEnabled;
		}
		set {
			_IsAutoShootingEnabled = value;
		}
	}
}
