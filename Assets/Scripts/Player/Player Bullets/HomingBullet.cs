﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HomingBullet : BasePlayerBullet {

	[SerializeField]
	private Transform _target =  null;
	[SerializeField]
	private Vector3 _direction;

    float _maxRangeSqr;

	private void GetNewTarget()
	{
        if (_target == null)
        {
            //find all enemies currently active in the scene
            GameObject[] targets = GameObject.FindGameObjectsWithTag("Enemy");
            
            for(int i =0;i<targets.Length;i++)
               if(Vector3.SqrMagnitude(StartPosition - targets[i].transform.position) <= _maxRangeSqr)
                {
                    _target = targets[i].transform;
                    _target.position= new Vector3(_target.position.x, _target.position.y, 0);
                    break;
                }
        }
	}

	private void LookAtTarget()
	{
        if (_target != null)
        {
            //get direction of _target(destination) from bullet(source)
            _direction = _target.position - transform.position;

            //set transform green axis towards the direction of _target
            transform.up = _direction;
        }
	}

    bool TargetIsStillValid()
    {
        if (_target != null)
        {
           return Vector3.SqrMagnitude(_target.position - StartPosition)<=_maxRangeSqr;
        }

        return false;
    }
    
    protected override void OnInitialize()
    {
        _maxRangeSqr = Range * Range;//using sqr distance
    }

    protected override void OnUpdate()
    {

    }

    protected override void OnLateUpdate()
    {
        if (!TargetIsStillValid())
        {
            GetNewTarget();
        }
        if(_target!=null)
            LookAtTarget();//change orientation to track moving targets

    }

    protected override void OnDestroySelf()
    {
        
    }

    protected override void OnPreShoot()//only happens once
    {
        GetNewTarget();
        LookAtTarget();
    }

    protected override void OnPostShoot()
    {
        
    }

    //override PlayerBullet's OnEnable()
    //void OnEnable()
    //{
    //	//set values fed by Gun Triggers
    //	base.Initialize ();

    //	GetNewTarget();

    //	if (_target != null) {
    //		//calculate direction in which the projectile is to be started
    //		LookAtTarget();

    //		//shoot bullet in the direction of _target
    //		rigidbody2D.velocity = transform.up * Speed;
    //	}
    //}

    //private void Update()
    //{
    //	if(_target==null)
    //		GetNewTarget ();

    //	if (_target != null) {
    //		LookAtTarget ();

    //		//change direction of the bullet according to new direction
    //		rigidbody2D.velocity = _direction.normalized * rigidbody2D.velocity.magnitude;
    //	} 
    //	else    // there is no enemy to target so do not shoot 
    //		DestroySelf ();

    //}
}
