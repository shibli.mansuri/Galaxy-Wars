﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
public class GunDefinition : ScriptableObject {

	public GunMetaData[] gunDefinitions = new GunMetaData[5];

    public GunMetaData GetMetaDataForGunType( GunType _gunType)
    {
        var findType = from definition in gunDefinitions
                       where definition.gunType == _gunType
                       select definition;
        return findType.FirstOrDefault();
    }
}

[Serializable]
public class GunMetaData{
	public GunType gunType;
	public GunDefinitionParams[] gunParams = new GunDefinitionParams[4];

}

[Serializable]
public struct GunDefinitionParams {
    public string prefabPath;
    public float damagePerBullet;
	public float maxBulletHealth;
	public float range;
	public float speed;
	public float delay;
}
