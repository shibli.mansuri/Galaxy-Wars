﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class GunModel : MonoBehaviour {

	public enum CombatClass { Single, Double, Triple, Spread, Homing, Canon, Laser, Locked }; 

	public CombatClass combatClass;
	public int level; 

	public GunModel(){
		combatClass = CombatClass.Locked;
		level = 0;
	}

	public GunModel(CombatClass t, int l){
		combatClass = t;
		level = l;
	} 

	public void SetLevel(int l){
		if(l <= 4)
			level = l;
	}

    public bool IsUnlocked()
    {
        return level > 0 && combatClass != CombatClass.Locked;
    }

}
