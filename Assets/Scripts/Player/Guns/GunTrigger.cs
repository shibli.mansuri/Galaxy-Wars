﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunTrigger : MonoBehaviour {

    private GameObject _bulletAsset;
	[SerializeField]
	private GunDefinitionParams _gunDefinition;
    IEnumerator fireCoRoutine;
    bool _canFire = true;

    private void OnEnable()
    {
        
        EventHub.AttachListener(DataConstants.STR_EVENT_GAME_PAUSED, DisableFiring);
        EventHub.AttachListener(DataConstants.STR_EVENT_GAME_UNPAUSED, EnableFiring);
    }

    private void OnDisable()
    {
        EventHub.DetachListener(DataConstants.STR_EVENT_GAME_PAUSED, DisableFiring);
        EventHub.DetachListener(DataConstants.STR_EVENT_GAME_UNPAUSED, EnableFiring);

    }
		


	public void Initialize(GunDefinitionParams gunDefParams)
	{
        _canFire = !LevelManager.instance.IsGamePaused;
        _bulletAsset = Resources.Load(gunDefParams.prefabPath,typeof(GameObject)) as GameObject;
        _gunDefinition = gunDefParams;
        fireCoRoutine = FireGun();
        StartCoroutine(fireCoRoutine);
    }

	private void ShootBullet(GameObject bulletGameObject)
	{
		//get reference of bullets
		BasePlayerBullet[] bullets = bulletGameObject.GetComponentsInChildren<BasePlayerBullet>();

		foreach (BasePlayerBullet bullet in bullets) {
            bullet.Initialize(_gunDefinition);
            bullet.Shoot();

		}
	}

	IEnumerator FireGun()
	{

        while (true)
        {
            if (_canFire)
            {
                GameObject bullet = UbhObjectPool.Instance.GetGameObject(_bulletAsset, transform.position, transform.rotation);
                ShootBullet(bullet);
            }

            yield return new WaitForSeconds(_gunDefinition.delay);
        }
	}

    void EnableFiring()
    {
        _canFire = true;
    }

    void DisableFiring()
    {
        _canFire = false;
    }

    public GunDefinitionParams GunDefinition {
		get {
			return _gunDefinition;
		}
		set {
			_gunDefinition = value;
		}
	}
}
