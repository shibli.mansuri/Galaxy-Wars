﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum SocketState { Disabled, Locked, Unlocked };
[Serializable]
public enum GunType { Single, Double, Triple, Spread, Homing, Canon, Laser };

[Serializable]
public class GunSocket
{
    public GunType _gunType;
    public SocketState _state;
    public int _level;
    public int _maxLevel;

    public GunSocket(SocketState state= SocketState.Disabled)
    {
        _gunType = GunType.Single;
        _state = state;
        _level = 0;
        _maxLevel = 4;
    }
    
    public GunSocket(int level, int maxLevel= 4, GunType gunType = GunType.Single, SocketState socketState = SocketState.Disabled)
    {
        _gunType = gunType;
        _level = level;
        _state = socketState;
        _maxLevel = maxLevel;
    }

    public void EnableSocket()
    {
        _state = SocketState.Locked;
    }

    public void UnlockSocket()
    {
        _state = SocketState.Unlocked;
    }

    public void SetLevel(int newLevel)
    {
        if (newLevel >= 0 && newLevel < _maxLevel)
            _level = newLevel;
        else Debug.LogError("GUNSOCKET: Invalid parameter input for new gunLeve");
    }

    public void IncreaseLevel()
    {
        SetLevel(_level + 1);
    }
    
    public void DecreaseLevel()
    {
        SetLevel(_level - 1);
    }

    public SocketState GetState()
    {
        return _state;
    }

    public int GetLevel()
    {
        return _level;
    }

    public GunType GetGunType()
    {
        return _gunType;
    }

    public void SetGunType(GunType newType)
    {
        if (_gunType != newType)
            _gunType = newType;
    }
}

