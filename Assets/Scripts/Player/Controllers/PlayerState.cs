﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : ScriptableObject {

	public GunModel primaryGun, secondaryGun;
	public GunModel[] assistGun;
	public int numOfAssistGunsUnlocked;
	public MegabombConsumable megaBomb;
	public ShieldConsumable shield;
	public Magnet magnet;
	public int coins;

	void Awake () {
		/*assistGun = new GunModel[6];
		// Get Values from PlayerPrefs and feed below.
		for (int i = 0; i < 6; i++) 
			assistGun [i] = new GunModel ();
		numOfAssistGunsUnlocked = 0;
		primaryGun = new GunModel (GunModel.GunType.Single, 1);
		secondaryGun = new GunModel (GunModel.GunType.Locked, 0);
		
		shield = new ShieldConsumable (1, 1);
		megaBomb = new MegabombConsumable(2, 1);
		magnet.isUnlocked = true;
	*/
	}
		
}
