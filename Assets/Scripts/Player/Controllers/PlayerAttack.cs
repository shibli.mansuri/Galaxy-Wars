﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {
    [SerializeField]
    GameObject _BulletPrefab;
    [SerializeField]
    float _ShotDelay;

	public AudioClip[] sounds;

	
	IEnumerator Start () {
		
        while(true)
        {
            Shot();
            yield return new WaitForSeconds(_ShotDelay);
        }
	}

    void Shot()
    {
        if (_BulletPrefab != null)
        {
            UbhObjectPool.Instance.GetGameObject(_BulletPrefab, transform.position, transform.rotation);
			AudioManager.instance.PlayRandomSfx (sounds);
        }
    }

    
}
