﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour,IHealth
{
    [SerializeField]
    public float _MaxHealth = 2;
    public float _CurrentHealth;
	bool isShieldOn;

    private void Awake()
    {
		isShieldOn = false;
        _CurrentHealth = _MaxHealth;
	}

	void OnEnable()
	{
		EventHub.AttachListener ("StopDamage", StopDamage);
		EventHub.AttachListener("ResumeDamage", ResumeDamage);

	}

	void OnDisable()
	{
		EventHub.DetachListener ("StopDamage", StopDamage);
		EventHub.DetachListener("ResumeDamage", ResumeDamage);
	}

    void GetDamaged(float amount = 1f)
    {
		AudioManager.instance.PlayPlayerHitSound ();
        _CurrentHealth -= amount;
          //Debug.Log("Player Damaged (Health = " + _CurrentHealth + " )");
        if (_CurrentHealth <= 0)
            Die();
    }

    void Die()
    {
        Destroy(this.gameObject);
		EventHub.TriggerEvent ("PlayerDeathGameOver");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
		if (other.gameObject.CompareTag (DataConstants.STR_ENEMY_BULLET_TAG) && isShieldOn == false) 
		{
			
			if (other.gameObject.GetComponent<ReleaseToPool> ()) {
				GetDamaged ();
				ReleaseToPool rtp = other.gameObject.GetComponent<ReleaseToPool> ();
				rtp.GetReleasedToPool ();
			} else if (other.gameObject.GetComponent<MortorBullet> ()) 
			{
				GetDamaged ((other.gameObject.GetComponent<MortorBullet> ().damage));
				
			} 

		}

        //check if player is colliding with flying enemies
		if (other.gameObject.layer == 9) {
           float damage = (other.GetComponent<BaseEnemy>().DamageToPlayer * _MaxHealth);
           GetDamaged (damage);
		}
			
    }

    public float GetCurrentHealthScale()
    {
        return Mathf.Clamp01((float)_CurrentHealth / (float)_MaxHealth);
    }

	private void StopDamage(){
		
		isShieldOn = true;
	}

	private void ResumeDamage(){
		isShieldOn = false;
	}

	public void	UpgradeHealth(int incrementValue=1)
	{
		if(_CurrentHealth+incrementValue<=_MaxHealth)
		_CurrentHealth += incrementValue;
		else
		_CurrentHealth = _MaxHealth;

	}
}