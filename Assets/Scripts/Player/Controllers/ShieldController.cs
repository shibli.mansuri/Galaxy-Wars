﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour {

	int health;
	void Start()
	{
		health = PlayerInventory.instance.GetShield ().GetLevel ();	
		gameObject.GetComponent<SpriteRenderer> ().enabled = false;
	}

	void Update()
	{
		if(PlayerInventory.instance.GetShield().isActive() == true)
			gameObject.GetComponent<SpriteRenderer> ().enabled = true;
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "EnemyFire" && PlayerInventory.instance.GetShield().isActive() == true) {
			ReleaseToPool rtp = collider.gameObject.GetComponent<ReleaseToPool> ();
			rtp.GetReleasedToPool ();
			//UbhObjectPool.Instance.ReleaseGameObject(collider.gameObject);
			health--;
			if (health == 0) {
				gameObject.GetComponent<SpriteRenderer> ().enabled = false;
				PlayerInventory.instance.GetShield().OnFinish ();
				health = PlayerInventory.instance.GetShield ().GetLevel ();	
			}

		}
	}
}
