﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float speed = 3f;
	Transform myTransform;
	public Vector3 desiredPos;
	public float yoffset=1f;

	bool hasTouched = false; // tells if the user has ever give input since the level has started

	private void Awake()
	{
		myTransform = this.transform;
		//cameraClass = Camera.main.GetComponent<CameraMovement>();
	}

	void Update () {
		GetUserInput();
		MovePlayer ();
	}

	void GetUserInput()
	{
		Vector2 inputPos;
		if (Input.GetMouseButton(0))
		{
			hasTouched = true;
            UnPauseGame();
			inputPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			desiredPos = new Vector3 (Camera.main.ScreenToWorldPoint (inputPos).x, Camera.main.ScreenToWorldPoint (inputPos).y+yoffset, myTransform.position.z);
		}
		else if (Input.touchCount > 0)
		{
			hasTouched = true;
            UnPauseGame();
			inputPos = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
			desiredPos = new Vector3(Camera.main.ScreenToWorldPoint(inputPos).x, Camera.main.ScreenToWorldPoint(inputPos).y+yoffset, myTransform.position.z);
		}
		else//no user input
		{
			desiredPos = myTransform.position ;
			desiredPos.y = myTransform.position.y + 0.12f;//0.12f;//0.12f; 
            PauseGame();
			
		}
	}

    void UnPauseGame(){
        if (UIManager.instance && UIManager.instance.isGameSemiPaused() == true) // checks if the game is in semi pause
            UIManager.instance.SemiPause(false);
    }

    void PauseGame(){
        if (UIManager.instance && hasTouched == true && UIManager.instance.isGamePaused() == false)
        {

            UIManager.instance.SemiPause(true);

            if (GameplayUIManager.EnableSemiPause == false)
                UIManager.instance.SemiPause(false);

        }
    }

	bool ReachedDesiredPosition()
	{

		return Vector3.SqrMagnitude(desiredPos - myTransform.position) < 0.01f;
	}

	void MoveToDesiredPos()
	{
		myTransform.position += (desiredPos - myTransform.position) * Time.deltaTime * speed;
	}

    void MovePlayer()
	{
        if (!ReachedDesiredPosition())
        {
            MoveToDesiredPos();
        }   
	}


}

