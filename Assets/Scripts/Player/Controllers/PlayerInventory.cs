﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerInventory : MonoBehaviour {

	
	public static PlayerInventory instance;
	private static MegabombConsumable _megaBomb;
	private static ShieldConsumable _shield;
	private static int coins;

	private int _odFrags;
    private int _powerGems;
    private int _evoStone;

    private int _shipEvoLevel;
    private ShipLoadOut _shipLoadout;
    private ChestManager _chestManager;

    
	void OnEnable()
	{
		EventHub.AttachListener ("Gameover", AddCoinsCollected);


	}

	void OnDisable()
	{
		EventHub.DetachListener ("Gameover", AddCoinsCollected);
	}

    void Awake(){

		if (instance == null) {
			instance = FindObjectOfType<PlayerInventory>();
			DontDestroyOnLoad (gameObject);
		}
		else if (instance != this)
			Destroy (gameObject);


        _chestManager = new ChestManager();
		Initialize();
	}

	void Start()
	{
        //Initialize();

	}

    void Initialize()
    {
        _odFrags = PlayerPrefsManager.GetODFrag();
        _powerGems = PlayerPrefsManager.GetPowerGems();
        _evoStone = PlayerPrefsManager.GetEvolutionStone();
        _shipEvoLevel = PlayerPrefsManager.GetShipEvoLevel();
        _shipLoadout = new ShipLoadOut();
		_shipLoadout.Initialize();

		coins = PlayerPrefsManager.GetCoins ();
		_megaBomb = new MegabombConsumable (PlayerPrefsManager.GetMegabombLevel(), PlayerPrefsManager.GetMegabombQuantity());
		_shield = new ShieldConsumable (PlayerPrefsManager.GetShieldLevel(), PlayerPrefsManager.GetShieldQuantity());
    }

    public void AddReward(RewardItems items)
    {
        AddCoins(items.Coins);
        AddShield(items.Shields);
        AddMegabomb(items.MegaBomb);
        ODFrags += items.OdFrags;
        PowerGems += items.PowerGems;
        EvoStone += items.EvoStones;
    }

	public void AddCoinsCollected()
	{
		coins += LevelManager.instance.coinsCollected;
		PlayerPrefsManager.SetCoins ();
	}

	public void AddCoins(int profit)
	{
		coins += profit;
		PlayerPrefsManager.SetCoins ();
	}

	public void SubtractCoins(int cost)
	{
		coins -= cost;
		PlayerPrefsManager.SetCoins ();
	}

	public int GetCoins()
	{
		return coins;
	}

	public MegabombConsumable GetMegabomb()
	{
		return _megaBomb;
	}

	public ShieldConsumable GetShield()
	{
		return _shield;
	}

	public void UpgradeMegabomb()
	{
		//int level = PlayerPrefsManager.GetMegabombLevel ();
		int level = _megaBomb.GetLevel();
		_megaBomb.SetLevel (level + 1);
		PlayerPrefsManager.SetMegabombLevel ();
	}

	public void AddMegabomb(int amount=1)
	{
		//int qty = PlayerPrefsManager.GetMegabombQuantity ();
		int qty = _megaBomb.GetQuantity();
		_megaBomb.SetQuantity (qty + amount);
		PlayerPrefsManager.SetMegabombQuantity();
	}

	public void SubtractMegabomb()
	{
		//int qty = PlayerPrefsManager.GetMegabombQuantity ();
		int qty = _megaBomb.GetQuantity();
		_megaBomb.SetQuantity (qty - 1);
		PlayerPrefsManager.SetMegabombQuantity();
	}

	public void AddShield(int amount=1)
	{
		//int qty = PlayerPrefsManager.GetShieldQuantity ();
		int qty = _shield.GetQuantity();
		_shield.SetQuantity (qty + amount);
		PlayerPrefsManager.SetShieldQuantity ();
	}

	public void SubtractShield()
	{
		//int qty = PlayerPrefsManager.GetShieldQuantity ();
		int qty = _shield.GetQuantity();
		_shield.SetQuantity (qty - 1);
		PlayerPrefsManager.SetShieldQuantity ();
	}

	public void UpgradeShield()
	{
		//int level = PlayerPrefsManager.GetShieldLevel ();
		int level = _shield.GetLevel ();
		_shield.SetLevel (level + 1);
		PlayerPrefsManager.SetShieldLevel ();
	}

	public void UpgradeMagnet()
	{
		int level = PlayerPrefsManager.GetMagnetLevel ();
		PlayerPrefsManager.SetMagnetLevel (level + 1);
	}
	
    public int ODFrags
    {
        get
        {
            return _odFrags;
        }

        set
        {
            _odFrags = value;
            PlayerPrefsManager.SetODFRAG(_odFrags);
        }
    }

    public int PowerGems
    {
        get
        {
            return _powerGems;
        }

        set
        {
            _powerGems = value;
            PlayerPrefsManager.SetPowerGems(_powerGems);
        }
    }

    public int EvoStone
    {
        get
        {
            return _evoStone;
        }

        set
        {
            _evoStone = value;
            PlayerPrefsManager.SetEvoStone(_evoStone);
        }
    }

    public int ShipEvoLevel
    {
        get
        {
            return _shipEvoLevel;
        }

        set
        {
            _shipEvoLevel = value;
            PlayerPrefsManager.SetShipEvoLevel(_shipEvoLevel);
        }
    }

    public ShipLoadOut ShipLoadout
    {
        get
        {
            return _shipLoadout;
        }

        private set
        {
            _shipLoadout = value;
        }
    }

    public ChestManager ChestManager
    {
        get
        {
            return instance._chestManager;
        }
    }
	
}


public static class PlayerInventoryExtensions {

    public static void AddChest(this PlayerInventory p, ChestModel c)
    {
        p.ChestManager.AddChest(c);
    }

    public static int ChestCount(this PlayerInventory p)
    {
        return p.ChestManager.GetCount();
    }

}



