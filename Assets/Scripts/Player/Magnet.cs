﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour {

	public int level;
	public float force;
	public bool isUnlocked;

	void Start () {
		level = PlayerPrefsManager.GetMagnetLevel ();
		CircleCollider2D collider = GetComponent<CircleCollider2D>();
		collider.radius = 1f + (level * 0.2f);
		force = level * 1.5f;
	}

	void Update () {
		
	}

	private void OnTriggerStay2D(Collider2D collider){
		if (collider.gameObject.tag == "Collectible") {
			AttractCollectible (collider.gameObject);
		}
	}

	private void AttractCollectible(GameObject collectible){
		Vector2 randomForce = Random.insideUnitCircle * 250f;
		randomForce.y *= randomForce.y > 0 ? -1:1;
		collectible.transform.position = Vector3.MoveTowards (collectible.transform.position, transform.position, force*Time.deltaTime);
		if (collectible.GetComponent<Rigidbody2D> () != null) {
			Rigidbody2D rigidbody2D = collectible.GetComponent (typeof(Rigidbody2D)) as Rigidbody2D;
			rigidbody2D.AddTorque (randomForce.SqrMagnitude ());
		}
	}

}
