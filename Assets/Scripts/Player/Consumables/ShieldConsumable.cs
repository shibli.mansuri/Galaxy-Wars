﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldConsumable : Consumable {

	public ShieldConsumable(int lev, int qty): base(lev, qty){}  //level is health in this case

	void Awake()
	{
		isActivated = false;
	}
		
	public override void Activate ()
	{
		if (isActivated == false) {
			PlayerInventory.instance.SubtractShield ();
			level = 10;
			isActivated = true;
			EventHub.TriggerEvent ("StopDamage");
		}

	}

	public override void OnFinish ()
	{
		isActivated = false;
		EventHub.TriggerEvent ("ResumeDamage");
	}
		
}
