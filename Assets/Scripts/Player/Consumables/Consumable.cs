﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Consumable: MonoBehaviour {

	//[SerializeField]
	protected int level; // if 0, then locked
	//[SerializeField]
	protected int quantity;
	//[SerializeField]
	public static bool isActivated;

	public Consumable(int lev, int qty){
		this.level = lev;
		this.quantity = qty;
	}
		

	public void SetLevel(int l){
		this.level = l;
	}

	public void SetQuantity(int qty){
		this.quantity = qty;
	}
		

	public int GetQuantity(){
		return this.quantity;
	}

	public bool isActive(){
		return isActivated;	
	}

	public void SetIsActive(bool status)
	{
		isActivated = status;
	}

	public int GetLevel(){
		return level;
	}

	public abstract void Activate();
	public abstract void OnFinish();

}
