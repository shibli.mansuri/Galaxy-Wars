﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MegabombConsumable : Consumable {

	public GameObject explodeVFX;
	public AudioClip explodeSFX;
	private CircleCollider2D collider;
	public MegabombConsumable(int lev, int qty): base(lev,qty){}

	void Awake()
	{
		isActivated = false;
		collider = GetComponent<CircleCollider2D> ();
		collider.enabled = false;
		level = PlayerInventory.instance.GetMegabomb().GetLevel();
	}

	void OnEnable(){
		EventHub.AttachListener ("MegabombActivationFromGUI", Activate);
	}

	void OnDisable(){
		EventHub.DetachListener ("MegabombActivationFromGUI", Activate);
	}

	/*void Update()
	{
		if (Input.GetKeyDown (KeyCode.M))
			Activate ();
		else {
			collider.radius = 0.5f;
			collider.enabled = false;
		}
	}*/
	public override void Activate ()
	{
		
		collider.enabled = true;
		//play SFX & VFX
		if(explodeVFX!=null){
			GameObject parent = GameObject.FindWithTag("Megabomb");
			AudioManager.instance.PlaySingle (explodeSFX);
			Instantiate(explodeVFX, transform.position, Quaternion.identity,parent==null?null:parent.transform);
			Camera.main.DOShakePosition (1f, 2);
		}

		while(collider.radius <= 11.8f)
			collider.radius += 1f * Time.deltaTime;

		PlayerInventory.instance.SubtractMegabomb ();
		Invoke("OnFinish", 1f);
	}

	public override void OnFinish()
	{
		collider.radius = 0.5f;
		collider.enabled = false;
	}

}