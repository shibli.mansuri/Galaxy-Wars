﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentController : MonoBehaviour {

	public Transform _camera;
    public GameObject[] _backgrounds;
    public EnvironmentView _view;

	private int index = 0;

    private void Awake()
    {
        _camera = Camera.main.transform;
    }

    void Update()
	{

		if (HasCameraReachedPosition()) {
			TileBackground ();
		}

	}

	private void TileBackground()
	{		
		index = SwapIndex();
		_view.ResetPosition(index);

	}

	private bool HasCameraReachedPosition()
	{
		if (_camera.position.y - Backgrounds [index].transform.position.y >= 0f)
			return true;
		else
			return false;
	}

	private int SwapIndex()
	{
		return (index < 1) ? 1 : 0;
	}

    public void SetBackground(Sprite backgroundSprite)
    {
        _backgrounds[0].GetComponentInChildren<SpriteRenderer>().sprite = _backgrounds[1].GetComponentInChildren<SpriteRenderer>().sprite = backgroundSprite; 
    }

    public void SetForeground(Sprite fgSprite)
    {
        GameObject foreground = _view.foreground.gameObject;
        foreground.GetComponent<SpriteRenderer>().sprite = fgSprite;
    }

    public GameObject[] Backgrounds
    {
        get
        {
            return _backgrounds;
        }

        set
        {
            _backgrounds = value;
        }
    }
}
