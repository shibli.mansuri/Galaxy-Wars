﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentView : MonoBehaviour {

    public GameObject[] _backgrounds;
    public Transform foreground;
	private Vector3 initalForegroundPosition;
	private float direction;

    void Awake()
	{
		direction = 1f;
		initalForegroundPosition = foreground.localPosition;
	}

	void Update()
	{
		MoveForegrounds ();
	}

	public void ResetPosition(int index)
	{
		float yPositionDifference = Mathf.Abs (Backgrounds[0].transform.localPosition.y-Backgrounds[1].transform.localPosition.y);
		yPositionDifference  += yPositionDifference ;
		Backgrounds [index].transform.localPosition = new Vector3 (0f, Backgrounds [index].transform.localPosition.y+yPositionDifference , 0f);
	}

	public void MoveForegrounds()
	{
		foreground.localPosition = new Vector3 (0f, Mathf.Lerp (foreground.localPosition.y, foreground.localPosition.y+direction , 3.5f* Time.deltaTime), -1f);
	}

	public void ResetForeground()
	{
		direction = (direction == 1) ? -1 : 1;
	}

    public GameObject[] Backgrounds
    {
        get
        {
            return _backgrounds;
        }

        set
        {
            _backgrounds = value;
        }
    }
}