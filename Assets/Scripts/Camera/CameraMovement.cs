﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    float _movementSpeed = 3f;
    Transform _myTransform;

    private void Awake()
    {
        _myTransform = this.transform;
    }

    void Update()
    {
        Vector3 pos = _myTransform.position;
        pos.y += _movementSpeed * Time.deltaTime;
        _myTransform.position = pos;
    }

    public float CurrentMovementSpeed
    {
        set
        {
            _movementSpeed = value;
        }
        get
        {
            return _movementSpeed;
        }
    } 
}
