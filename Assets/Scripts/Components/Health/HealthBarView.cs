﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBarView : UbhMonoBehaviour {

    public Slider _UISlider;
    public Image _UIBarImage;

    public void UpdateHealthView(float currentHealth)
    {
        currentHealth = Mathf.Clamp01(currentHealth);
        _UISlider.value = currentHealth;
        _UIBarImage.color = Color.Lerp(Color.red, Color.green, currentHealth);
    }
}
