﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarComponent : UbhMonoBehaviour {
    [SerializeField]
    Vector2 _Offset= new Vector2(0, -0.85f);
    [SerializeField]
    Vector2 _Scale= Vector2.one;

    IHealth healthObject;
    HealthBarView view;
    private void Awake()
    {
        healthObject = gameObject.GetComponent<IHealth>();
        if (healthObject == null)
            Debug.LogError(this.gameObject.name + " does not contain an IHealth class");
        else
            SpawnHealthBar();
    }

    private void Update()
    {
        DrawHealth();
    }

    void DrawHealth()
    {
        if (healthObject != null)
        {
            view.UpdateHealthView(healthObject.GetCurrentHealthScale());
            //view.transform.localPosition = _Offset;
        }
            
    }

	public void DisableHealthBar()
	{

		view.gameObject.SetActive (false);
	}

    void SpawnHealthBar()
    {
        view = (Instantiate(Resources.Load(DataConstants.RESOURCES_HEALTHBAR_PATH), transform) as GameObject).GetComponent<HealthBarView>();
        view.transform.localPosition = _Offset;
        view.transform.localScale = _Scale;
    }
}
