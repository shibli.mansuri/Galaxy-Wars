﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class OnBecameInvisibleHandler : MonoBehaviour {

    bool isVisible = false;

    [SerializeField]
    UnityEvent OnInvisible;

    private void OnBecameVisible()
    {
        isVisible = true;
    }
    private void OnBecameInvisible()
    {
            OnInvisible.Invoke();
    }
}
