﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnBecameVisibleHandler : MonoBehaviour {

	[SerializeField]
	UnityEvent OnVisible;

	private void OnBecameVisible()
	{
		if (OnVisible != null)
			OnVisible.Invoke();
	}
}
