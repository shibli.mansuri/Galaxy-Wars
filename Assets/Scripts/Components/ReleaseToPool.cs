﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReleaseToPool : UbhMonoBehaviour {

	public void GetReleasedToPool()
    {
        UbhObjectPool.Instance.ReleaseGameObject(transform.gameObject);
    }
}
