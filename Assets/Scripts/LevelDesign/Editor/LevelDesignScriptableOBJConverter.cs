﻿using System;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
public class LevelDesignScriptableOBJConverter{

    const string assetPath = "Assets/ScriptableObjects/LevelDesign.asset";
    LevelDesign scriptableObject;
    SerializedObject serializedScriptableObj;
    SerializedObject wavesSerializedObj;
    SerializedProperty wavesListSerializedProperty;
    SerializedProperty levelsListSerializedProperty;
    ReorderableList wavesReorderableList;

    WavesReorderableListDrawer listDrawer;

    int currentIndex = 0;

    public  LevelDesignScriptableOBJConverter (){
        listDrawer = new WavesReorderableListDrawer();
        scriptableObject = AssetDatabase.LoadAssetAtPath(assetPath, typeof(LevelDesign)) as LevelDesign;
        SetWavesList(currentIndex);
        
    }

    public SerializedObject GetSerializedRoot()
    {
        if (serializedScriptableObj == null)
            serializedScriptableObj = new SerializedObject(scriptableObject);
        return serializedScriptableObj;
    }

    public ReorderableList GetWavesListAtLevel(int levelIndex)
    {
        if(currentIndex!=levelIndex)
        {
            currentIndex = levelIndex;
            SetWavesList(currentIndex);
        }
        return wavesReorderableList;
    }

    
    public void SetWavesList(int index)
    {
        Debug.Log("index " + index);
        if (index < GetLevelCount() && index>=0)
        {
            SerializedProperty leveldefinition = GetLevelsListSerializedProperty().GetArrayElementAtIndex(index);
            wavesListSerializedProperty = leveldefinition.FindPropertyRelative("waves");
            wavesSerializedObj = wavesListSerializedProperty.serializedObject;
            wavesReorderableList = new ReorderableList(wavesSerializedObj, wavesListSerializedProperty, true, true, true, true);
            listDrawer.Init(wavesReorderableList,index);
        }
    }

    public int GetWaveCount()
    {
        if (wavesReorderableList != null)
        {
            return wavesReorderableList.count;
        }
        return -1;
    }


    public SerializedObject GetWavesSerializedObjectParent()
    {
        return wavesSerializedObj;
    }

    SerializedProperty GetLevelsListSerializedProperty()
    {
        if (levelsListSerializedProperty == null)
            levelsListSerializedProperty = GetSerializedRoot().FindProperty("levels");
        return levelsListSerializedProperty;
    }

    public int GetLevelCount()
    {
        return scriptableObject.levels.Count;
    }

    public bool IsValid()
    {
        return scriptableObject;
    }
}


public  class WavesReorderableListDrawer{
    int currentLevel;
    ReorderableList list;
    public void Init(ReorderableList rList, int index= 1)
    {
        list = rList;
        list.drawHeaderCallback = DrawHeader;
        list.elementHeightCallback = GetElementHeight;
        list.drawElementCallback = DrawElement;
        
        currentLevel = index + 1;
    }
    
    private void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
    {
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        rect.y += 2;
        EditorGUI.PropertyField(new Rect(rect.x, rect.y, 295, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("prefab"), new GUIContent("Enemy"));
        EditorGUI.PropertyField(new Rect(rect.x+315, rect.y, 100, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("count"), new GUIContent(""));
        rect.y += EditorGUIUtility.singleLineHeight + 5;
        EditorGUI.PropertyField(new Rect(rect.x, rect.y, 260, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("startWait"), new GUIContent("Start Delay"));
        EditorGUI.PropertyField(new Rect(rect.x + 280, rect.y, 210, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("inbetweenWait"), new GUIContent("Interval"));
        rect.y += EditorGUIUtility.singleLineHeight + 5;
       
    }
    private float GetElementHeight(int index)
    {
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        return EditorGUI.GetPropertyHeight(element);
    }

    private void DrawHeader(Rect rect)
    {
        EditorGUI.LabelField(rect,"Level " + currentLevel);
    }
}


