﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class LevelEditor : EditorWindow {
    bool totalEnemiesFoldoutStatus = false; // will become an array in the future
    bool levelSettingsFoldoutStatus = false;    // will become an array in the future
    LevelDesignScriptableOBJConverter levelDesign;
    int m_CurrentIndex = 0;
    Vector2 scrollPos;


    [MenuItem("Tools/Level Editor %l")] // %l creates keyboard shortcut (Ctrl + L) for the level editor 
    static void CreateWindow()
    {
        // Gets existing level editor window; creates if does not exist
        LevelEditor window = (LevelEditor)EditorWindow.GetWindow(typeof(LevelEditor));
    }

    void OnEnable()
    {
       /* LevelDesign levelDesign = new LevelDesign();
        levelDesignSO = new SerializedObject(levelDesign);
        wavesList = new ReorderableList(levelDesignSO, levelDesignSO.FindProperty("levels"), true, true, true, true);
        wavesList.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "Waves");
        wavesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            rect.y += 2;
            rect.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(rect, wavesList.serializedProperty.GetArrayElementAtIndex(index));
        };*/
        levelDesign = new LevelDesignScriptableOBJConverter();
     
        if (levelDesign.IsValid())
        {
            levelDesign.SetWavesList(m_CurrentIndex);

        }

    }

    void OnInspectorUpdate()
    {
        Repaint();
    } 

    void OnGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal(); // Start of main window
        DrawSidebar();
        EditorGUILayout.Space();
        DrawWavesList();
        EditorGUILayout.EndHorizontal(); // end of main window
    }

    private void DrawWavesList()
    {

        Rect scrollRect = EditorGUILayout.BeginVertical();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);
        levelDesign.GetSerializedRoot().Update();
        levelDesign.GetWavesListAtLevel(m_CurrentIndex).DoLayoutList();
        levelDesign.GetSerializedRoot().ApplyModifiedProperties();
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
    }
    private void DrawSidebar()
    {
        // for dummy data
        int i = 6;
        
        // Start Sidebar
        EditorGUILayout.BeginVertical(GUILayout.Width(200));
        if(GUILayout.Button("Level 1"))
        {
            // do something
        }
        EditorGUILayout.LabelField("Summary", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Total Waves: " + i);
        totalEnemiesFoldoutStatus = EditorGUILayout.Foldout(totalEnemiesFoldoutStatus, "Enemies: " + i);
        if (totalEnemiesFoldoutStatus)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("Total Movable Enemies: " + i);
            EditorGUILayout.LabelField("Total Immovable Enemies: " + i);
            EditorGUI.indentLevel--;
        }

        EditorGUILayout.LabelField("Boss Health: " + i);

        levelSettingsFoldoutStatus = EditorGUILayout.Foldout(levelSettingsFoldoutStatus, "Level Settings");
        if (levelSettingsFoldoutStatus)
        {
            string[] _choices = new[] { "foo", "foobar" };
            int _choiceIndex = 0;

            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(272));

            EditorGUILayout.LabelField("Background", GUILayout.MaxWidth(86));
            _choiceIndex = EditorGUILayout.Popup(_choiceIndex, _choices, GUILayout.MaxWidth(50));

            EditorGUILayout.LabelField("Foreground", GUILayout.MaxWidth(86));
            _choiceIndex = EditorGUILayout.Popup(_choiceIndex, _choices, GUILayout.MaxWidth(50));

            EditorGUILayout.EndHorizontal();

            float x = 3.5f;
            x = EditorGUILayout.FloatField("Camera Speed",x);
            i = EditorGUILayout.IntField("Number of Waves", i);

            EditorGUI.indentLevel--;
        }

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Duplicate"))
        {
            // do something
        }

        if (GUILayout.Button("Reset"))
        {
            // do something
        }

        if (GUILayout.Button("Remove"))
        {
            // do something
        }
        EditorGUILayout.EndHorizontal();

        //End Sidebar
        EditorGUILayout.EndVertical();
    }
}
