﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class LevelEditorWindow : EditorWindow{
    const int TOP_PADDING = 2;
    const string HELP_TEXT = "Could not load Level Design Asset from ";
    

    static Vector2 s_WindowsMinSize = Vector2.one * 300.0f;
    static Rect s_HelpRect = new Rect(10.0f, 20.0f, 300.0f, 100.0f);
    static Rect s_ListRect = new Rect(Vector2.zero, s_WindowsMinSize);

    
    //SerializedObject m_LevelSerializedObject;
    //SerializedProperty levelDefs;
    //ReorderableList m_WavesList = null;
    //SerializedObject m_WavesSerializedObject;
    //SerializedProperty m_WavesProperty;
    int m_CurrentIndex= 0;
    Vector2 m_ScrollPos;

    //LevelDesign m_LevelSO = null;

    LevelDesignScriptableOBJConverter levelDesign;
    ReorderableList wavesList;

    [MenuItem("Window/LevelEditor/Open Window %#e")]
    static void Initialize()
    {
        LevelEditorWindow window = EditorWindow.GetWindow<LevelEditorWindow>(false, "Level Editor");
        window.minSize = s_WindowsMinSize;
    }

    
    void OnEnable()
    {
        levelDesign = new LevelDesignScriptableOBJConverter();

        Debug.Log(levelDesign.GetLevelCount());
        if (levelDesign.IsValid())
        {
            levelDesign.SetWavesList(m_CurrentIndex);
            
        }
       
    }
    

    void OnInspectorUpdate()
    {
        Repaint();
    }

    void OnGUI()
    {
        if (levelDesign.IsValid())
        {
            levelDesign.GetSerializedRoot().Update();
            DrawWaves();
            DrawLevelButtons();
            levelDesign.GetSerializedRoot().ApplyModifiedProperties();
        }
        else
        {
            EditorGUI.HelpBox(s_HelpRect, HELP_TEXT, MessageType.Warning);
        }
    }

    void DrawWaves()
    {
        int rowCount = levelDesign.GetWaveCount();
        rowCount = rowCount == -1 ? 1 : rowCount;
        int offsetX = 100;
        int offsetY = 10;
        int areaWidth = 500;
        float areaHeight = this.position.height;
        
        GUILayout.BeginArea(new Rect(offsetX, offsetY, areaWidth, areaHeight), "Levels");
        m_ScrollPos =EditorGUILayout.BeginScrollView(m_ScrollPos, true,true,GUILayout.Width(areaWidth), GUILayout.Height(this.position.height));
        //GUILayout.BeginArea(new Rect(0,0, areaWidth-50, areaHeight), "Levels");
        levelDesign.GetWavesListAtLevel(m_CurrentIndex).DoLayoutList();
        //GUILayout.EndArea();
        EditorGUILayout.EndScrollView();
        GUILayout.EndArea();
        
    }

    void OnDrawElement(Rect rect, int index, bool isActive, bool isFocused)
    {
        //var element = m_LevelsList.serializedProperty.GetArrayElementAtIndex(index);
        //rect.y += 2;
        
        

        //waveParamsArray.serializedObject.Update();
        //m_WavesList.DoLayoutList();
        //waveParamsArray.serializedObject.ApplyModifiedProperties();
       // EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, 250), spawnNextEnum,GUIContent.none);
    }

    void DrawLevelButtons()
    {
        int levelCount = levelDesign.GetLevelCount();
        GUILayout.BeginArea(new Rect(10, 10, 80, 20 *levelCount ),"Levels");
        GUILayout.BeginVertical();
        for (int i = 0; i < levelCount; i++)
        {
            int index = i;
            CreateButton(index);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    void CreateButton(int index)
    {
        if(GUILayout.Button("Level " + index))
        {
            ButtonPressed(index);
        }
    }

    void ButtonPressed(int index)
    {
        m_CurrentIndex = index;
        levelDesign.SetWavesList(index);
    }
}


