﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelParser : MonoBehaviour {

    private int levelNumber;
    private EnvironmentController _environmentController;
    private CameraMovement _cameraMovement;
    private Waves _wavesController;

    public LevelDesign _levelDesign;

    void Awake()
    {
        Initialize();
        SetEnvironmentValues();
        SetCameraSpeed();
        SetWaves();
    }

    private void Initialize()
    {
        LoadAssetData();
        _environmentController = GameObject.FindObjectOfType<EnvironmentController>().GetComponent<EnvironmentController>();
        _cameraMovement = Camera.main.GetComponent<CameraMovement>();
        _wavesController = GameObject.FindObjectOfType<Waves>().GetComponent<Waves>();
        levelNumber = GameStatistics.instance.levelButton;
    }

    private void SetEnvironmentValues()
    {
        _environmentController.SetBackground(_levelDesign.levels[levelNumber-1].background);
        _environmentController.SetForeground(_levelDesign.levels[levelNumber-1].foreground);

    }

    private void SetCameraSpeed()
    {
        _cameraMovement.CurrentMovementSpeed = _levelDesign.levels[levelNumber - 1].cameraSpeed;
    }

    private void SetWaves()
    {
        WaveParameters[] waveData = new WaveParameters[_levelDesign.levels[levelNumber - 1].waves.Count];
        for(int i=0; i<waveData.Length; i++)
        {
            waveData[i] = new WaveParameters(_levelDesign.levels[levelNumber - 1].waves[i]);
        }
        _wavesController.WaveParams = waveData;
    }

    private void LoadAssetData()
    {
        _levelDesign = Resources.Load(DataConstants.LEVEL_DESIGN_ASSET_PATH) as LevelDesign;
        if (_levelDesign == null)
            Debug.LogError("Level Design Scriptable Object Asset Not Found!");
    }

    public LevelDesign LevelDesign
    {
        get
        {
            return _levelDesign;
        }

        set
        {
            _levelDesign = value;
        }
    }


}
