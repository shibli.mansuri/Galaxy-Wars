﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class LevelDesign : ScriptableObject {
    [ListDrawerSettings(OnBeginListElementGUI = "OnBeginListElementGUI", NumberOfItemsPerPage = 1)]
    public List<LevelDefinition> levels;

    private void OnBeginListElementGUI(int index)
    {   
        GUILayout.Label("Level# " + (index+1).ToString());
        if (levels[index].waves != null)
        {
            TimeSpan approxTime = TimeSpan.FromSeconds(CalculateApproxTime(index));
            GUILayout.Label("Total Waves: " + (levels[index].waves.Count).ToString() +
                " || Enemies: " + (levels[index].EnemyCount()).ToString() +
                " || Approx Time: " + string.Format("{0}min & {1}s", approxTime.Minutes, approxTime.Seconds));
        }
    }

    private float CalculateApproxTime(int index)
    {
        float time = 0f;
        for(int i=0; i<levels[index].waves.Count; i++)
        {
            WaveParameters wave = levels[index].waves[i];
            //formula => start delay + (quantity-1 * interval ) + next wave interval + health proximity of enemies
            time += (wave.startWait + (wave.count-1 * wave.inbetweenWait) + wave.nextAfterTime + wave.count*0.05f); 
        }
        return time;
    }
}

[Serializable]
public struct LevelDefinition {
    [TabGroup("Level Settings")]
    public Sprite background;
    [TabGroup("Level Settings")]
    public Sprite foreground;
    [TabGroup("Level Settings")]
    public float cameraSpeed;
    [TabGroup("Waves In Level")]
    [ListDrawerSettings(OnBeginListElementGUI = "OnBeginListElementGUI", OnEndListElementGUI = "OnEndListElementGUI")]
    public List<WaveParameters> waves;
    [Button("Add Wave"), HorizontalGroup("Wave Buttons")]
    public void AddWave()
    {
        if(waves!=null)
            waves.Add(new WaveParameters());
    }

    public int EnemyCount()
    {
        int enemies = 0;
        for(int i=0; i<waves.Count; i++)
        {
            enemies += waves[i].count;
        }
        return enemies;
    }

    private void OnBeginListElementGUI(int index)
    {
        if(waves!=null && waves.Count>0)
            GUILayout.Label("Wave# " + (index + 1).ToString());
    }

    private void OnEndListElementGUI(int index)
    {
        if (index >= 0 && index < waves.Count - 1)
        {
            if (GUILayout.Button("Duplicate Wave"))
            {
                waves.Insert(index + 1, new WaveParameters(waves[index]));
            }
        }
    }

   
}