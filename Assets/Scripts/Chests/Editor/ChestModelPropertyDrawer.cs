﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomPropertyDrawer(typeof(ChestModel))]
public class ChestModelPropertyDrawer : PropertyDrawer {

    float defaultLabelWidth, defaultfieldWidth,inspectorWidth;
    bool _durationIsOnNewLine;
    public Dictionary<string, bool> _foldOutValuesDict = new Dictionary<string, bool>();

    SerializedProperty _chestName,
        _coinReward,
        _ODFragReward,
        _powerGemReward,
        _evolutionStoneReward,
        _unlockDurationMinutes,
        _unlockDeadlineStr, //Actually DateTime converted into string
        _currentState;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.DrawRect(new Rect(position.x,position.y+5f,position.width,position.height*0.9f), new Color(0.7f,0.7f,0.7f));
        int originalIndentLevel = EditorGUI.indentLevel;
        inspectorWidth = position.width;
        EditorGUI.indentLevel = 0;
        defaultLabelWidth = EditorGUIUtility.labelWidth;
        defaultfieldWidth = EditorGUIUtility.fieldWidth;

        position.height = EditorGUIUtility.singleLineHeight;

        _chestName = property.FindPropertyRelative("_chestName");
        _coinReward = property.FindPropertyRelative("_coinReward");
        _ODFragReward = property.FindPropertyRelative("_ODFragReward");
        _powerGemReward = property.FindPropertyRelative("_powerGemReward");
        _evolutionStoneReward = property.FindPropertyRelative("_evolutionStoneReward");
        _unlockDurationMinutes = property.FindPropertyRelative("_unlockDurationMinutes");
        _unlockDeadlineStr = property.FindPropertyRelative("_unlockDeadlineStr");
        _currentState = property.FindPropertyRelative("_currentState");

        DrawFieldsTopRow(ref position);

        if (_currentState.enumValueIndex==(int)ChestModel.ChestState.Locked)
        {
            DrawDuration(ref position);
        }

        DrawRewards(ref position,property);

        //draw some rect debug stuff
        //MoveDown(ref position);
        //EditorGUI.LabelField(position, position.ToString());
        EditorGUI.indentLevel = originalIndentLevel;
        MoveDown(ref position,20f);
        EditorGUI.DrawRect(new Rect(position.x + position.width * 0.25f, position.y, position.width * 0.75f, position.height / 8f), Color.black);
    }

    void DrawFieldsTopRow(ref Rect position)
    {
        MoveDown(ref position,10f);
        //Drawing Name 
        float nameRectWidth = GetNameRectWidth();
        Rect nameRect = new Rect(position.x, position.y, nameRectWidth, position.height);
        EditorGUIUtility.labelWidth = 50f;
        _chestName.stringValue = DrawText(nameRect, _chestName.stringValue, "Name");

        DrawLocked(ref position,nameRectWidth);
        SetDefaultWidths();
    }

    void DrawLocked(ref Rect position, float nameRectWidth)
    {
        //switches between locked and unlocked state, is not editable if the chest is under progress
        float unlockRectWidth = 65f;
        Rect unlockedRect = new Rect(position.x + nameRectWidth, position.y, unlockRectWidth, position.height);
        ChestModel.ChestState chestState = (ChestModel.ChestState)_currentState.enumValueIndex;
        string labelText= "unAssigned";
        
        bool islocked = true;
        bool isEditable = true;
        switch (chestState)
        {
            
            case ChestModel.ChestState.UnlockInProgress:
                labelText = "In Progress";
                isEditable = false;
                break;
            case ChestModel.ChestState.Locked:
                labelText = "Locked";
                break;
            case ChestModel.ChestState.Unlocked:
                labelText = "Locked";
                islocked = false;
                break;
        }

        Rect boolRect = EditorGUI.PrefixLabel(unlockedRect, new GUIContent(labelText));
        if (isEditable)
           islocked =  EditorGUI.Toggle(boolRect, islocked);
        _currentState.enumValueIndex = (int)(islocked ? ChestModel.ChestState.Locked : ChestModel.ChestState.Unlocked);
    }

    float GetTopRowWidth()
    {
        float nameWidth = GetNameRectWidth();
        float lockedWidth = 65f;
        float space = 10f;
        return nameWidth + lockedWidth+space;
    }

    float GetNameRectWidth()
    {
        float maxNameRectWidth = 0.7f*inspectorWidth;
        float nameRectWidth = GetResponsizeRect("Name", _chestName.stringValue);
        nameRectWidth = nameRectWidth > maxNameRectWidth ? maxNameRectWidth: nameRectWidth;//stay within half the inspectors width
        return nameRectWidth;
    }

    void DrawDuration(ref Rect position)
    {
        float topRowWidth = GetTopRowWidth();
        
        var time = TimeSpan.FromMinutes(_unlockDurationMinutes.doubleValue);
        string hrsLabel = time.Hours > 0 ? time.Hours + "hs:" : "";
        string minutesLabel = time.Minutes > 0 ? time.Minutes + "m:" : "";
        string secondsLabel = time.Seconds + "s ";
        string durationLabel = "For : " + hrsLabel + minutesLabel + secondsLabel;
        float durationLabelWidth = CalcSize(durationLabel).x;
        float contentSize = CalcSize(_unlockDurationMinutes.doubleValue.ToString()).x;
        EditorGUIUtility.labelWidth = durationLabelWidth;

        if (position.width < (topRowWidth + durationLabelWidth + contentSize+5f))
        {//if theres no space on top
            _durationIsOnNewLine = true;
            MoveDown(ref position);
            _unlockDurationMinutes.doubleValue = DrawDouble(position, _unlockDurationMinutes.doubleValue, durationLabel);
        }
        else
        {//draw inline
            _durationIsOnNewLine = false;
            Rect inlineRect = position;
            inlineRect.x += topRowWidth;
            inlineRect.width -= topRowWidth;
            _unlockDurationMinutes.doubleValue = DrawDouble(inlineRect, _unlockDurationMinutes.doubleValue, durationLabel);
        }
            if (_unlockDurationMinutes.doubleValue < 0)
            _unlockDurationMinutes.doubleValue = 0;
        SetDefaultWidths();
    }

    void DrawRewards(ref Rect position,SerializedProperty property)
    {
        MoveDown(ref position);
        bool _foldOutVal = GetUnfoldValue(property);

        EditorGUI.indentLevel++;
        string foldoutLbl;
        if (_foldOutVal == false)
        {//closed

            foldoutLbl = MakeFoldOutLbl();
           
               
        }
        else
        {//open
            foldoutLbl = "Edit Rewards";
        }
        _foldOutVal=  EditorGUI.Foldout(position, _foldOutVal, foldoutLbl, true);

        if (_foldOutVal)//draw the rewards
        {
            MoveDown(ref position);
            _coinReward.intValue = DrawInt(position, _coinReward.intValue, "Coins");
            _coinReward.intValue = _coinReward.intValue < 0 ? 0 : _coinReward.intValue;

            MoveDown(ref position);
            _powerGemReward.intValue = DrawInt(position, _powerGemReward.intValue, "Power Gems");
            _powerGemReward.intValue = _powerGemReward.intValue < 0 ? 0 : _powerGemReward.intValue;

            MoveDown(ref position);
            _ODFragReward.intValue = DrawInt(position, _ODFragReward.intValue, "OD Frags");
            _ODFragReward.intValue = _ODFragReward.intValue < 0 ? 0 : _ODFragReward.intValue;

            MoveDown(ref position);
            _evolutionStoneReward.intValue = DrawInt(position, _evolutionStoneReward.intValue, "Evo Stones");
            _evolutionStoneReward.intValue = _evolutionStoneReward.intValue < 0 ? 0 : _evolutionStoneReward.intValue;
        }
        _foldOutValuesDict[property.propertyPath] = _foldOutVal;
        EditorGUI.indentLevel--;
    }

    string MakeFoldOutLbl()
    {
        string coinsLbl = _coinReward.intValue > 0 ? "C:" + _coinReward.intValue : "";
        string ODLbl = _ODFragReward.intValue > 0 ? "OD:" + _ODFragReward.intValue : "";
        string PGLbl = _powerGemReward.intValue > 0 ? "PG:" + _powerGemReward.intValue : "";
        string EvoLbl = _evolutionStoneReward.intValue > 0 ? "EV:" + _evolutionStoneReward.intValue : "";

        if (string.IsNullOrEmpty(coinsLbl + ODLbl + PGLbl + EvoLbl))
            return "No Rewards";

        string toReturn="";
        if(!string.IsNullOrEmpty(coinsLbl))
            toReturn += coinsLbl + AddSeperatorIf(ODLbl, PGLbl, EvoLbl);
        if (!string.IsNullOrEmpty(PGLbl))
            toReturn += PGLbl + AddSeperatorIf(ODLbl,EvoLbl);
        if (!string.IsNullOrEmpty(ODLbl))
            toReturn += ODLbl + AddSeperatorIf(EvoLbl);
       
        toReturn += EvoLbl;
        return toReturn;
    }

    string AddSeperatorIf(params string [] checkString)
    {
        for(int i =0;i<checkString.Length;i++)
            if(!string.IsNullOrEmpty(checkString[i]))
                return "| ";

        return "";
    }

    void SetDefaultWidths()
    {
        EditorGUIUtility.fieldWidth = defaultfieldWidth;
        EditorGUIUtility.labelWidth = defaultLabelWidth;
    }

    float GetResponsizeRect(string title, string value, int minWidth=100)//rect that resizes according to content
    {
        Vector2 titleSize = CalcSize(title);
        Vector2 valueSize = CalcSize(value);
        float reservedSpace = 4 * 5;// some room for extra charactes and spaces so that the rect grows as the person is typing
        float characterWidth = titleSize.x + valueSize.x+reservedSpace;
        float rectWidth = characterWidth <= minWidth ? minWidth: characterWidth;
        return rectWidth;
    }
    
    bool GetUnfoldValue(SerializedProperty property)
    {
        bool unfolded = false;
        if (!_foldOutValuesDict.ContainsKey(property.propertyPath))
        {
            _foldOutValuesDict.Add(property.propertyPath, unfolded);
            return unfolded;
        }
        _foldOutValuesDict.TryGetValue(property.propertyPath, out unfolded);
        return unfolded;
    }

    Vector2 CalcSize(string text)
    {
        return GUI.skin.label.CalcSize(new GUIContent(text));
    }

    void DrawProperty(Rect position, SerializedProperty property,string LabelName)
    {
        EditorGUI.PropertyField(position, property, new GUIContent(LabelName));
    }

    string DrawText(Rect position, string text, string label=null)
    {
        position = new Rect(position);
        return label == null? EditorGUI.TextField(position,  text): EditorGUI.TextField(position, label,text);
    }

    int DrawInt(Rect position, int val,string label=null)
    {
        return EditorGUI.IntField(position,label==null?"property":label, val);
    }

    double DrawDouble(Rect position, double val, string label = null)
    {
        position = new Rect(position);
        return EditorGUI.DoubleField(position, label == null ? "property" : label, val);
    }

    void MoveRight(ref Rect position, float dist = -1)
    {
        position.x += dist > 0 ? dist : position.width;
    }

    void MoveDown(ref Rect position, float dist = -1)
    {
        position.y +=dist>0?dist: position.height+ EditorGUIUtility.standardVerticalSpacing;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float rows;
        if (GetUnfoldValue(property))//if foldout is open
            rows = 6.4f;
        else
            rows = 2f;
        if (_durationIsOnNewLine)
            rows += 1f;
        
        return base.GetPropertyHeight(property, label) + (rows * (16+EditorGUIUtility.standardVerticalSpacing));
    }
}