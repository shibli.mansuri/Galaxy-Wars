﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestViewUIManager : MonoBehaviour {

	public GameObject _chestCellPrefab;
	public GameObject _claimPanelPrefab;
	public List<ChestCellUI> _chestCellsUI;
	public ChestUIManager _manager;

	public List<GameObject> chestPanels;

	public void Awake()
	{
		Initialize ();
	}

	public void CreatePanel(string panelNumber, string chestName,string rewardList, int index)
	{
		GameObject newChestCell = Instantiate (_chestCellPrefab, GameObject.Find ("Content").transform);
		newChestCell.name = panelNumber;
		newChestCell.SetActive (true);

		ChestCellUI chestCellInfo = newChestCell.GetComponent<ChestCellUI> ();
		chestCellInfo.ChestName.text = chestName;
		chestCellInfo.RewardList.text = rewardList;

        //spawn chest panel to scroll view and add reference to chest panel list
        //ChestCellUI newCell = Instantiate(_chestCellPrefab).GetComponent<ChestCellUI>();

		chestPanels.Add(newChestCell);

		//add chest cell ui reference to list
		_chestCellsUI.Add (chestCellInfo);
		DelegateActivateFunctionality (index);
	}

	public void TimeOver(int index)
	{
		ChestCellUI chestCellInfo = _chestCellsUI[index];
		//Show time remaining 0:0:0 in chest panel's time text UI
		chestCellInfo.TimeText.text = "0:0:0";

		//enable claim chest button
		chestCellInfo.Button.interactable = true; 

		chestCellInfo.ButtonText.text = "Claim";

		DelegateClaimFunctionalityToButton (index);

	}

	public void UpdateTime(TimeSpan timeRemaining, int index)
	{
		ChestCellUI chestCellInfo = _chestCellsUI [index];
		chestCellInfo.TimeText.text = Mathf.Abs (timeRemaining.Hours) + ":" + Mathf.Abs (timeRemaining.Minutes) +
			":" + Mathf.Abs (timeRemaining.Seconds);

		//disable claim chest button
		chestCellInfo.Button.interactable = false; 

		chestCellInfo.ButtonText.text = "Claim";
	}

	private void DelegateActivateFunctionality(int index)
	{
		ChestCellUI chestCellInfo = _chestCellsUI [index];
		chestCellInfo.TimeText.text = "";
		chestCellInfo.ButtonText.text = "Activate";
		chestCellInfo.Button.interactable = true;

		chestCellInfo.Button.onClick.RemoveAllListeners ();
		chestCellInfo.Button.onClick.AddListener (delegate {
			_manager.ActivateChest (index);
		});
			
	}

	private void DelegateClaimFunctionalityToButton(int index)
	{
		ChestCellUI chestCellInfo = _chestCellsUI [index];
		chestCellInfo.TimeText.text = "0:0:0";
		//chestCellInfo.ButtonText.text = "Claim";
		chestCellInfo.Button.interactable = true;
		chestCellInfo.Button.onClick.RemoveAllListeners ();
		chestCellInfo.Button.onClick.AddListener (delegate {
			ShowClaimPanel(index);
		});
	}

	private void ShowClaimPanel(int index)
	{
		ChestCellUI chestCellInfo = _chestCellsUI [index];
		ClaimPanelUI claimPanelInfo = _claimPanelPrefab.GetComponent<ClaimPanelUI> ();

		claimPanelInfo.Heading.text = chestCellInfo.ChestName.text;
		claimPanelInfo.RewardList.text = chestCellInfo.RewardList.text;

		GameObject claimPanel = Instantiate (_claimPanelPrefab,  GameObject.Find ("Canvas").transform);
		Button button = claimPanel.GetComponent<ClaimPanelUI> ().Button;

		button.onClick.RemoveAllListeners ();
		button.onClick.AddListener (delegate {
			Destroy(chestPanels[index]);
			chestPanels.RemoveAt(index);
			_chestCellsUI.RemoveAt(index);
			Destroy(claimPanel);
			RefreshButtonDelegates();
			_manager.ClaimRewards(index);	

		});

	}

	private void RefreshButtonDelegates()
	{
		for (int i = 0; i < _chestCellsUI.Count; i++) {
			int index = i;
			DelegateActivateFunctionality (index);
		}
	}

	private void Initialize()
	{
		_chestCellPrefab = Resources.Load (DataConstants.CHEST_CELL_UI_PATH) as GameObject;
		_claimPanelPrefab = Resources.Load (DataConstants.CLAIM_PANEL_UI_PATH) as GameObject;
	}
		
	public GameObject ClaimPanelPrefab {
		get {
			return _claimPanelPrefab;
		}
		set {
			_claimPanelPrefab = value;
		}
	}
}
