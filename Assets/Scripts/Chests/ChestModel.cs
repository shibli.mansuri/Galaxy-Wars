﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ChestModel {

	public string _chestName;
	public int _coinReward;
	public int _ODFragReward;
	public int _powerGemReward;
	public int _evolutionStoneReward;
	public double _unlockDurationMinutes;
	public string _unlockDeadlineStr;       //Actually DateTime converted into string
    public ChestState _currentState;        //public int _chestState;
	public int _notificationID;

    public enum ChestState { Locked, UnlockInProgress, Unlocked , Opened };

	public ChestModel(string name, int coins, int odFrags, int powerGems, int evolutionStones, double unlockDurationMinutes, bool unlockNow)
	{
		_chestName = name;
		_coinReward = coins;
		_ODFragReward = odFrags;
		_powerGemReward = powerGems;
		_evolutionStoneReward = evolutionStones;
		_unlockDurationMinutes = unlockDurationMinutes;
        _currentState = ChestState.Locked;
        Unlock(unlockNow);
	}

    public ChestModel(ChestModelToJson jsonModel)
    {
        _chestName = jsonModel._chestName;
        _coinReward = jsonModel._coinReward;
        _ODFragReward = jsonModel._ODFragReward;
        _powerGemReward = jsonModel._powerGemReward;
        _evolutionStoneReward = jsonModel._evolutionStoneReward;
        _unlockDurationMinutes = jsonModel._unlockDurationMinutes;
        _unlockDeadlineStr = jsonModel._unlockDeadlineStr;
        _currentState = (ChestState)jsonModel._chestState;
		_notificationID = jsonModel._notificationID;
       
    }

    public void SetChestName(string name)
	{
		_chestName = name;
	}

	public string GetChestName()
	{
		return _chestName;
	}

	public void SetCoinReward(int coins)
	{
		_coinReward = coins;
	}

	public int GetCoinReward()
	{
		return _coinReward;
	}

	public void SetODFragReward(int odFrags)
	{
		_ODFragReward = odFrags;
	}

	public int GetODFragReward()
	{
		return _ODFragReward;
	}

	public void SetPowerGemReward(int powerGems)
	{
		_powerGemReward = powerGems;
	}

	public int GetPowerGemReward()
	{
		return _powerGemReward;
	}

	public void SetEvolutionStoneReward(int evolutionStones)
	{
		_evolutionStoneReward = evolutionStones;
	}

	public int GetEvolutionStoneReward()
	{
		return _evolutionStoneReward;
	}

	public void SetUnlockDeadline()
	{
		_unlockDeadlineStr = (DateTime.UtcNow.AddMinutes(_unlockDurationMinutes)).ToString();
	}

	public string GetUnlockTime()
	{
		return _unlockDeadlineStr;
	}

	public double GetUnlockDuration()
	{
		return _unlockDurationMinutes;
	}

	public int GetNotificationID()
	{
		return _notificationID;
	}

	public void Unlock(bool unlockNow)
	{

		if (unlockNow && _currentState == ChestState.Locked)//if locked we should unlock it
        {
			SetUnlockDeadline();
			if (_unlockDurationMinutes == 0 || _unlockDurationMinutes < 0)
				_currentState = ChestState.Opened;
			else if (_unlockDurationMinutes > 0) {
				_currentState = ChestState.UnlockInProgress;
				_notificationID = NotificationsManager.ScheduleNotificationForChest (this);
			}
        }
	}

	public void Open(bool unlockNow)
	{
		if (unlockNow && _currentState == ChestState.UnlockInProgress) {
			_currentState = ChestState.Opened;
		}
	}

	public bool IsLocked()
	{
		if (_currentState == ChestState.Locked)
			return true;
		else
			return false;
	}

	public bool IsInProgress()
	{
		if (_currentState == ChestState.UnlockInProgress)
			return true;
		else
			return false;
	}

	public bool IsUnlocked()
	{
		if (_currentState == ChestState.Opened)
			return true;
		else
			return false;
	}

	public string GetChestAsString()
	{
		return JsonUtility.ToJson (new ChestModelToJson(this));
	}

}

[Serializable]
public struct ChestModelToJson{
    public string _chestName;
    public int _coinReward;
    public int _ODFragReward;
    public int _powerGemReward;
    public int _evolutionStoneReward;
    public double _unlockDurationMinutes;
    public string _unlockDeadlineStr;
    public int _chestState;
	public int _notificationID;

    public ChestModelToJson(ChestModel model)
    {
        _chestName = model._chestName;
        _coinReward = model._coinReward;
        _ODFragReward = model._ODFragReward;
        _powerGemReward = model._powerGemReward;
        _evolutionStoneReward = model._evolutionStoneReward;
        _unlockDurationMinutes = model._unlockDurationMinutes;
        _unlockDeadlineStr = model._unlockDeadlineStr;
        _chestState = (int)model._currentState;
		_notificationID = model._notificationID;
    }
}
