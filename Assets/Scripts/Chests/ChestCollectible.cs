﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestCollectible : BaseCollectible {

	public ChestModel chest;
	private ModalPanel modalPanel;

	void Awake()
	{
		modalPanel = ModalPanel.instance;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag(DataConstants.STR_PLAYER_TAG))
		{
			EventHub.TriggerEvent ("NumberOfCollectibles");
			OnCollidedWithPlayer(collision.gameObject);
		}

	}

	void OnCollidedWithPlayer(GameObject player)
	{
		modalPanel.Choice ("You got a chest!", AddToInventory, DiscardChest);
		DestroySelf ();
	}

	private void AddToInventory()
	{
		PlayerInventory.instance.AddChest (chest);
	}

	private void DiscardChest()
	{
		Debug.Log ("Chest discarded");
	}


}
