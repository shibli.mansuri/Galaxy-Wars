﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestCellUI : MonoBehaviour {

	public Image _image;
	public Text _chestName;
	public Text _rewardList;
	public Text _timeText;
	public Text _buttonText;
	public Button _button;
    private Text duration;

    public Button Button {
		get {
			return _button;
		}
		set {
			_button = value;
		}
	}

	public Text ButtonText {
		get {
			return _buttonText;
		}
		set {
			_buttonText = value;
		}
	}

	public Text TimeText {
		get {
			return _timeText;
		}
		set {
			_timeText = value;
		}
	}

	public Text RewardList {
		get {
			return _rewardList;
		}
		set {
			_rewardList = value;
		}
	}

	public Image Image {
		get {
			return _image;
		}
		set {
			_image = value;
		}
	}

	public Text ChestName {
		get {
			return _chestName;
		}
		set {
			_chestName = value;
		}
	}

    public Text Duration
    {
        get
        {
            return duration;
        }

        set
        {
            duration = value;
        }
    }
}
