﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClaimPanelUI : MonoBehaviour {

	public Text _heading;
	public Text _rewardList;
	public Button _button;

	public Button Button {
		get {
			return _button;
		}
		set {
			_button = value;
		}
	}

	public Text Heading {
		get {
			return _heading;
		}
		set {
			_heading = value;
		}
	}

	public Text RewardList {
		get {
			return _rewardList;
		}
		set {
			_rewardList = value;
		}
	}
}
