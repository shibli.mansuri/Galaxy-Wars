﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestUIManager : MonoBehaviour {

	public ChestViewUIManager _view;
    public ChestManager chestManager;

    void Awake()
    {
        
		PlayerPrefsManager.DeleteAll ();
		ChestManager manager = new ChestManager ();
	
		manager.AddChest(new ChestModel ("Epic Chest", 5, 2, 3, 0,0.3f, false));
        manager.AddChest(new ChestModel("Legendary Chest", 5, 2, 3, 0, 0.2f, false));
        manager.AddChest(new ChestModel("Common Chest", 5, 2, 3, 0, 0f, false));
        manager.SaveChestList ();

		Initialize();

    }

    private void Initialize()
    {

        //initialize list holding all chest panels
        _view.chestPanels = new List<GameObject>();
      
        //get chest manager reference
		chestManager = new ChestManager();
    }



    void Start()
    {
        //check if chests exist
        if (chestManager._chests.Count > 0) {
           
			//iterate over all chests
            for (int i = 0; i < chestManager._chests.Count; i++) {
               
				ChestModel currModel = chestManager._chests[i];

				//intialize string for rewards UI text
                string rewardsText = "";
				string panelNumber = ""; 
				string chestName ="";

				//get chest name from chest manager
				chestName = currModel.GetChestName();

                rewardsText += MakePluralIf(currModel.GetCoinReward(), "Coin");
                rewardsText += MakePluralIf(currModel.GetODFragReward(), "OD Frag");
                rewardsText += MakePluralIf(currModel.GetPowerGemReward(), "Power Gem");
                rewardsText += MakePluralIf(currModel.GetEvolutionStoneReward(), "Evolution Stone");
       

                //name chest panel according to index
				panelNumber = "Chest#" + (i + 1);
				_view.CreatePanel (panelNumber, chestName ,rewardsText, i);
                
            }
        }

    }


	void Update()
	{	
		//if chests exists
		if (chestManager._chests.Count > 0) {
			//iterate through all chests
			for (int i = 0; i < chestManager._chests.Count ; i++) {
				int index = i;
				TimeSpan timeRemaining;
		
				//check if chest is activated
				if (chestManager._chests [i].IsInProgress ()) {
					//calculate time remaining to be able to claim chest
					timeRemaining = DateTime.Parse(chestManager._chests [index].GetUnlockTime ()) - DateTime.UtcNow;
					if (IsTimeRemaining (timeRemaining)) {
						//Show time remaining in chest panel's time text UI
						_view.UpdateTime (timeRemaining, index);
					}
                    else
                    {
						chestManager._chests[i].Open(true);
                    }
				}
				else if(chestManager._chests [i].IsUnlocked ())	//check if wait time is completed
				{
					_view.TimeOver (index);
				}
				else if(chestManager._chests [i].IsLocked ()) { // check if chest is not activated yet

					//Do nothing
				}
			}
		}
	}
		

	public void ClaimRewards(int index)
	{
		//Claim chest and Remove chest from storage
		chestManager.OpenChest(index); 

	}

	public void ActivateChest(int index)
	{
		//activate chest
		chestManager._chests [index].Unlock (true);
		//save chest
		chestManager.SaveChestList();
	}
		
	string MakePluralIf(int quantity, string origString)
	{
		string toReturn = quantity > 1 ? quantity + origString + "s" : quantity + origString;
		toReturn += "\n";
		return toReturn;
	}

	private bool IsTimeRemaining(TimeSpan timeRemaining)
	{
		if ((timeRemaining.Hours == 0 && timeRemaining.Minutes == 0 && timeRemaining.Seconds == 0) || (timeRemaining.Hours < 0 || timeRemaining.Minutes < 0 || timeRemaining.Seconds < 0)) {
			return false;
		} else
			return true;
	}

}
 