﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChestManager  {

	public List<ChestModel> _chests;

	public ChestManager()
    {
        _chests = new List<ChestModel>();
        LoadChestsFromPlayerPrefs();
    }

    void LoadChestsFromPlayerPrefs()
    {
        string jsonString = PlayerPrefsManager.GetChestList();
        if (jsonString != "{}" && jsonString != "") // if json was received with data
        {
            char[] separator = { '*' };
            string[] chestsStrings = jsonString.Split(separator);
            for (int i = 0; i < chestsStrings.Length; i++)
            {
                ChestModelToJson jsonModel = JsonUtility.FromJson<ChestModelToJson>(chestsStrings[i]);
                _chests.Add(new ChestModel(jsonModel));
            }
        }
    }

    public void SaveChestList()
    {
        string jsonString = "";
        for (int i = 0; i < _chests.Count; i++)
        {
            if (i == _chests.Count - 1)
                jsonString += _chests[i].GetChestAsString();
            else
                jsonString += _chests[i].GetChestAsString() + "*";
        }
        PlayerPrefsManager.SetChestList(jsonString);
    }

    public void OpenChest(int index)
	{
        ChestModel chestToOpen = _chests[index]; ;
		PlayerInventory.instance.AddCoins (chestToOpen.GetCoinReward());
		PlayerInventory.instance.ODFrags = chestToOpen.GetODFragReward ();
		PlayerInventory.instance.PowerGems = chestToOpen.GetPowerGemReward ();
		PlayerInventory.instance.EvoStone = chestToOpen.GetEvolutionStoneReward ();
		RemoveChest(index);
        SaveChestList();
    }

    

    public void AddChest(ChestModel chest)
    {
        _chests.Add(chest);
    }

    public void RemoveChest(int index)
    {
        _chests.RemoveAt(index);
    }

    

    public int GetCount()
    {
        return _chests.Count;
    }
}
