﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct RewardItems
{
    public int _megaBomb;
    public int _shields;
    public int _coins;
    public int _odFrags;
    public int _powerGems;
    public int _evoStones;

    public RewardItems(int coins = 0, int odFrags = 0, int powerGems = 0,
        int evoStones = 0, int megaBomb = 0, int shields = 0)
    {
        _megaBomb = megaBomb;
        _shields = shields;
        _coins = coins;
        _odFrags = odFrags;
        _powerGems = powerGems;
        _evoStones = evoStones;
    }

    public override string ToString()
    {
        string coinsStr = ConvertToString(Coins, "Coins");
        string shieldStr = ConvertToString(Shields, "Shields");
        string megabombStr = ConvertToString(MegaBomb, "MegaBombs");
        string odFragsStr = ConvertToString(OdFrags, "ODs");
        string powerGemStr = ConvertToString(PowerGems, "PowerGems");
        string evoStr = ConvertToString(EvoStones, "Evos");

        return AddSeperatorBetween(coinsStr, shieldStr, megabombStr, odFragsStr, powerGemStr, evoStr);
    }

    string ConvertToString(int quantity, string headerLabel)
    {
        return quantity == 0 ? null : quantity + " " + headerLabel;
    }

    string AddSeperatorBetween(params string[] stringParams)
    {
        string toReturn="";
        for(int i = stringParams.Length - 1; i >= 0; i--)//go in reverse 
        {
            if (null != stringParams[i])
                toReturn = stringParams[i] + toReturn;
            if (i != 0)
            {
                if (null != stringParams[i - 1])
                    toReturn = ", " + toReturn;
            }
        }
        return toReturn; 
    }

    public bool isEmpty()
    {
        if (_megaBomb == _odFrags && _odFrags == _powerGems && _powerGems == _coins && _coins == _shields && _shields == _evoStones && _evoStones == 0)
            return true;
        else
            return false;
    }

    public int MegaBomb
    {
        get
        {
            return _megaBomb;
        }
    }

    public int Shields
    {
        get
        {
            return _shields;
        }
    }

    public int Coins
    {
        get
        {
            return _coins;
        }
    }

    public int OdFrags
    {
        get
        {
            return _odFrags;
        }
    }

    public int PowerGems
    {
        get
        {
            return _powerGems;
        }
    }

    public int EvoStones
    {
        get
        {
            return _evoStones;
        }
    }
}