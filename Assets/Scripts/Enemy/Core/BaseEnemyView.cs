﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemyView : MonoBehaviour {
    public SpriteRenderer _View;
	public SpriteRenderer _DeadView;
    bool _IsHurt;
    IEnumerator _HurtCoroutine;

    Color _OriginalColor;
    public Color _HurtColor;
    float _AnimationDuration;


	void Awake()
	{
		if(_DeadView)_DeadView.gameObject.SetActive (false);

	}
	/*Transform Gun;
	SpriteRenderer GunMuzzleFlash;
	public Animator GunAnimator;
	public enemy_animation GunAnimation;
	/*
	void Awake()
	{
		GunAnimation = new enemy_animation (GunAnimator);
	}

	public void PlayIdle()
	{
		GunAnimation.PlayIdle ();
		if (GunAnimator.GetCurrentAnimatorStateInfo (0).IsName ("recoil")) {
			GunMuzzleFlash.enabled = false;
		}
	}

	public void PlayRecoil()
	{
		GunMuzzleFlash.enabled = true;
		GunAnimation.PlayRecoil ();
	}

	public void PlayMuzzleflash()
	{
		GunAnimation.PlayMuzzle ();
	}*/

	private void Start()
    {
        _OriginalColor = _View.color;
    }


    public void PlayHurtAnimation()
    {
        _IsHurt = true;
        if (_HurtCoroutine == null)
        {
            _HurtCoroutine = HurtAnimation();
            StartCoroutine(_HurtCoroutine);
        }
        else//extend the animation duration if it is already playing
        {
            _AnimationDuration += _AnimationDuration<0.2f?0.2f:0;
        }

    }

    IEnumerator HurtAnimation()
    {
        _AnimationDuration = 0.2f;
        float _TimeElapsedHurtAnimation = 0;
        while (_TimeElapsedHurtAnimation< _AnimationDuration)// make the color red
        {
            _View.color = Color.Lerp(_OriginalColor, _HurtColor, Mathf.PingPong(Time.time*5, 1));
            _AnimationDuration -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _IsHurt = false;
        _HurtCoroutine = null;
        float _EndAnimationDuration = 0.2f;
        float _TimeElapsed = 0f;
        Color _CurrentColor = _View.color;
        while (!_IsHurt)// make the color normal
        {
            _View.color = Color.Lerp( _CurrentColor, _OriginalColor, _TimeElapsed / _EndAnimationDuration);
            _TimeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
