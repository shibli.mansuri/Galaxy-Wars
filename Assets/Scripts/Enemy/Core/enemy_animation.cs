﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_animation{

	//tags 
	//AssaultBoat-1-Gun-tag
	//AssaultBoat-2-Gun-tag
	//ScoutBoat-1-Gun-tag
	//ScoutBoat-2-gun-tag

	public Animator enemy_animator;
	public enemy_animation(Animator anim)
	{
		enemy_animator = anim;
	}
	public enemy_animation()
	{
		
	}
	//Find the animator instead of using constructor


	public void PlayRecoil()
	{
		enemy_animator.Play ("recoil");
	}

	public void PlayIdle()
	{
		enemy_animator.Play ("idle");
	}

	public void PlayMuzzle()
	{
		
		enemy_animator.Play ("heavy_muzzl",-1,0f);
	}


}
