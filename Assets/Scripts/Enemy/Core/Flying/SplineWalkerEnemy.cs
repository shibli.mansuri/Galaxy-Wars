﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineWalkerEnemy : BaseEnemy
{

    [SerializeField]
    float _MovementDuration = 3f;
    [SerializeField]
    bool _LookForward = true;
    
    SplineWalker splineWalker;
    

	void start()
	{

		if (GetComponent<UbhShotCtrl> ())
			GetComponent<UbhShotCtrl> ().shotSound = shotSound;
	}
    public void InitSplineWalker(float overrideDuration, BezierSpline path)//called from Waves.cs when this enemy is spawned
    {
        splineWalker = GetComponent<SplineWalker>(); //Get Or Add splinewalker component
        if (splineWalker == null)
        {
            splineWalker = this.gameObject.AddComponent<SplineWalker>();
        }

        if (overrideDuration == -1)//keep default duration
            splineWalker.SetDuration(_MovementDuration);
        else
            splineWalker.SetDuration(overrideDuration) ;

       

        splineWalker.lookForward = _LookForward;
        splineWalker.SetPath(path);
        transform.position = path.GetPoint(0);
    }

    public override void OnStart()
    {
		
    }

    public override void OnDeath()
    {
		
    }

    public override void OnGetHurt()
    {
        if (_View == null)
        {
            Debug.LogError("No View Associated with SplineWalkerEnemy");
        }
        else
        {
            _View.PlayHurtAnimation();
        }
    }

    public float MovementDuration
    {
        get
        {
            return _MovementDuration;
        }
        set
        {
            _MovementDuration = value;
        }
    }
}
