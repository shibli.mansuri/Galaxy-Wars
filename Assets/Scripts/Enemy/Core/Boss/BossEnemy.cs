using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
[RequireComponent(typeof(SplineWalker))]
public class BossEnemy : BaseEnemy
{
	
[SerializeField]
	BezierSpline _EntrancePath;
	[SerializeField]
	BossBehavior[] _Behaviors;
	SplineWalker _BossMover;
	BossState _CurrentState;
	BossBehavior _CurrentBehavior;
	BossBehaviorManager _BehaviorManager;
	int _CurrentPathIndex;
	
	public bool isLevelBoss;
	public int bossNumber;
	public bool show=false;
	IEnumerator a;

	
public override void OnStart()
//sets boss to its entrance path animation
	
	{
		_BossMover = GetComponent<SplineWalker>();

		_BehaviorManager = new BossBehaviorManager(_Behaviors);


		
		InitializeEntrance();
		AssignSoundToEnemyGun();
	}

	void AssignSoundToEnemyGun()

	{
		if (shotSound)
		for (int i = 0; i < _Behaviors.Length; i++) 
	{
			
	_Behaviors [i].GunsManager.shotSound = shotSound;

	}
	
	}


	private void InitializeEntrance()
	
	{
		SwitchState(BossState.Entrance);

		transform.position = _EntrancePath.GetPoint(0);

		SetPath(_EntrancePath);

		DisableAllBehaviors();

	}

	void DisableAllBehaviors()// just disables the guns
	
	{
		foreach (BossBehavior behavior in _Behaviors)
		DisableBehavior(behavior);
	}

	void SwitchState(BossState newState)
	{
		if (_CurrentState != newState)
		{
			Debug.Log("Boss Enemy: Switching from : " + _CurrentState + " to : " + newState);
			_CurrentState = newState;
		}
	}

	private void Update()
	{
		ProcessState();
	}

	void ProcessState()
	{
		switch (_CurrentState)
		{
		case BossState.Entrance:
			if (_BossMover.IsMovementComplete)
				SwitchState(BossState.Fighting);
			break;
		case BossState.Fighting:
			if (SwitchConditionsSatisfied())
				SwitchBehavior();
			break;
		case BossState.Dying:
			break;
		}

	}

	bool SwitchConditionsSatisfied()
	{
		bool behaviorAssigned = _CurrentBehavior != null;
		if (!behaviorAssigned)//after the entrance phase no behavior is assigned
			return true;
		    return !IsHealthAboveTrigger()|| IsMovementFinished();
	}

	void SwitchBehavior()
	{
		if (_CurrentBehavior==null || !IsHealthAboveTrigger())//either no behavior has been assigned or the health is too low for this behavior to continue
		{
			BossBehavior nextBehavior = _BehaviorManager.GetNextBehavior(_CurrentHealth / _MaxHealth);//will be null if the last behavior cutoff is greater than 0

			if (nextBehavior!=null) 
			{
				if (_CurrentBehavior != null)//Disable the last behavior
					DisableBehavior(_CurrentBehavior);
				_CurrentBehavior = nextBehavior;
				EnableBehavior(_CurrentBehavior);
				SetNewPath(_CurrentBehavior, 0);
				Debug.Log("Setting new Behavior");
			}
			else//should not end up here
			{

			}

		}
		else if (IsMovementFinished())
		{
			SetNewPath(_CurrentBehavior, ++_CurrentPathIndex);
		}
	}
	void DisableBehavior(BossBehavior behavior)
	{
        behavior.GunsManager.StopShotRoutine();
        behavior.GunsManager.gameObject.SetActive(false);
		
	}

	void EnableBehavior(BossBehavior behavior)
	{
        behavior.GunsManager.gameObject.SetActive(true);
        behavior.GunsManager.StartShotRoutine();

    }

	void SetNewPath(BossBehavior behavior, int index)
	{
		_CurrentPathIndex = index % behavior.paths.Length;

		_BossMover.SetPath(behavior.paths[_CurrentPathIndex]);
		_BossMover.SetDuration(behavior.movementDuration);
		_BossMover.mode = behavior.mode;
		_BossMover.lookForward = behavior.lookForward;
	}

	bool IsHealthAboveTrigger()
	{
		return _CurrentHealth/_MaxHealth >= _CurrentBehavior.HealthCutoff;
	}

	bool IsMovementFinished()
	{
		return  _BossMover.IsMovementComplete || //mode was set to once and we've reached the end of the spline path
			(_CurrentBehavior.loopThreshold > 0 && (_BossMover.Loops() >= _CurrentBehavior.loopThreshold));//mode was set to loops, loop threshold was enabled and loops have exceeded threshold

	}

	void SetPath(BezierSpline path)
	{
		_BossMover.SetPath(path);
	}

	void RemovePath(BezierSpline path)
	{

	}

	public override void OnDeath()
	{
		
		Camera.main.DOShakePosition(1f, 2);
		if (isLevelBoss)
		{
			EventHub.TriggerEvent ("BossDeathGameover");
			EventParser.instance.SetStat (GameStatistics.Stat.BossesDefeated, bossNumber);
		}
	}

	public override void OnGetHurt()
	{
		if (_View == null)
		{
			Debug.LogError("No View Associated with Boss");
		}
		else
		{
			_View.PlayHurtAnimation();
		}
	}
}

[Serializable]
public class BossBehavior
{
	[Tooltip("Health value below which this is triggered")]
	[Range(0,1)]
	public float HealthCutoff;
	[Tooltip("Guns Equipped")]
	public UbhShotCtrl GunsManager;
	[Tooltip("If other behaviors lie within the health threshold then switch after this many loops")]
	public int loopThreshold=-1;
	[Tooltip("Movement Duration")]
	public float movementDuration;
	[Tooltip("Movement Type")]
	public SplineWalkerMode mode;
	[Tooltip("Paths to follow")]
	public BezierSpline[] paths;
	public bool lookForward = false;

}

public enum BossState
{
	None,
	Entrance,
	Fighting,
	Dying,
}

public class BossBehaviorManager
{

	Dictionary<float, List<BossBehavior>> _BehaviorsDictionary;
	int _CurrentBehaviorIndex=-1;
	Dictionary<float, List<BossBehavior>>.KeyCollection _Keys;
	float _CurrentKey = -1;

	public BossBehaviorManager(BossBehavior[] Behaviors)
	{
		Initialize(Behaviors);
	}

	public void Initialize(BossBehavior[] Behaviors)//creates a dictionary where the health is the key and the behaviors associated with that health threshold are the values
	{                                               // so you can have 3 behaviors at health >=70
		_BehaviorsDictionary = new Dictionary<float, List<BossBehavior>>();
		foreach (BossBehavior b in Behaviors)
		{
			if (_BehaviorsDictionary.ContainsKey(b.HealthCutoff))
			{
				List<BossBehavior> behaviorList;
				_BehaviorsDictionary.TryGetValue(b.HealthCutoff, out behaviorList);
				if (behaviorList == null)
					Debug.LogError("BossEnemy: No behavior List stored for key: " + b.HealthCutoff);
				behaviorList.Add(b);
			}
			else
			{
				List<BossBehavior> behaviorList = new List<BossBehavior>();
				behaviorList.Add(b);
				_BehaviorsDictionary.Add(b.HealthCutoff, behaviorList);
			}
		}
	}

	public BossBehavior GetNextBehavior(float currentHealth)
	{
		List<BossBehavior> behaviorsList;
		float newKey = GetKeyAboveHealth(currentHealth);
		_BehaviorsDictionary.TryGetValue(newKey, out behaviorsList);
		if (behaviorsList == null)
		{
			Debug.LogError("Boss Behavior Manager: No behaviors associated with : " + currentHealth);//set atleast one behavior to HealthCutoff 0
			return null;
		}


		if (newKey != _CurrentKey)
		{//either this is the first time or the boss lost health
			_CurrentKey = newKey;
			_CurrentBehaviorIndex = 0;
		}

		if (behaviorsList.Count > 1)//if more than one behaviors in this threshold
		{
			_CurrentBehaviorIndex = _CurrentBehaviorIndex + 1 % behaviorsList.Count;
		}
		else
		{
			_CurrentBehaviorIndex = 0;
		}

		return behaviorsList[_CurrentBehaviorIndex];
	}

	float GetKeyAboveHealth(float currentHealth)//returns the highest health greater than or equal to currentHealth
	{
		if (currentHealth > 1 || currentHealth < 0)
		{
			Debug.LogError("BossBehaviorManager: currentHealth must be a percentage of maxHealth");
			return -1;
		}

		var toReturn = from healthThresholds in _BehaviorsDictionary.Keys
				where currentHealth >= healthThresholds
			orderby healthThresholds descending
			select healthThresholds;
		return toReturn.FirstOrDefault();
	}
}
