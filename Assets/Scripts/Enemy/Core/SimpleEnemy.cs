﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : BaseEnemy
{

//	void start()
//	{
//
//		if (GetComponent<UbhShotCtrl> ())
//			GetComponent<UbhShotCtrl> ().shotSound = shotSound;
//	}

    public override void OnDeath()
    {
        //throw new NotImplementedException();
    }

    public override void OnGetHurt()
    {
        //throw new NotImplementedException();
		if (_View == null)
		{
			Debug.LogError("No View Associated with SplineWalkerEnemy");
		}
		else
		{
			_View.PlayHurtAnimation();
		}
    }

    public override void OnStart()
    {
        
    }
}
