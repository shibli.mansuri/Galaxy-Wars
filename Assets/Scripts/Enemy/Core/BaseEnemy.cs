﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseEnemy : UbhMonoBehaviour,IHealth
{
	public AudioClip[] explosions;
	public AudioClip hurtSound;
	public AudioClip shotSound;
    protected float _CurrentHealth;
    [SerializeField]
    protected float _MaxHealth;
    string _PlayerBulletTag = DataConstants.STR_PLAYER_BULLET_TAG;
    bool _Alive = false;
    [SerializeField]
    [Range(0, 1f)]
    float _DamageToPlayerBullet = 1;
    [SerializeField]
    [Range(0, 1f)]
    float _DamageToPlayer = 0.3f;
    [SerializeField]
    [Range(0, 1f)]
    float _DamageOnCollisonWithPlayer = 0f;
    protected BaseEnemyView _View;
    [SerializeField]
    GameObject _DeathParticle;
    [SerializeField]
    GameObject _DeathReward;
	//[SerializeField]
	GameObject _PowerUpReward;
	GameObject _HealthCollectibleReward;
	[SerializeField]
    Vector2 _RewardsMinMax;
	[SerializeField]
	GameObject _ChestReward;
	[SerializeField]
	ChestModel chest;
	private bool spawnPowerUp=false;
	private  bool spawnHealthCollectible=false;
	public int numberOfRewardsToSpawn;
	public bool hasDeathState=false;


    private void Start()
    {
	    _CurrentHealth = _MaxHealth;
        _Alive = true;
        OnStart();
        _View = GetComponent<BaseEnemyView>();
		_HealthCollectibleReward = Resources.Load (DataConstants.RESOURCES_HEALTH_COLLECTIBLE_PATH) as GameObject;
		_PowerUpReward= Resources.Load(DataConstants.RESOURCES_POWERUP_COLLECTIBLE_PATH) as GameObject;
			
		// to assign sound to UBH gun
		if (GetComponent<UbhShotCtrl> ())
				GetComponent<UbhShotCtrl> ().shotSound = shotSound;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(_PlayerBulletTag))
        {
			BasePlayerBullet bulletClass = other.GetComponent<BasePlayerBullet>();
            float damageToBullet = _DamageToPlayerBullet * bulletClass.MaximumHealth; 
			GetHurt(bulletClass.Damage);
			bulletClass.GetDamaged(damageToBullet);    
        }

		if (other.gameObject.tag == "Megabomb") {
			GetHurt (PlayerInventory.instance.GetMegabomb().GetLevel() );
		}

        if (other.gameObject.CompareTag(DataConstants.STR_PLAYER_TAG))
        {
            float damage = _DamageOnCollisonWithPlayer * _MaxHealth;
            GetHurt(damage);
        }
    }

    void GetHurt(float amount = 1)
    {
        _CurrentHealth -= amount;
        OnGetHurt();

		if (!hurtSound) 
		{
			AudioManager.instance.PlaySingle (hurtSound, 0.3f);
		}
        if (_CurrentHealth <= 0)
        {
            Die();
			//use this to count enemies killed
			EventHub.TriggerEvent("EnemyKilled");
        }
            
    }

    void Die()
    {
        if (_Alive)
        {
            SpawnDeathFXAndRewards();
            DestroySelf();
            OnDeath();
			AudioManager.instance.PlayRandomSfx (explosions);
        }
    }

    public void OutOfScreenBounds()// called from inspector by OnInvisibleHandler script
    {
        if (_Alive)
            DestroySelf();
    }

    void DestroySelf()
    {
        if (_Alive)
        {
            _Alive = false;
            if (EventHub.Instance != null)// to check for stopping play through Editor
                EventHub.TriggerEvent(DataConstants.STR_EVENT_ENEMY_DESTROYED);
			if (_View&&_View._DeadView) {
				if (hasDeathState) {
					_View._DeadView.gameObject.SetActive (true);
					_View._View.gameObject.SetActive (false);
					DisableComponents ();
					Destroy (this.gameObject, 10f);
				}else
				Destroy(this.gameObject);
			}
			else
				Destroy(this.gameObject);

        }
    }

	void DisableComponents()
	{

		if (GetComponent<HealthBarComponent>()) 
		{
			GetComponent<HealthBarComponent> ().DisableHealthBar ();
		}
		if (GetComponent<BoxCollider2D>()) 
		{
			GetComponent<BoxCollider2D> ().enabled = false;

		}
		if (GetComponent<PolygonCollider2D>()) 
		{
			GetComponent<PolygonCollider2D> ().enabled = false;

		}

	}

    void SpawnDeathFXAndRewards()
    {
        if (_DeathParticle != null)
        {
            GameObject parent = GameObject.FindWithTag("Wave");//.transform;
            Instantiate(_DeathParticle, transform.position, Quaternion.identity,parent==null?null:parent.transform);
        }

		if (_ChestReward != null) {
			_ChestReward.GetComponent<ChestCollectible> ().chest = chest;
			ChestCollectible chestCollectible = UbhObjectPool.Instance.GetGameObject (_ChestReward, transform.position, Quaternion.identity).GetComponent<ChestCollectible> ();
		}
		SpawnDeathRewards ();
    }

	void SpawnDeathRewards()
	{
		if (spawnPowerUp)
		{
			
			if (_PowerUpReward != null) {
				BaseCollectible collectible = UbhObjectPool.Instance.GetGameObject (_PowerUpReward, transform.position, Quaternion.identity).GetComponent<BaseCollectible> ();
				collectible.MoveAndStop ();
			} else {
				print ("The variable is null");
			}
		} 
		else if (spawnHealthCollectible)
		{
			print ("going to spawn health collectible");
			if (_HealthCollectibleReward != null) {
				BaseCollectible collectible = UbhObjectPool.Instance.GetGameObject (_HealthCollectibleReward, transform.position, Quaternion.identity).GetComponent<BaseCollectible> ();
				collectible.MoveAndStop ();

			} else {
				print ("Could not spawn health collectible");
			}
		}
		else if (_DeathReward != null)
		{
			numberOfRewardsToSpawn = Mathf.RoundToInt(UnityEngine.Random.Range(_RewardsMinMax.x, _RewardsMinMax.y));
			for(int i = 0; i < numberOfRewardsToSpawn; i++)
			{
				BaseCollectible collectible =  UbhObjectPool.Instance.GetGameObject(_DeathReward, transform.position,Quaternion.identity).GetComponent<BaseCollectible>();
				collectible.MoveAndStop();
			}
		}

	}

    public float GetCurrentHealthScale()
    {
        return Mathf.Clamp01(_CurrentHealth / _MaxHealth);
    }

	public void ActivatePowerUp()
	{
		spawnPowerUp = true;

	}

	public void ActivateHealthCollectible()
	{
		
		spawnHealthCollectible= true;

	}


    public float DamageToPlayer
    {
        get
        {
            return _DamageToPlayer;
        }

        set
        {
            _DamageToPlayer = value;
        }
    }


    public abstract void OnGetHurt();
	public abstract void OnDeath();
	public abstract void OnStart();
}

