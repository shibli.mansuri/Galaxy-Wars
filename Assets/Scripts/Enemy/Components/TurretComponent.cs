﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretComponent : UbhMonoBehaviour {
    [SerializeField]
    GameObject turretView;

    Transform playerTransform;

    private void Awake()
    {
        playerTransform = GameObject.FindWithTag(DataConstants.STR_PLAYER_TAG).transform;
    }

    private void Update()
    {
        if (playerTransform != null)
        {
            turretView.transform.LookAt2D(playerTransform, 90);
        }
    }
}
