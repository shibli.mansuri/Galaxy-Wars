﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncShotDirectionComponent : UbhMonoBehaviour {

    private UbhBaseShot[] _ArmedGuns;
    public float _gunRotationOffset = 0f;

    void Awake () {
        _ArmedGuns = GetComponentsInChildren<UbhBaseShot>();
    }
	
	
	void Update () {
        SyncArmedGunsAngle();
	}

    void SyncArmedGunsAngle()
    {
        if (_ArmedGuns != null)
        {
            for (int i = 0; i < _ArmedGuns.Length; i++)
            {
                _ArmedGuns[i]._AngleMod = transform.rotation.eulerAngles.z + 180 + _gunRotationOffset;
            }
        }

    }
}
