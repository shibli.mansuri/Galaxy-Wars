﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shopkeeper : MonoBehaviour {
    public enum ItemType { GunSlot, Megabomb, Shield, Magnet }

    public static void BuyItem(ItemType itemType)
    {
        int price = GetPriceForInAppCurrency(itemType);
        PlayerInventory.instance.SubtractCoins(price);
    }

    public static bool CanBuy(ItemType itemType)
    {
        bool canBuy = false;
        int price = GetPriceForInAppCurrency(itemType);
        if (price <= PlayerInventory.instance.GetCoins()) // should check according to ItemType and compare accordingly
            canBuy = true;

        return canBuy;
    }

    //Should be in EconomySettings.cs
    private static int GetPriceForInAppCurrency(ItemType itemType)
    {
        if (itemType == ItemType.GunSlot)
        {
            return 500;
        }
        else
            return 0;
    }
}
