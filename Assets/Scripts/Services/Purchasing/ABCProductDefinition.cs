﻿using UnityEngine.Purchasing;

public class ABCProductDefinition {

    string _independentID;//use this to refer to this product 
    IDs _storeIDs;
    ProductType _type;
    string _name;
    string _desc;
    float _price;
    RewardItems _payOut;

    public ABCProductDefinition(RewardItems payOut,string indepenedentID, ProductType type,  
        string name="Generic Product", string desc="No Description", float price = 0.99f, IDs storeIDs = null)
    {
        _payOut = payOut;
        _independentID = indepenedentID;
        _storeIDs = storeIDs;
        _type = type;
        _name = name+" "+indepenedentID;
        _desc = desc;
        _price = price;
        
    }

    public string Id { get { return _independentID; } }

    public ProductType Type { get { return _type; } }

    public string Name { get { return _name; } }

    public string Desc{ get {    return _desc; } }

    public float Price{ get { return _price;   } }

    public IDs StoreIDs { get { return _storeIDs; } }

    public RewardItems PayOut { get { return _payOut; } }

    public bool IsCrossPlatform { get { return null != _storeIDs; } }

}
