﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class ProductCatalog
{
    ProductLoadingService _loadingService;
    Dictionary<string, ABCProductDefinition> Catalog;

    public ProductCatalog()
    {
        BuildCatalog();
    }

    void BuildCatalog()
    {
        Catalog = new Dictionary<string, ABCProductDefinition>();
        ABCProductDefinition[] products = ProductLoadingService.LoadProducts();
        for(int i = 0; i < products.Length; i++)
        {
            ABCProductDefinition item = products[i];
            Catalog.Add(item.Id, item);
        }
    }

    public ConfigurationBuilder MakeConfiguration()
    {
        var configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        foreach (KeyValuePair<string,ABCProductDefinition> keyValuePair in Catalog)
            configurationBuilder.AddProduct(keyValuePair.Value);
        return configurationBuilder;
    }

    public bool ProcessProductPurchased(string id)
    {
        ABCProductDefinition product = null;
        Catalog.TryGetValue(id, out product);

        if (null != product)
        {
            PlayerInventory.instance.AddReward(product.PayOut);
            return true;
        }
        else
        {
            Debug.LogError("Product purchased does not eixst in catalog , ID : " + id);
            return false;   
        }
    }

    public ABCProductDefinition[] GetProductDefinitions()
    {
        ABCProductDefinition[]  toReturn = new ABCProductDefinition[  Catalog.Count];
        Catalog.Values.CopyTo(toReturn, 0);
        return toReturn;
    }
}
