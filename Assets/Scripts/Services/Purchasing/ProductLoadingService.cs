﻿using UnityEngine.Purchasing;

public class ProductLoadingService
{

    static ABCProductDefinition[] ReadData()
    {
        //where's the data stored and how is it structured?
        //dummy data follows
        RewardItems item = new RewardItems(1, 2, 3, 4, 5, 6);
        ABCProductDefinition[] _dummyData;
        _dummyData = new ABCProductDefinition[5];
        _dummyData[0] = new ABCProductDefinition(item, "id1", ProductType.Consumable);
        _dummyData[1] = new ABCProductDefinition(item, "id2", ProductType.Consumable);
        _dummyData[2] = new ABCProductDefinition(item, "id3", ProductType.Consumable);
        _dummyData[3] = new ABCProductDefinition(item, "id4", ProductType.Consumable);
        _dummyData[4] = new ABCProductDefinition(item, "id5", ProductType.Consumable);
        return _dummyData;
    }

   public static ABCProductDefinition[] LoadProducts()
   {
        return ReadData();
   }

}
