﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleDestroyer : MonoBehaviour {


	
	void Awake () {
        ParticleSystem[] particles = GetComponentsInChildren<ParticleSystem>();
        var maxDurationParticle = from p in particles
                            where p.main.duration > 0
                            orderby p.main.duration descending
                            select p;
        float maxDuration = maxDurationParticle.FirstOrDefault().main.duration;
        var looping = particles.Where(p => p.main.loop == true);
        foreach (ParticleSystem p in looping)
        {
            ParticleSystem.MainModule main = p.main;
            main.loop = false;
        }
            
        Destroy(this.gameObject, maxDuration);
	}
	
	
	void Update () {
		
	}
}
