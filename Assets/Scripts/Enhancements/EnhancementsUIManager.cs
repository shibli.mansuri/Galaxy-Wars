﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnhancementsUIManager : MonoBehaviour {

	public Text coinsText;

	public Text magnetDescription;
	public Button magnetButton;

	public Text megaBombDescription;
	public Button megaBombButton;

	public Text shieldDescription;
	public Button shieldButton;

	void Update () {
		coinsText.text = "Coins: " + PlayerInventory.instance.GetCoins ();

		magnetDescription.text = "Upgrade coin attraction! (" + PlayerPrefsManager.GetMagnetLevel() + "/10)";
		if (PlayerPrefsManager.GetMagnetLevel () == 10 || PlayerInventory.instance.GetCoins () < 150)
			magnetButton.interactable = false;
		else
			magnetButton.interactable = true;

		megaBombDescription.text = "Upgrade megabomb damage! (" + PlayerInventory.instance.GetMegabomb().GetLevel() + "/10)";
		if (PlayerInventory.instance.GetMegabomb ().GetLevel () >= 10 || PlayerInventory.instance.GetCoins () < 150)
			megaBombButton.interactable = false;
		else
			megaBombButton.interactable = true;

		shieldDescription.text = "Upgrade shield health! (" + PlayerInventory.instance.GetShield().GetLevel() + "/10)";
		if (PlayerInventory.instance.GetShield ().GetLevel () >= 10 || PlayerInventory.instance.GetCoins () < 150)
			shieldButton.interactable = false;
		else
			shieldButton.interactable = true;
		
	}
}
