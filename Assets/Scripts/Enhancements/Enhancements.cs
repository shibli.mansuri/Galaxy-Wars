﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enhancements : MonoBehaviour {

	public void BuyMagnetUpgrade()
	{
		if (PlayerInventory.instance.GetCoins () >= 150) 
		{
			PlayerInventory.instance.SubtractCoins (150);
			PlayerInventory.instance.UpgradeMagnet ();
		}
	}

	public void BuyMegabombUpgrade()
	{
		if (PlayerInventory.instance.GetCoins () >= 150) 
		{
			PlayerInventory.instance.SubtractCoins (150);
			PlayerInventory.instance.UpgradeMegabomb ();
		}
	}

	public void BuyShieldUpgrade()
	{
		if (PlayerInventory.instance.GetCoins () >= 150) 
		{
			PlayerInventory.instance.SubtractCoins (150);
			PlayerInventory.instance.UpgradeShield ();
		}
	}
}
